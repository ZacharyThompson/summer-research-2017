package weka.filters;

import com.research.wuil.classification.data.FileEvent;
import com.research.wuil.classification.data.FileEventObservation;
import com.research.wuil.classification.data.feature.Feature;
import com.research.wuil.classification.data.feature.Features;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.research.wuil.classification.data.FileEventObservation.ObservationsFromFile;
import static com.research.wuil.classification.data.FileEventObservation.getInstances;

public class StringToFileEvents extends SimpleStreamFilter {
    Features m_Features = Features.getDefaultFeatures();
    int m_Target = 0;


    @Override
    public String globalInfo() {
        return "A filter used to convert a String attribute to a set of FileEvents; of which a set of features can be produced";
    }

    @Override
    protected Instances determineOutputFormat(Instances inputFormat) throws Exception {
        Instances result = new Instances(inputFormat, 0);
        result.deleteAttributeAt(m_Target);

        for (Attribute attr : m_Features.getAttributes()) {
            if (attr.name().equals("class")) continue;
            result.insertAttributeAt(attr, m_Target);
        }
        return result;
    }

    @Override
    protected Instance process(Instance instance) {
        String value = instance.stringValue(m_Target);
        String parts[] = value.split("\\[|\\]");

        List<FileEvent> events = new ArrayList<>();
        int featureSize = m_Features.getAttributes().size();
        Instance result = new DenseInstance(featureSize + instance.numAttributes() - 2);

        int iOld = 0;
        int iNew = 0;
        while (iNew < m_Target) {
            result.setValue(iNew++, instance.value(iOld++));
        }
        iOld++;
        for (Feature feature : m_Features) {
            for (String part : parts) {
                String file_parts[] = part.split(":");
                if (file_parts.length != 2) continue;
                feature.process(file_parts[1]);
            }
            for (double val : feature.getValues().values()) {
                result.setValue(iNew++, val);
            }
        }
        while (iNew < result.numAttributes()) {
            result.setValue(iNew++, instance.value(iOld++));
        }
        return result;
    }


    public static void main (String args[]) {
        File file = new File("E:\\Research\\Repository\\resources\\server\\127.0.0.1\\logs\\1515316407124.log");
        List<FileEventObservation> observations = ObservationsFromFile(file, "outlier");
        Instances instances = getInstances(1);
        instances.add(observations.get(0).toInstance(instances));
        try {
            StringToFileEvents events = new StringToFileEvents();
            events.setInputFormat(instances);
            Filter.useFilter(instances, events);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Feature> getFeature() {
        return m_Features.getFeatures();
    }

    public void setFeature(List<Feature> features) {
        this.m_Features = new Features(features);
    }
}
