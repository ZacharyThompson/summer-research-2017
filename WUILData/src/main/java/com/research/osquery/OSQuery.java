package com.research.osquery;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

public class OSQuery {
    public static void main (String args[]) throws Exception {
        System.out.println(OSQuery.class);
        System.exit(0);
        File osqueryd = new File(OSQuery.class.getResource("osqueryd.exe").getFile());
        File root = osqueryd.getParentFile();
        File log = new File(root, "\\log\\");
        //FileUtils.cleanDirectory(log);

        Path database = Files.createTempDirectory("osqueryd.db");


        String params[] = {
                osqueryd.getAbsolutePath(),
                String.format("--database_path=%s", database),
                String.format("--pidfile=%s\\osqueryd.pidfile", root.getAbsolutePath()),
                String.format("--logger_path=%s\\log\\", root.getAbsolutePath()),
                String.format("--config_path=%s\\osquery.conf", root.getAbsolutePath()),
                String.format("--verbose=true", root.getAbsolutePath()),
                String.format("--allow-unsafe=true")
        };
        for (String part : params) {
            System.out.print(part + " ");
        }
        ProcessBuilder pb = new ProcessBuilder(params);
        pb.redirectErrorStream(true);

        Process process = pb.start();
        Runnable task = () -> {
            try {
                String line;
                BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while ((line = input.readLine()) != null) {
                    System.out.println(line);
                }
                input.close();
            } catch (IOException e) {
                System.out.println(" procccess not read " + e);
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        int result = process.waitFor();
        thread.join();
        if (result != 0) {
            System.out.println("Process failed with status : " + result);
        }


    }
}
