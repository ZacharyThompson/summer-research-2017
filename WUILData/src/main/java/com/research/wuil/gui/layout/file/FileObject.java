package com.research.wuil.gui.layout.file;

import javafx.beans.property.SimpleBooleanProperty;

import java.io.File;

/**
 *
 * The FileObject to be displayed in the directory visualizer.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileObject {
    /**
     * The File represented by this Object.
      */
    private File m_File;

    /**
     * The Class label the data within this Object have.
     */
    private String m_Label = "";

    /**
     * Observable pattern deterministic of whether this file is ignored or not.
     */
    private SimpleBooleanProperty m_Ignored;

    /**
     * Transient value indicating whether this file is being written to.
     */
    private transient boolean m_Blocked = false;

    /**
     * Constructor.
     *
     * @param file The file for this Object.
     */
    public FileObject(File file) { this(file, "unknown", true); }

    /**
     * Constructor.
     */
    public FileObject() { this.m_File = new File(""); }

    /**
     * Constructor.
     *
     * @param file The file for this Object.
     * @param label The class label of th file.
     * @param ignored Whether this file is ignored.
     */
    public FileObject(File file, String label, boolean ignored) {
        this.m_File = file;
        this.m_Label = label;
        this.m_Ignored = new SimpleBooleanProperty(false);
        this.setIgnored(ignored);
    }

    /**
     * Set whether this file is ignored and therefore not used for classification.
     * Will trigger an observable event for anything bound to the Observable
     * Boolean item.
     *
     * @param value Ignored or not.
     */
    public void setIgnored(boolean value) { m_Ignored.set(value); }

    /**
     * Checks whether this file is ignored and therefore not used for classification.
     *
     * @return is ignored.
     */
    public boolean isIgnored() { return m_Ignored.get(); }

    /**
     * Gets the Observable ignored property.
     *
     * @return Observable boolean property ignored.
     */
    public SimpleBooleanProperty getIgnoreProperty() { return m_Ignored; }

    /**
     * Gets the file that this Object is representative of.
     *
     * @return The file
     */
    public File getFile() { return m_File; }

    /**
     * Sets the label of this Object.
     *
     * @param label The label
     */
    public void setLabel(String label) { this.m_Label = label; }

    /**
     * Gets the label of this Object.
     *
     * @return The label
     */
    public String getLabel() { return m_Label; }

    /**
     * Sets whether this Object is blocked (ie being written to).
     *
     * @param blocked Blocked or not.
     */
    public void setBlocked(boolean blocked) { this.m_Blocked = blocked; }

    /**
     * Checks whether this file is blocked (ie being written to).
     *
     * @return Blocked or not.
     */
    public boolean isBlocked() { return m_Blocked; }

    /**
     * Checks whether the file is a valid log file and is valid for use in
     * classification.
     *
     * @return Whether a file or not.
     */
    public boolean isFile() { return !m_File.isDirectory() && !m_File.getPath().contains(".conf"); }

    /**
     * Checks whether the data contained in the file is valid training data. Will test by ensuring
     * the file is a valid log file, the Object is not ignored, the Object is not blocked and the label
     * is not unknown.
     *
     * @return Whether valid training data.
     */
    public boolean isTrainData() { return isFile() && !isIgnored() && !m_Label.equals("unknown") && !m_Blocked; }

    /**
     * Checks whether the data contained in the file is valid test data. Will test by ensuring
     * the file is a valid log file, the Object is not ignored, and it's label is unknown.
     *
     * @return Whether valid test data.
     */
    public boolean isTestData() { return isFile() && !isIgnored() && m_Label.equals("unknown"); }

    /**
     * String representation of this Object.
     *
     * @return String representation of this Object.
     */
    @Override
    public String toString() { return (isFile()) ? m_File.getName() + " --- " + m_Label : m_File.getName(); }

    /**
     * Equals override.
     *
     * @param o Object to compare equality.
     * @return Whether the Object is equal to the current Object.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof File) return m_File.equals(o);
        if (o instanceof FileObject) return m_File.equals(((FileObject) o).m_File);
        return false;
    }

    /**
     * hashCode override to maintain contract required for comparing equality of Objects (equal Objects must
     * have the same hashCode).
     *
     * @return The hash code of the Object.
     */
    @Override
    public int hashCode() {
        return m_File.hashCode();
    }
}