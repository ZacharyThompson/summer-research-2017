package com.research.wuil.gui.layout.selection.input;

import javafx.scene.layout.Region;
import org.jetbrains.annotations.NotNull;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * This interface is used to provide an interface to create JavaFX Input fields, dynamically, for different
 * types of data. This ranges from the most basic types, the primitive wrappers, to Objects that implement
 * and interface. The type parameter is used to define the type of input this particular input takes.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public abstract class GenericInput<T> implements Cloneable {
    /**
     * Get the value of this input.
     *
     * @return Input value
     */
    public abstract T getValue();

    /**
     * Set the value of this input.
     *
     * @param t Value to set.
     */
    public abstract void setValue(T t);

    /**
     * Get the JavaFX input, as a subclass of Region, used to collect input from the user.
     *
     * @return The input.
     */
    public abstract Region getInput();

    /**
     * Create a copy of the Object
     *
     * @return Copy of the Object
     */
    public abstract GenericInput<T> copy();

    /**
     * Gets a GenericInput Object for specific input data type.
     *
     * @param object The object being edited; the property belongs to this Objects class.
     * @param property The specific property to create an input for.
     * @return The generic input for the specified property.
     */
    public static GenericInput getGenericInput(Object object, PropertyDescriptor property) {
        Class type = property.getPropertyType();
        if (List.class.isAssignableFrom(type)) {
            try {
                Method field = object.getClass().getDeclaredMethod(
                        property.getReadMethod().getName(), property.getReadMethod().getParameterTypes());
                Type returnType = field.getGenericReturnType();
                if (returnType instanceof ParameterizedType) {
                    ParameterizedType param = (ParameterizedType) returnType;
                    return new ListInput(param.getActualTypeArguments()[0]);
                }
            } catch (Exception e) {
                System.err.println("Unable to find generic List type");
                return null;
            }
        }
        return getGenericInput(type);
    }

    /**
     * Gets a GenericInput Object for a specific input data type.
     *
     * @param type The type to create a generic input for.
     * @return The generic input for the specified type.
     */
    public static GenericInput getGenericInput(@NotNull Class type) {
        if (type.equals(String.class)) {
            return new StringInput();
        } else if (type.isPrimitive()) {
            if (type.equals(boolean.class)) {
                return new BooleanInput();
            } else {
                return new NumericInput(type);
            }
        } else if (List.class.isAssignableFrom(type)) {
            return null;
        } else if (type.isInterface() || Modifier.isAbstract(type.getModifiers())) {
            try {
                return new InterfaceInput(type);
            } catch (Exception e) {
                System.err.println(String.format("Could not create new instance [%s]", type));
            }
        }
        return null;
    }
}