package com.research.wuil.gui.layout.selection.input;

import com.research.wuil.gui.layout.selection.ListPane;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.Region;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Object that creates an Input dialog for List items. Work in progress.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ListInput extends GenericInput<List<Object>> {
    List<Object> m_Data = new ArrayList<>();
    Button m_Input = new Button("Choose");
    Class m_Class;
    ListPane<GenericInput> listPane;

    public ListInput(Type type) {
        if (type instanceof Class) {
            m_Class = (Class) type;
        }
        m_Input.setOnMouseClicked(event -> show());
    }

    @Override
    public List<Object> getValue() {
        return m_Data;
    }

    @Override
    public void setValue(List<Object> objects) {
        m_Data = objects;
        if (listPane != null)
            listPane.addItems(objects);
    }

    @Override
    public Region getInput() {
        return m_Input;
    }

    public void show() {
        if (m_Class == null) {
            System.err.println("Invalid Class type");
            return;
        }

        listPane = new ListPane<>(m_Class);
        listPane.addItems(m_Data);

        DialogPane dialogPane = new DialogPane();
        dialogPane.setContent(listPane);
        dialogPane.getButtonTypes().add(ButtonType.OK);
        dialogPane.getButtonTypes().add(ButtonType.CANCEL);
        dialogPane.setPrefWidth(500);

        Dialog<Boolean> dialog = new Dialog<>();
        dialog.setTitle("List of " + m_Class.getSimpleName() + "s");
        dialog.setWidth(500);
        dialog.setDialogPane(dialogPane);
        dialog.setResultConverter(buttonType -> {
            if (buttonType == ButtonType.OK) {
                m_Data.clear();
                m_Data.addAll(listPane.getItems());
            }
            return true;
        });
        dialog.showAndWait();
    }

    /**
     * Create a copy of this Object.
     *
     * @return A copy of this Object
     */
    @Override
    public ListInput copy() {
        ListInput input = new ListInput(m_Class);
        input.setValue(this.getValue());
        return input;
    }
}
