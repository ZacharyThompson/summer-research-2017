package com.research.wuil.gui.layout.console.io;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * Text output stream used in tandem with a JavaFX text area.
 *
 * Inspired by post: https://stackoverflow.com/questions/342990/create-java-console-inside-a-gui-panel
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class TextAreaOutputStream extends OutputStream {
    /**
     * Line Appender used to append lines to a Text Area.
     */
    LineAppender m_Appender;

    /**
     * Constructor.
     *
     * @param textArea TextArea to append Strings from the OutputStream to.
     * @param maxLines The maximum number of lines to be used.
     */
    public TextAreaOutputStream(TextArea textArea, int maxLines) {
        m_Appender = new LineAppender(textArea, maxLines);
    }

    /**
     * Constructor. Defaults maximum number of lines as 1000.
     *
     * @param textArea TextArea to append Strings from the OutputStream to.
     */
    public TextAreaOutputStream(TextArea textArea) {
        this(textArea, 1000);
    }

    /**
     * Append integer to TextArea as a byte.
     *
     * @param b Value to be written.
     */
    @Override
    public synchronized void write(int b) {
        if (m_Appender != null) write(new byte[]{(byte)b}, 0, 1);
    }

    /**
     * Append byte array to TextArea as String.
     *
     * @param b Byte array to be used to construct the String.
     */
    @Override
    public synchronized void write(@NotNull byte[] b) {
        if (m_Appender != null) write(b, 0, b.length);
    }

    /**
     * Write byte array to TextArea as String using offset and length to construct
     * the String.
     *
     * @param b Byte array to be used to construct the String.
     * @param off Offset to begin in the byte array.
     * @param len Length of the String.
     */
    @Override
    public synchronized void write(@NotNull byte[] b, int off, int len) {
        if (m_Appender != null) m_Appender.append(bytesToString(b, off, len));
    }

    /**
     * Append String to TextArea with no filter.
     *
     * @param string String to be appended.
     */
    public synchronized  void write(String string) {
        if (m_Appender != null) m_Appender.append(string);
    }

    /**
     * Flush content override. The object auto flushes always, hence this does nothing.
     */
    @Override
    public synchronized void flush() {}

    /**
     * Closes the output stream.
     */
    @Override
    public synchronized void close() {
        m_Appender = null;
    }

    /**
     * Clears all data from the TextArea.
     */
    public synchronized void clear() {
        if (m_Appender != null) m_Appender.clear();
    }

    /**
     * Converts a byte array to a String.
     *
     * @param b Byte array to be used.
     * @param off Offset to start the String from.
     * @param len Length of the String.
     * @return The String created.
     */
    private static String bytesToString(byte[] b, int off, int len) {
        try { return new String(b,off,len,"UTF-8"); } catch(UnsupportedEncodingException thr) { return new String(b,off,len); }
    }

    /**
     * Line Appender that handles the actual displaying of data to the Text Area.
     */
    private static class LineAppender implements Runnable {
        /**
         * Text Area to append data to.
         */
        private final TextArea m_TextArea;

        /**
         * Maximum number of lines.
         */
        private final int m_MaxLines;

        /**
         * Lengths of the various lines that have been added.
         */
        private final LinkedList<Integer> m_Lengths;

        /**
         * Values of the various lines that have been added.
         */
        private final List<String> values;

        /**
         * Length of the current line in the TextArea
         */
        private int m_CurLength = 0;

        /**
         * Whether a clear request has been requested.
         */
        private boolean m_Clear = false;

        /**
         * Whether a m_Queue request has been requested.
         */
        private boolean m_Queue = true;

        /**
         * Constructor.
         *
         * @param textArea Text Area to append lines to.
         * @param maxLines The maximum number of lines permitted.
         */
        LineAppender(TextArea textArea, int maxLines) {
            this.m_TextArea = textArea;
            this.m_MaxLines = maxLines;
            this.m_Lengths = new LinkedList<>();
            this.values = new ArrayList<>();
        }

        /**
         * Append value to Text Area.
         *
         * @param value String to be appended.
         */
        synchronized void append(String value) {
            values.add(value);
            if (m_Queue) {
                m_Queue = false;
                Platform.runLater(this);
            }
        }

        /**
         * Issue a clear request.
         */
        synchronized void clear() {
            m_Clear = true;
            m_CurLength = 0;
            m_Lengths.clear();
            values.clear();
            if (m_Queue) {
                m_Queue = false;
                Platform.runLater(this);
            }
        }

        /**
         * Writing method which is to be executed on the UI thread. Used to append or rewrite data
         * to the displayed TextArea.
         */
        @Override
        public synchronized void run() {
            if (m_Clear) m_TextArea.setText("");
            for (String val : values) {
                m_CurLength += val.length();
                if (val.endsWith(EOL1) || val.endsWith(EOL2)) {
                    if (m_Lengths.size() >= m_MaxLines) {
                        m_TextArea.replaceText(0, m_Lengths.removeFirst(), "");
                        m_Lengths.addLast(m_CurLength);
                        m_CurLength = 0;
                    }
                }
                m_TextArea.appendText(val);
            }
            values.clear();
            m_Clear = false;
            m_Queue = true;
        }

        static private final String EOL1="\n";
        static private final String EOL2=System.getProperty("line.separator",EOL1);
    }
}
