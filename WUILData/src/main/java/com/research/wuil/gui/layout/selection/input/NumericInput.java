package com.research.wuil.gui.layout.selection.input;

import com.research.wuil.utils.TypeParser;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;

/**
 *
 * Object that creates a TextField input for Classes that extent Number.
 *
 * @param <T> Type
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class NumericInput<T extends Number> extends GenericInput<T> {
    /**
     * The Input field
     */
    private TextField m_Input;

    /**
     * The type of input.
     */
    private Class<T> m_Type;

    /**
     * Constructor.
     *
     * @param type The type of input.
     */
    public NumericInput(Class<T> type) {
        m_Input = new TextField();
        m_Type = type;
    }

    /**
     * Get the value within the input. Casts the input from within the TextField
     * to the appropriate type.
     *
     * @return The, appropriately cast, value from the input field.
     */
    @Override
    public T getValue() {
        try {
            return cast(m_Input.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set the value in the TextField to the String value of the given item/
     *
     * @param t Value to set.
     */
    @Override
    public void setValue(T t) {
        m_Input.setText(t.toString());
    }

    /**
     * Get the JavaFX input, a TextField, which is used to collect input from the user.
     *
     * @return TextField input.
     */
    @Override
    public Region getInput() {
        return m_Input;
    }

    /**
     * Cast String to the Object type, otherwise return null.
     *
     * @param s String value to cast.
     * @return Parsed value.
     */
    private T cast(String s) {
        try {
            return (T) TypeParser.parse(s, m_Type);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Create a copy of this Object.
     *
     * @return A copy of this Object
     */
    @Override
    public NumericInput<T> copy() {
        NumericInput<T> input = new NumericInput<T>(m_Type);
        input.setValue(this.getValue());
        return input;
    }
}
