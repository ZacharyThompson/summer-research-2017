package com.research.wuil.gui.layout;

import com.research.wuil.classification.data.FileEventObservation;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import com.research.wuil.gui.layout.file.FileObject;
import com.research.wuil.gui.layout.file.FileViewerPane;
import com.research.wuil.utils.OptionSerializer;
import javafx.scene.control.SplitPane;
import weka.classifiers.Classifier;
import weka.core.Instances;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * A pane used to illustrate a connection.
 * Each instance of a connection between a end point and the server spawns a
 * new connection pane which logs incoming OSQuery data, performs classification
 * and updates the m_Client accordingly.
 * Simplified, this is an instance of the Server for a unique connection.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ConnectionPane extends SplitPane {
    /**
     * The Socket of the client.
     */
    private Socket m_Client;

    /**
     * The console pane to log information and take input.
     */
    private FilteredConsolePane m_ConsolePane = new FilteredConsolePane();

    /**
     * The file view pane to allow an administrator to modify log file settings.
     */
    private FileViewerPane m_FileViewerPane;

    /**
     * An input buffered reader from the Client socket
     */
    private BufferedReader m_In;

    /**
     * An output print writer for the Client socket
     */
    private PrintWriter m_Out;

    /**
     * An output print writer for writing the logs to a new file.
     */
    private PrintWriter m_LogOut;

    /**
     * Constructor.
     *
     * @param client The connection sockets.
     * @param root The root file for where the server holds client information.
     */
    public ConnectionPane(Socket client, File root) {
        this.m_Client = client;
        this.getItems().add(m_ConsolePane);

        List<FileObject> blockedFiles = new ArrayList<>();
        File file;

        // If the client isn't null, set up framework for intended communication
        if (client != null) {
            // Find the client folder, otherwise create it.
            root = new File(root, client.getInetAddress().toString());
            if (!root.exists())  {
                root.mkdir();
            }
            // Create a new log file
            file = new File(root, "logs/" + System.currentTimeMillis() + ".log");
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Create a FileObject for the file and block it.
            FileObject blockedFile = new FileObject(file, "target", false);
            blockedFile.setBlocked(true);
            blockedFiles.add(blockedFile);
            // Create file viewer pane.
            m_FileViewerPane = new FileViewerPane(root, m_ConsolePane, blockedFiles);

            try {
                // Collect input and output streams
                m_In = new BufferedReader(new InputStreamReader(client.getInputStream()));
                m_Out = new PrintWriter(client.getOutputStream(), true);
                m_LogOut = new PrintWriter(new FileOutputStream(file), true);
                m_FileViewerPane.setWriter(m_Out);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Start listening to the client
            listenToClient();
        } else {
            m_FileViewerPane = new FileViewerPane(root, m_ConsolePane, blockedFiles);
        }
        this.getItems().add(m_FileViewerPane);

        // Add input handlers to the console. This was primarily for testing, but still serves a purpose.
        m_ConsolePane.addInputHandler((line, level) -> {
            if (level >= 3) {
                String[] data = line.split(" ", 2);
                switch (data[0]) {
                    case "exit":
                        close();
                    case "clear":
                        m_ConsolePane.clear();
                    case "transmit":
                        if (data.length == 2 && m_Out != null) m_Out.println(data[1]);
                }
            }
        });
    }

    /**
     * Get the host name of the connected client.
     *
     * @return Host name of client.
     */
    public String getName() {
        return m_Client.getInetAddress().getHostName();
    }

    /**
     * Attempt to close connection.
     */
    public void close() {
        try {
            m_Client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the console pane associated with this console pane.
     * Allows different classes to log relevant information to the console output.
     *
     * @return The filtered console pane.
     */
    public FilteredConsolePane getConsolePane() {
        return m_ConsolePane;
    }

    /**
     * Start listening to the client.
     */
    private void listenToClient() {
        new Thread(() -> {
            String line;
            try {
                // Simple pattern for reading a line of input from the client.
                while (m_ConsolePane != null && (line = m_In.readLine()) != null) {
                    // If the line starts with the text "query-results", it is a OSQuery record.
                    if (line.startsWith("query_results:")) {
                        // Records for an observation are appended using a <SEPARATOR> sequence.
                        // Every record within this String must be part of the same batch query.
                        line = line.replace("query_results:", "");
                        ArrayList<String> items = new ArrayList<>(Arrays.asList(line.split("<SEPARATOR>")));
                        for (String item : items) {
                            // Log the data to be used for further classification
                            m_LogOut.println(item);
                            m_ConsolePane.log("Client", item);
                        }
                        // As a new thread, attempt to classify the incoming data.
                        new Thread(() -> {
                            ArrayList<FileEventObservation> observations = FileEventObservation.ObservationsFromList(items, "unknown");
                            Instances instances = FileEventObservation.RawInstancesFromCollection(observations);
                            m_FileViewerPane.classify(instances);
                        }).start();
                    }
                    // If the line starts with the text "classifier", it is a classification model.
                    else if (line.startsWith("classifier:")) {
                        // Strip the identifier from the line
                        line = line.replace("classifier:", "");
                        // Log classifier to console
                        m_ConsolePane.log("Client", String.format("Attempting to load classifier {%s}", line));
                        // Attempt to deserialize the String.
                        Object o = OptionSerializer.deserialize(line);
                        // If the String deserializes to an Object that is an instanceof Classifier, change the model
                        if (o != null && o instanceof Classifier) {
                            m_FileViewerPane.setClassifier((Classifier) o);
                            m_ConsolePane.log("Client", String.format("Successfully loaded classifier {%s}", o.getClass()));
                        }
                    }
                    // Else simply log the output to the console pane
                    else {
                        m_ConsolePane.log("Client", line);
                    }
                }
            } catch (Exception e) {
                m_ConsolePane.println("Connection Lost");
            }
            m_Out.close();
        }).start();
    }
}