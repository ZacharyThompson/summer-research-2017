package com.research.wuil.gui.layout.file;

import javafx.beans.binding.Bindings;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.paint.Color;

import java.awt.*;

/**
 *
 * The TreeCell used to display data in the directory visualizer implemented.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileTreeCell extends TextFieldTreeCell<FileObject> {
    /**
     * The ContextMenu for the TreeCell
     */
    private final FileContextMenu m_ContextMenu = new FileContextMenu();

    /**
     * Constructor.
     */
    public FileTreeCell() {
        this.setOnContextMenuRequested(evt -> {;
            prepareContextMenu(getTreeItem());
            evt.consume();
        });
    }

    /**
     * Helper method to prepare the context menu.
     *
     * @param item The TreeItem to prepare for.
     */
    private void prepareContextMenu(TreeItem<FileObject> item) {
        if (item == null) return;

        FileObject obj = item.getValue();

        MenuItem delete = m_ContextMenu.getDelete();
        delete.setOnAction(evt -> {
            item.getParent().getChildren().remove(item);
            m_ContextMenu.freeActionListeners();
            try {
                obj.getFile().delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        MenuItem open = m_ContextMenu.getOpen();
        open.setOnAction(evt -> {
            try {
                Desktop.getDesktop().open(obj.getFile());
            } catch (Exception e){
                e.printStackTrace();
            }
            m_ContextMenu.freeActionListeners();
        });

        MenuItem ignore = m_ContextMenu.getIgnore();
        ignore.setOnAction(evt -> {
            obj.setIgnored(!obj.isIgnored());
            m_ContextMenu.freeActionListeners();
        });

        MenuItem positive = m_ContextMenu.getMarkPositive();
        positive.setOnAction(evt -> {
            obj.setLabel("target");
            m_ContextMenu.freeActionListeners();
            textProperty().setValue(obj.toString());
        });

        MenuItem negative = m_ContextMenu.getMarkNegative();
        negative.setOnAction(evt -> {
            obj.setLabel("outlier");
            m_ContextMenu.freeActionListeners();
            textProperty().setValue(obj.toString());
        });

        MenuItem unknown = m_ContextMenu.getMarkUnknown();
        unknown.setOnAction(evt -> {
            obj.setLabel("unknown");
            m_ContextMenu.freeActionListeners();
            textProperty().setValue(obj.toString());
        });

        ignore.setVisible(obj.isFile());
        negative.setVisible(obj.isFile());
        positive.setVisible(obj.isFile());
        unknown.setVisible(obj.isFile());
    }

    /**
     * Update item.
     *
     * @param item Item to update.
     * @param empty Whether the item is empty.
     */
    @Override
    public void updateItem(FileObject item, boolean empty) {
        super.updateItem(item, empty);
        if (!empty) {
            setContextMenu("nocontext".equals(item.toString()) ? null : m_ContextMenu.getContextMenu());
            setEditable(!"noedit".equals(item.toString()));
        }

        if (item != null && item.isFile()) {
            textFillProperty().bind(Bindings.when(item.getIgnoreProperty()).then(Color.RED).otherwise(Color.GREEN));
        } else if (item != null){
            textFillProperty().bind(Bindings.when(item.getIgnoreProperty()).then(Color.BLACK).otherwise(Color.BLACK));
        }
    }

    /**
     * Basic FileContextMenu Object.
     */
    public static class FileContextMenu {
        private MenuItem open = new MenuItem("Open");
        private MenuItem delete = new MenuItem("Delete");
        private MenuItem ignore = new MenuItem("Toggle Ignore");
        private MenuItem markPositive = new MenuItem("Mark Target");
        private MenuItem markNegative = new MenuItem("Mark Outlier");
        private MenuItem markUnknown = new MenuItem("Mark Unknown");
        private ContextMenu contextMenu;

        FileContextMenu() {
            this.contextMenu = new ContextMenu(
                    markPositive, markNegative, markUnknown,
                    new SeparatorMenuItem(),
                    open, delete,
                    new SeparatorMenuItem(),
                    ignore);
        }

        ContextMenu getContextMenu() {
            return contextMenu;
        }

        MenuItem getOpen() { return open; }
        MenuItem getDelete() { return delete; }
        MenuItem getIgnore() { return ignore; }
        MenuItem getMarkPositive() { return markPositive; }
        MenuItem getMarkNegative() { return markNegative; }
        MenuItem getMarkUnknown() { return markUnknown; }

        void freeActionListeners() {
            this.open.setOnAction(null);
            this.delete.setOnAction(null);
        }
    }
}
