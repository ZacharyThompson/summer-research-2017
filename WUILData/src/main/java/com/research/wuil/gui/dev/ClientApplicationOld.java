package com.research.wuil.gui.dev;

import com.research.wuil.gui.Client;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public class ClientApplicationOld extends Application {
    private FilteredConsolePane consolePane = new FilteredConsolePane("Default", "Server", "Osquery");
    private TabPane tabPane = new TabPane();
    private Client client;
    private HashMap<String, String> parameters = new HashMap<>();

    public ClientApplicationOld() {
        Tab tab = new Tab("Client Console");
        setInputEvents(consolePane);
        tab.setContent(consolePane);
        tab.setClosable(false);
        tabPane.getTabs().add(tab);

        //parameters.put("osqueryd", ResourceManager.getClient().getFile("osqueryd").getAbsolutePath());
        parameters.put("osqueryd", "/home/zac/Documents/osqueryd");
        parameters.put("monitor", "/home/zac/Documents/Monitored Environment/");
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Client");
        primaryStage.setScene(new Scene(tabPane, 1000, 800));
        primaryStage.show();
    }

    public synchronized void addTab(Tab tab) {
        Platform.runLater(new Thread (()-> {
            tabPane.getTabs().add(tab);
        }));
    }

    private void setInputEvents(FilteredConsolePane consolePane) {
        consolePane.addInputHandler((input, level) -> {
            if (input.startsWith("connect ")) {
                if (client == null || !client.isRunning()) {
                    client = new Client(consolePane, input, new File(parameters.get("monitor")));
                } else {
                    consolePane.log("Server", "Connection already established");
                }
            } else if (input.startsWith("set ")) {
                input = input.replaceFirst("set ", "");
                String[] split = input.split(" ");
                if (split.length < 2) {
                    consolePane.log("Default", "Invalid Arguments: Expected set [parameter name] [parameter value]");
                }
                String value = "";
                for (int i = 1; i < split.length; i++) {
                    value += split[i] + " ";
                }
                parameters.put(split[0], value.trim());
            } else if (input.startsWith("get")) {
                consolePane.log("Default", "Parameters:");
                consolePane.log("Default", parameters.toString());
            }
        });
    }

    public void addTabs(List<Tab> tabs) {
        for (Tab tab : tabs) {
            this.addTab(tab);
        }
    }

    public List<Tab> getTabs() {
        return tabPane.getTabs();
    }

    private void println(String line) {
        consolePane.log("Default", line);
    }
}
