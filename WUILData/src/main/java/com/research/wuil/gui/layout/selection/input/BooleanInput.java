package com.research.wuil.gui.layout.selection.input;

import javafx.scene.control.ComboBox;
import javafx.scene.layout.Region;

/**
 *
 * Object that creates a ComboBox input for Boolean values.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class BooleanInput extends GenericInput<Boolean> {
    /**
     * The Input field
     */
    private ComboBox<Boolean> m_Input = new ComboBox<>();

    /**
     * Constructor.
     */
    public BooleanInput() {
        m_Input.getItems().addAll(true, false);
    }

    /**
     * Get the value within the input.
     *
     * @return The selected item
     */
    @Override
    public Boolean getValue() {
        return m_Input.getValue();
    }

    /**
     * Set the value of the input
     *
     * @param b Value to set.
     */
    @Override
    public void setValue(Boolean b) {
        m_Input.setValue(b);
    }

    /**
     * Get the JavaFX input, a ComboBox, which is used to collect input from the user.
     *
     * @return ComboBox input.
     */
    @Override
    public Region getInput() {
        return m_Input;
    }

    /**
     * Create a copy of this Object.
     *
     * @return A copy of this Object
     */
    @Override
    public BooleanInput copy() {
        BooleanInput input = new BooleanInput();
        input.setValue(this.getValue());
        return input;
    }
}
