package com.research.wuil.gui.application;

import javafx.stage.Stage;

/**
 *
 * The application to be used for development purposes.
 * It acts as both the server and client by extending the server
 * and attaching the client to it.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class HybridApplication extends ServerApplication {
    /**
     * Start the application
     *
     * @param primaryStage Stage allocated to the application.
     */
    @Override
    public void start(Stage primaryStage) {
        ClientApplication client = new ClientApplication();
        this.addTabs(client.getTabs());
        super.start(primaryStage);
    }
}
