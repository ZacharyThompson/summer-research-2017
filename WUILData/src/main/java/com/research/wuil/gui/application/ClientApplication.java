package com.research.wuil.gui.application;

import com.research.wuil.gui.layout.ClientHomePane;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.util.List;

/**
 *
 * The Application that the client will use as an interface to the project.
 * Enables connecting to a backend server that will handle the classification and storage of
 * logging records.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ClientApplication extends Application {
    /**
     * The tab m_Pane to hold the tabs relevant to to this object.
     */
    private TabPane m_TabPane = new TabPane();

    /**
     * Constructor
     */
    public ClientApplication() {
        Tab tab = new Tab("Client Console");
        tab.setContent(new ClientHomePane());
        tab.setClosable(false);
        m_TabPane.getTabs().add(tab);
    }

    /**
     * Starts the application.
     *
     * @param primaryStage The stage to be used for this application.
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Client");
        primaryStage.setScene(new Scene(m_TabPane, 1000, 800));
        primaryStage.show();
    }

    /**
     * Adds tab to tab m_Pane.
     *
     * @param tab Tab to be added.
     */
    public synchronized void addTab(Tab tab) {
        Platform.runLater(new Thread (()-> {
            m_TabPane.getTabs().add(tab);
        }));
    }

    /**
     * Add tabs to tab m_Pane.
     *
     * @param tabs Tabs to be added.
     */
    public void addTabs(List<Tab> tabs) {
        for (Tab tab : tabs) {
            this.addTab(tab);
        }
    }

    /**
     * Returns a collection of tabs currently associated with this application.
     *
     * @return Collection of tabs.
     */
    public List<Tab> getTabs() {
        return m_TabPane.getTabs();
    }
}
