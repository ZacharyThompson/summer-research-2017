package com.research.wuil.gui;

import com.research.wuil.gui.application.ServerApplication;
import com.research.wuil.gui.layout.ConnectionPane;
import com.research.wuil.utils.ResourceManager;

import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * A Simple socket server used to accept incoming connections.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class Server implements Runnable {
    /**
     * The server socket that will be bound to the specified port and connections listened to.
     */
    private ServerSocket m_Server;

    /**
     *  The port to listen for incoming connections on.
     */
    private int m_Port;

    /**
     * The outgoing print stream to print messages to.
     */
    private PrintStream m_Out;

    /**
     * The server application for this Object. Established connections spawn a tab which are added
     * the application
     */
    private ServerApplication m_Application;

    /**
     * Constructor. Default to port 8080.
     *
     * @param application The server application to spawn threaded clients on.
     */
    public Server(ServerApplication application) {
        this(application, 8080);
    }

    /**
     * Constructor.
     *
     * @param application The server application to spawn threaded clients on.
     * @param port The port to listen on.
     */
    public Server(ServerApplication application, int port) {
        this.m_Port = port;
        this.m_Application = application;
        this.m_Out = application.getConsole().m_Out;
    }

    /**
     * Thread to listen for clients on.
     * Each established connection spawns a new thread for the client and
     * adds a tab to the server application.
     */
    @Override
    public void run() {
        try {
            m_Server = new ServerSocket(m_Port);
            m_Out.println("Server bound on m_Port : " + m_Server.getLocalPort());

            while (true) {
                Socket client = m_Server.accept();
                m_Out.println("Connection Established : " + client.getInetAddress());
                ConnectionPane connectionPane = new ConnectionPane(client, ResourceManager.getServer().getRoot());
                m_Application.addConnection(connectionPane);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
