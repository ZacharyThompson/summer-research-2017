package com.research.wuil.gui.application;

import com.research.wuil.classification.data.FileEventObservation;
import com.research.wuil.classification.data.feature.Feature;
import com.research.wuil.gui.layout.selection.GenericSelectionButton;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.filters.StringToFileEvents;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.research.wuil.classification.data.FileEventObservation.ObservationsFromFile;
import static com.research.wuil.classification.data.FileEventObservation.RawInstancesFromCollection;

public class SelectionApplication extends Application {
    ArrayList<Feature> features = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        GenericSelectionButton genericSelectionButton = new GenericSelectionButton(Classifier.class);
        FilteredClassifier classifier = new FilteredClassifier();
        classifier.setClassifier(new IBk());
        classifier.setFilter(new StringToFileEvents());
        genericSelectionButton.setSelection(classifier);

        Button button = new Button("Execute");
        button.setOnAction(e -> {
            Object o = genericSelectionButton.getSelection();
            if (o instanceof Classifier) {
                Test((Classifier) o);
            }
        });

        VBox vbox = new VBox(genericSelectionButton, button);
        Scene scene = new Scene(vbox);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void Test(Classifier classifier) {
        File file = new File("E:\\Research\\Repository\\resources\\server\\127.0.0.1\\logs\\1515316407124.log");

        List<FileEventObservation> observations = ObservationsFromFile(file, "target");
        Instances instances = RawInstancesFromCollection(observations);

        try {
            Evaluation evaluation = new Evaluation(instances);
            evaluation.crossValidateModel(classifier, instances, 10, new Random(10));
            System.out.println(evaluation.toSummaryString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}