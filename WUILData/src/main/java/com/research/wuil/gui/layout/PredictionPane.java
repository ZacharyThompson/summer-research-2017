package com.research.wuil.gui.layout;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.Section;
import javafx.application.Platform;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Random;


/**
 *
 * A simple visualization of the backend classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class PredictionPane extends GridPane {
    /**
     * The number of segments to split the gauge into.
     */
    private static final int NUM_SEGMENTS = 7;

    /**
     * The maximum RGB value.
     */
    private static double MAX_RGB = 255;

    /**
     * The gauge for illustrating predictions.
     */
    private Gauge m_Gauge;

    /**
     * The chart to illustrate a time series relationship between predicted m_Values.
     */
    private LineChart<Number, Number> m_Chart;

    /**
     * The series for the line chart.
     */
    private XYChart.Series<Number, Number> m_Series;


    /**
     * Constructor.
     */
    public PredictionPane() {
        createGauge();
        createChart();
        this.add(m_Gauge, 0, 0);
        this.add(m_Chart, 0, 1);
    }

    /**
     * Creates a gauge.
     */
    private void createGauge() {
        // Builds the gauge
        m_Gauge = GaugeBuilder.create()
                .subTitle("Deviation From Pattern")
                .unit("%")
                .skinType(Gauge.SkinType.SIMPLE)
                .animated(true)
                .build();

        // Modifies the segment colours
        for (int i = 0; i < NUM_SEGMENTS; i++) {
            Section section = new Section(i * (100.0 / NUM_SEGMENTS), (i + 1) * (100.0 / NUM_SEGMENTS), nextColor(i+1));
            m_Gauge.addSection(section);
        }

        // Add an on mouse clicked to the gauge. Work in progress.
        m_Gauge.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
                /*Stage stage = new Stage();
                stage.setTitle("Graph");
                stage.setScene(new Scene(new Button()));
                stage.show();*/
                //ToDo
            }
        });
    }

    /**
     * Creates a chart.
     */
    private void createChart() {
        // Configure the chart
        final NumberAxis xAxis = new NumberAxis();
        xAxis.setVisible(false);
        xAxis.setTickLabelsVisible(false);
        xAxis.setTickMarkVisible(false);
        xAxis.setMinorTickVisible(false);
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(100);
        yAxis.setAutoRanging(false);
        yAxis.setMinorTickVisible(false);
        yAxis.setTickUnit(25);

        m_Chart = new LineChart<>(xAxis, yAxis);
        m_Chart.setTitle("Deviation History");
        m_Series = new XYChart.Series<>();
        m_Chart.getData().add(m_Series);
    }

    /**
     * Calculate the next color for the segment in the gauge.
     * Gives a transition from green to red to illustrate the
     * warning levels effectively.
     *
     * @param index The index of the segment.
     * @return The next color in the pattern.
     */
    private static Color nextColor(int index) {
        int size = NUM_SEGMENTS;
        double red = (index * (2 * MAX_RGB / size));
        double green = (2 * MAX_RGB - index * (2 * MAX_RGB / size));
        double blue = 0;
        return toColor(red, green, blue);
    }

    /**
     * Converts double red, green, blue levels to a colour.
     *
     * @param r The extent of red.
     * @param g The extent of green.
     * @param b The extent of blue.
     * @return The associated color.
     */
    private static Color toColor(double r, double g, double b) {
        r = r < 0 ? 0 : r > MAX_RGB ? 255 : r;
        g = g < 0 ? 0 : g > MAX_RGB ? 255 : g;
        b = b < 0 ? 0 : b > MAX_RGB ? 255 : b;
        return Color.rgb((int)r, (int)g, (int)b);
    }

    /**
     * The previously predicted values.
     */
    private ArrayList<Double> m_Values = new ArrayList<>();

    /**
     * Update the prediction panel with a new prediction.
     *
     * @param value The new prediction value.
     */
    public void update(double value) {
        m_Values.add(value);
        Platform.runLater(() -> {
            m_Gauge.setValue(value * 100);
            m_Series.getData().add(new XYChart.Data<>(m_Series.getData().size(), value * 100));
        });
    }

    /**
     * Simple simulation method for testing purposes.
     */
    public void simulate() {
        final Random random = new Random();
        Thread thread = new Thread(() -> {
           while (true) {
               update(random.nextDouble());
               try {
                   Thread.sleep(1000);
               } catch (Exception e) {
                   e.printStackTrace();
               }
           }
        });
        thread.start();
    }
}
