package com.research.wuil.gui.layout.console.io;

import com.research.wuil.gui.layout.console.FilteredConsolePane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ConsoleApplication extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FilteredConsolePane pane = new FilteredConsolePane(10);
        primaryStage.setScene(new Scene(pane));
        for (int i = 0; i < 100; i++) {
            pane.log("Default", "Logging " + i);
        }
        primaryStage.show();
    }
}
