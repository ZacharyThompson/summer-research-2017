package com.research.wuil.gui.layout;

import com.research.wuil.gui.layout.selection.GenericSelectionButton;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import weka.classifiers.Classifier;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.meta.OneClassClassifier;
import weka.filters.StringToFileEvents;

import java.util.HashMap;

/**
 *
 * The pane that allows the user to define the m_Settings for the connection
 * and the file monitoring and classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ClientSettingsPane extends GridPane {
    /**
     * Maps a name of a setting to an input for that setting
     */
    private HashMap<String, Node> m_Settings = new HashMap<>();

    /**
     * Constructor.
     *
     * @param event An event to trigger when a connection is established
     */
    public ClientSettingsPane(EventHandler event) {
        addSetting("IP Address", "127.0.0.1");
        addSetting("Port", "8080");
        addSetting("Target Environment", "/home/zac/Documents/Monitored Environment/");
        addSetting("OSQuery Interval", "60");

        GenericSelectionButton btn = new GenericSelectionButton(Classifier.class);
        FilteredClassifier filteredClassifier = new FilteredClassifier();
        filteredClassifier.setFilter(new StringToFileEvents());
        OneClassClassifier occ = new OneClassClassifier();
        occ.setClassifier(new IBk());
        filteredClassifier.setClassifier(occ);
        btn.setSelection(filteredClassifier);

        addNode("Classifier", btn);
        addConnect(event);

        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(40);
        ColumnConstraints c2 = new ColumnConstraints();
        c2.setPercentWidth(60);

        this.getColumnConstraints().addAll(c1, c2);
        this.setVgap(5);
        this.setHgap(5);
        this.setPadding(new Insets(5));
    }

    /**
     * Add a setting to the setting hash map.
     *
     * @param name The name of the setting
     * @param value The default value of the setting.
     */
    private void addSetting(String name, String value) {
        TextField txt = new TextField();
        txt.setText(value);
        Label lbl = new Label(name);

        this.add(lbl, 0, m_Settings.size());
        this.add(txt, 1, m_Settings.size());

        m_Settings.put(name, txt);
    }

    /**
     * Add a node to the setting hash map.
     *
     * @param name The name of the setting
     * @param node The node that is used as a control for the setting.
     */
    private void addNode (String name, Node node) {
        this.add(new Label(name), 0, m_Settings.size());
        this.add(node, 1, m_Settings.size());

        m_Settings.put(name, node);
    }

    /**
     * Add a connect button with a given event handler to trigger
     * upon connection request.
     *
     * @param event Event to trigger on connection.
     */
    private void addConnect(EventHandler event) {
        Button connect = new Button("Connect");
        this.add(connect, 1, m_Settings.size());
        GridPane.setHalignment(connect, HPos.RIGHT);
        connect.setOnMouseClicked(event);
    }

    /**
     * Get the value of a setting.
     * If the value is text, the returned Object will be a String.
     * If the value is a Classifier,the returned Object will be the Classifier.
     *
     * @param key Key to locate a setting value for.
     * @return The value associated with the given key in the setting map.
     */
    public Object getParameter(String key) {
        if (m_Settings.containsKey(key)) {
            Node node = m_Settings.get(key);
            if (node instanceof TextField) {
                return ((TextField) node).getText();
            } else if (node instanceof GenericSelectionButton) {
                return ((GenericSelectionButton) node).getSelection();
            } else {
                return node;
            }
        }
        return "";
    }
}
