package com.research.wuil.gui.layout.selection.dev;

public class ClassifierItem {
    private String name;

    public ClassifierItem(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            return o.toString().equals(name);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}
