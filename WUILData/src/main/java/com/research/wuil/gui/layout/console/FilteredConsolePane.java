package com.research.wuil.gui.layout.console;

import com.research.wuil.gui.layout.console.io.FilteredTextAreaOutputStream;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Filtered text console pane used to simulate a traditional console in a JavaFX environment.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FilteredConsolePane extends GridPane {
    /**
     * The PrintStream for which output is written to.
     */
    public PrintStream m_Out;

    /**
     * Collection of input handlers which control what is done with data inputted.
     */
    private List<InputHandler> m_InputHandlers = new ArrayList<>();

    /**
     * The FilteredTextAreaOutputStream for the console pane.
     */
    private FilteredTextAreaOutputStream m_Taos;

    /**
     * Constructor.
     *
     * @param maxLines The maximum lines of input
     * @param filters Filters to be used in the FilteredTextAreaOutputSteam
     */
    public FilteredConsolePane(int maxLines, String ... filters) {
        TextArea taOut = createConsoleOut();
        TextField tfIn = createConsoleIn();
        this.add(taOut, 0, 0);
        this.add(tfIn, 0, 1);

        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(100);

        RowConstraints row = new RowConstraints();
        row.setFillHeight(true);
        row.setVgrow(Priority.ALWAYS);

        this.getColumnConstraints().add(col);
        this.getRowConstraints().add(row);

        m_Taos = new FilteredTextAreaOutputStream(taOut, maxLines, filters);
        m_Out = new PrintStream(m_Taos);

        m_InputHandlers.add((input, i) -> {
            if (i >= 1) m_Out.println(input);
        });
    }


    /**
     * Constructor.
     *
     * @param filters Filters to be used in the FilteredTextAreaOutputSteam
     */
    public FilteredConsolePane(String ... filters) {this (500, filters); }

    /**
     * Constructor. Default filters for the Server application.
     */
    public FilteredConsolePane() {
        this(500);
    }

    /**
     * Constructor. Default filters for the Server application.
     *
     * @param maxLines The maximum number of lines to display
     */
    public FilteredConsolePane(int maxLines) {
        this(maxLines,"Client", "Evaluation", "Default");
    }

    /**
     * Helper method used to create a TextArea to be used as the Output for the console.
     *
     * @return TextArea for console output.
     */
    private TextArea createConsoleOut() {
        TextArea taOut = new TextArea();
        taOut.setEditable(false);
        taOut.setFocusTraversable(false);
        taOut.setWrapText(true);

        return taOut;
    }

    /**
     * Helper method used to create a TextField to be used as the Input for the console.
     *
     * @return TextField for console input.
     */
    private TextField createConsoleIn() {
        TextField tfIn = new TextField();
        tfIn.setOnKeyPressed((keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                println(tfIn.getText().trim());
                tfIn.setText("");
            }
        });
        return tfIn;
    }

    /**
     * Prints line to console with level 3 intent.
     *
     * @param txt Text to print.
     */
    public void println(String txt) {
        println(txt, 3);
    }

    /**
     * Prints line to console with a given level of intent.
     *
     * @param txt Text to print.
     * @param level Level of intent.
     */
    public void println(String txt, int level) {
        if (m_Out != null) {
            for (InputHandler inputHandler : m_InputHandlers) {
                inputHandler.handleInput(txt, level);
            }
        }
    }

    /**
     * Send a clear request to the Filtered Console.
     */
    public void clear() {
        m_Taos.clear();
    }

    /**
     * Add input handler to the console input object.
     *
     * @param inputHandler InputHandler implementation to handle inputted data.
     */
    public void addInputHandler(InputHandler inputHandler) {
        Platform.runLater(() -> {
            m_InputHandlers.add(inputHandler);
        });
    }

    /**
     * Log data to the Filtered Console.
     *
     * @param filter Filter to be used.
     * @param value Value to be written.
     */
    public void log(String filter, String value) {
        m_Taos.log(filter, value);
    }

    /**
     * Interface for the input handling.
     */
    public interface InputHandler {
        public void handleInput(String input, int level);
    }
}
