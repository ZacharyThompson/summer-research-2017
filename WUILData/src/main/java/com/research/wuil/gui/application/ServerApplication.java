package com.research.wuil.gui.application;

import com.research.wuil.gui.Server;
import com.research.wuil.gui.layout.ConnectionPane;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import com.research.wuil.utils.ResourceManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.util.List;

/**
 *
 * The Application to be used by the server.
 * Handles communication with the client and performs classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ServerApplication extends Application {
    /**
     * The tab m_Pane to house the tabs associated with the application.
     */
    private TabPane m_TabPane = new TabPane();

    /**
     * The console m_Pane to handle input and output of dialog.
     */
    private FilteredConsolePane m_ConsolePane;

    /**
     * Constructor.
     */
    public ServerApplication() {
        Tab tab = new Tab("Server Console");
        ConnectionPane connectionPane = new ConnectionPane(null,ResourceManager.getServer().getRoot());
        tab.setContent(connectionPane);
        tab.setClosable(false);

        m_ConsolePane = connectionPane.getConsolePane();
        m_TabPane.getTabs().add(tab);

        Server server = new Server(this);
        Thread serverThread = new Thread(server);
        serverThread.start();
    }

    /**
     * Starts the application.
     *
     * @param primaryStage Stage provided for the application.
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Server");
        primaryStage.setScene(new Scene(m_TabPane, 1000, 800));
        primaryStage.show();
    }

    /**
     * Adds tab to tab m_Pane.
     *
     * @param tab Tab to be added.
     */
    public synchronized void addTab(Tab tab) {
        Platform.runLater(new Thread (()-> {
            m_TabPane.getTabs().add(tab);
        }));
    }

    /**
     * Add tabs to tab m_Pane.
     *
     * @param tabs Tabs to be added.
     */
    public void addTabs(List<Tab> tabs) {
        for (Tab tab : tabs) {
            this.addTab(tab);
        }
    }

    /**
     * Returns a collection of tabs currently associated with this application.
     *
     * @return Collection of tabs.
     */
    public List<Tab> getTabs() {
        return m_TabPane.getTabs();
    }

    /**
     * Gets the console m_Pane.
     *
     * @return The console m_Pane.
     */
    public FilteredConsolePane getConsole() {
        return m_ConsolePane;
    }

    /**
     * Adds a new connection m_Pane to correspond to a new connection being established.
     *
     * @param pane The connection m_Pane.
     */
    public synchronized void addConnection(ConnectionPane pane) {
        Tab tab = new Tab(pane.getName());
        tab.setContent(pane);
        tab.setOnCloseRequest((e) -> {
            pane.close();
        });
        this.addTab(tab);
    }
}
