package com.research.wuil.gui.layout.selection;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.apache.commons.lang.StringUtils;
import weka.core.OptionMetadata;
import weka.gui.ProgrammaticProperty;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * Pane used to adjust the various properties of a given object
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class PropertyPane extends VBox {
    /**
     * The target class
     */
    private Class m_Target;

    /**
     * The Object to be modified.
     */
    private Object m_Object;

    /**
     * The grid pane that houses the controls
     */
    private GridPane m_GridPane = new GridPane();

    /**
     * Collection of property modifies; one modifier for each property.
     */
    private List<PropertyModifier> m_Modifiers = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param o The Object to be modified.
     */
    public PropertyPane(Object o) {
        super();
        m_Target = o.getClass();
        m_Object = o;
        int i = 0;
        try {
            // Get the properties for the class of the given Object.
            BeanInfo bi = Introspector.getBeanInfo(m_Target);
            ArrayList<PropertyDescriptor> properties =
                    new ArrayList<>(Arrays.asList(bi.getPropertyDescriptors()));

            // Get the methods for the class of the given Object.
            ArrayList<MethodDescriptor> methods =
                    new ArrayList<>(Arrays.asList(bi.getMethodDescriptors()));
            ArrayList<MethodDescriptor> tipTexts = new ArrayList<>();
            for (MethodDescriptor m : methods) {
                if (StringUtils.endsWithIgnoreCase (m.getName(), "TipText")) {
                    tipTexts.add(m);
                }
            }

            try {
                Method method = m_Target.getMethod("globalInfo");
                String description = method.invoke(m_Object).toString();
                TextArea text = new TextArea(description);
                text.setWrapText(true);
                text.setMaxHeight(100);
                text.setEditable(false);
                this.getChildren().add(text);
            } catch (Exception e) {
                System.err.println("globalInfo not found");
            }

            // Check each property and ensure it is valid for modification
            for (PropertyDescriptor property : properties) {
                // If the property is hidden or expert, don't allow manipulation
                if (property.isHidden() || property.isExpert()) {
                    continue;
                }

                // Get the name, getter and setter for the property.
                String name = property.getDisplayName();
                Method getter = property.getReadMethod();
                Method setter = property.getWriteMethod();

                // If the property does not have both a getter and setter, continue
                if (getter == null || setter == null) continue;

                // Check the annotations
                ArrayList<Annotation> annotations = new ArrayList<>();
                if (setter.getDeclaredAnnotations().length > 0) {
                    annotations.addAll(Arrays.asList(setter.getDeclaredAnnotations()));
                }
                if (getter.getDeclaredAnnotations().length > 0) {
                    annotations.addAll(Arrays.asList(getter.getDeclaredAnnotations()));
                }

                boolean skip = false;
                for (Annotation a : annotations) {
                    if (a instanceof ProgrammaticProperty) {
                        skip = true;
                        break;
                    } else if (a instanceof OptionMetadata) {
                        name = ((OptionMetadata) a).displayName();
                        String tempTip = ((OptionMetadata)a).description();
                    }
                }
                if (skip) {
                    continue;
                }

                // Create a property modifier to allow the user to adjust this property.
                PropertyModifier modifier = new PropertyModifier(m_Object, property);
                // If the modifier is not valid, continue.
                if (!modifier.isValid()) continue;

                // Find tip text
                String tipText = "";
                for (MethodDescriptor method : tipTexts) {
                    if (method.getName().equalsIgnoreCase(name + "TipText")) {
                        tipText = method.getMethod().invoke(m_Object).toString();
                        tipTexts.remove(method);
                        break;
                    }
                }
                // Add the modifier to the modifiers list
                m_Modifiers.add(modifier);
                // Configure and add to grid pane
                modifier.getInput().prefWidthProperty().bind(m_GridPane.widthProperty());
                Label lblName = new Label(name);
                lblName.setTooltip(new Tooltip(tipText));
                m_GridPane.add(lblName, 0, i);
                m_GridPane.add(modifier.getInput(), 1, i++);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Configure JavaFX settings for the controls and panes
        this.setPadding(new Insets(5));
        this.getChildren().add(m_GridPane);
        m_GridPane.prefWidthProperty().bind(this.widthProperty());

        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(50);
        ColumnConstraints c2 = new ColumnConstraints();
        c2.setPercentWidth(50);

        m_GridPane.getColumnConstraints().addAll(c1, c2);
        m_GridPane.setVgap(5);
        m_GridPane.setHgap(5);
        m_GridPane.setPadding(new Insets(5, 0,0,0));
    }

    /**
     * Save each of the property values
     */
    public void save() {
        for (PropertyModifier modifier : m_Modifiers) {
            modifier.saveValue();
        }
    }

    /**
     * Reset each of the property values
     */
    public void reset() {
        for (PropertyModifier modifier : m_Modifiers) {
            modifier.loadValue();
        }
    }
}
