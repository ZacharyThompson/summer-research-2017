package com.research.wuil.gui.layout.selection;

import com.research.wuil.utils.OptionSerializer;
import javafx.scene.control.*;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

/**
 *
 * The GenericSelectionButton allows the user to select a class that implements a specified interface
 * and configure the class. This is very basic and lightweight approach to classifier, filter and etc
 * selection implemented in the Weka GUI.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class GenericSelectionButton extends HBox {
    /**
     * The root Class of the GenericSelectionButton for which all possibilities must extend.
     */
    private Class m_Root;

    /**
     * The TreeView for selecting the desired Object.
     */
    private TreeView<String> m_TreeView;

    /**
     * The Root item which represents the top layer of the package the root item is in (Represents
     * packages hierarchically).
     */
    private PackageTreeItem m_RootItem;

    /**
     * The class of the current selected item.
     */
    private Class<?> m_SelectedClass;

    /**
     * The Object of the current selected item.
     */
    private Object m_SelectedObject;

    /**
     * The property pane displayed to allow the user to adjust properties of the selected class.
     */
    private PropertyPane m_PropertyPane;

    /**
     * The TextField that holds the selected item and allows configuring the model.
     */
    private TextField m_TextField;

    /**
     * Constructor.
     *
     * @param root The root class
     */
    public GenericSelectionButton(Class<?> root) {
        this (root, true);
    }

    /**
     * Constructor.
     *
     * @param root The root class.
     * @param showButton Whether the choose button should be hidden.
     */
    public GenericSelectionButton(Class<?> root, boolean showButton) {
        m_Root = root;
        m_TreeView = new TreeView<>();
        m_RootItem = new PackageTreeItem(root.getPackage());

        // Get all classes that inherit the root and add a branch for it.
        ArrayList classes = getClasses(root);
        for (Object o : classes) {
            if (o instanceof Class) {
                m_RootItem.addBranch((Class) o);
            }
        }
        sort(m_RootItem);
        m_TreeView.setRoot(m_RootItem);
        CustomMenuItem menuItem = new CustomMenuItem(m_TreeView);
        menuItem.setHideOnClick(false);

        // Create Text Field
        m_TextField = new TextField();
        m_TextField.setEditable(false);
        m_TextField.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2) edit();
        });
        m_TextField.setContextMenu(getContextMenu());

        // On double click, change the selected item
        m_TreeView.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2) {
                if (m_TreeView.getSelectionModel().getSelectedItem() instanceof ClassTreeItem) {
                    ClassTreeItem item = (ClassTreeItem) m_TreeView.getSelectionModel().getSelectedItem();
                    if (m_SelectedClass == null || !m_SelectedClass.equals(item.getClassType())) {
                        m_SelectedClass = ((ClassTreeItem) m_TreeView.getSelectionModel().getSelectedItem()).getClassType();
                        try {
                            setSelection(m_SelectedClass.newInstance());
                        } catch (Exception e) {
                            System.err.println(String.format("Could set selected Object [%s]", m_SelectedClass));
                        }
                    }
                }
            }
        });
        m_TextField.prefWidthProperty().bind(this.widthProperty());
        this.setPrefWidth(Double.MAX_VALUE);

        if (showButton) {
            MenuButton button = new MenuButton("Choose");
            button.setMinWidth(75);
            button.getItems().add(menuItem);
            this.getChildren().addAll(m_TextField, button);
        } else {
            this.getChildren().add(m_TextField);
        }
    }

    /**
     * Get the Object that has currently been selected. The Object will have the options defined
     * in the Property pane.
     *
     * @return The selected Object.
     */
    public Object getSelection() {
        return m_SelectedObject;
    }

    /**
     * Sets a given Object to be the currently selected item.
     *
     * @param o Object to be set as selected.
     */
    public void setSelection(Object o) {
        m_SelectedObject = o;
        m_PropertyPane = new PropertyPane(m_SelectedObject);
        m_TextField.setText(m_SelectedObject.getClass().getSimpleName());
    }

    /**
     * Sorting Tree function. This recursive method sorts the children of a TreeItem before sorting the TreeItem
     * itself. It sorts by whether or not the Child is a leaf.
     *
     * @param item Tree item to sort.
     * @param <T> The type parameter of the TreeItem.
     */
    private static <T> void sort(TreeItem<T> item) {
        for (TreeItem<T> i : item.getChildren()) {
            sort(i);
        }
        item.getChildren().sort(Comparator.comparing(TreeItem::toString));
        item.getChildren().sort(Comparator.comparing(TreeItem::isLeaf));
    }

    /**
     * Get all classes that inherit from the specified class as well as exist within a sub-package.
     *
     * @param root The root class for which all found classes must inherit.
     * @param <T> The type parameter of the class.
     * @return Collection of all classes that inherit from the specified root.
     */
    private static <T> ArrayList<Class<? extends T>> getClasses(Class<T> root) {
        Reflections reflections = new Reflections(root.getPackage().getName(), new SubTypesScanner(false));
        Set<Class<? extends T>> allClasses = reflections.getSubTypesOf(root);
        allClasses.removeIf(Class::isInterface);
        allClasses.removeIf((a) -> Modifier.isAbstract(a.getModifiers()));
        return new ArrayList<>(allClasses);
    }

    /**
     * Edit the model by launching a property modification dialog.
     */
    private void edit() {
        if (m_SelectedObject == null) return;

        // Create dialog for the property pane
        DialogPane dialogPane = new DialogPane();
        dialogPane.setContent(m_PropertyPane);
        dialogPane.getButtonTypes().add(ButtonType.OK);
        dialogPane.getButtonTypes().add(ButtonType.CANCEL);
        dialogPane.setPrefWidth(500);

        Dialog<Boolean> dialog = new Dialog<>();
        dialog.setTitle(m_SelectedObject.getClass().getSimpleName());
        dialog.setWidth(500);
        dialog.setDialogPane(dialogPane);
        dialog.setResultConverter(buttonType -> {
            // Save the values if the return Button was OK before loading values
            // (removes any user input error)
            if (buttonType == ButtonType.OK) {
                m_PropertyPane.save();
            }
            m_PropertyPane.reset();
            return true;
        });
        dialog.showAndWait();
    }

    /**
     * Constructs a context menu for the text field.
     * Allows the user to copy and paste a model configuration (currently using default
     * java serialization as it is more versatile) and allows the user to edit.
     *
     * @return The context menu providing the various controls.
     */
    private ContextMenu getContextMenu() {
        ContextMenu menu = new ContextMenu();
        MenuItem copy = new MenuItem("Copy");
        final ClipboardContent content = new ClipboardContent();
        copy.setOnAction(e -> {
            content.putString(OptionSerializer.serialize(m_SelectedObject));
        });
        MenuItem paste = new MenuItem("Paste");
        paste.setOnAction(e -> {
            if (content.hasString()) {
                Object o = OptionSerializer.deserialize(content.getString());
                if (m_Root.isAssignableFrom(o.getClass())) {
                    this.setSelection(o);
                }
            }
        });
        MenuItem edit = new MenuItem("Edit");
        edit.setOnAction(e -> {
            edit();
        });
        menu.getItems().addAll(copy, paste, edit);
        return menu;
    }
}
