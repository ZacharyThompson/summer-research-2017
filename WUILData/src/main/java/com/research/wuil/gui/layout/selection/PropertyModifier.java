package com.research.wuil.gui.layout.selection;

import com.research.wuil.gui.layout.selection.input.GenericInput;
import javafx.scene.layout.Region;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 *
 * Object used to associate a getter and setter with an input to allow
 * a user interface to manipulate the properties of a class.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class PropertyModifier {
    /**
     * The descriptor of the property this Object is used to manipulate.
     */
    private PropertyDescriptor m_Property;

    /**
     * Getter method used to get the value of the specific property this
     * Object is used to manipulate.
     */
    private Method m_Getter;

    /**
     * Setter method used to set the value of a specific property this
     * Object is used to manipulate.
     */
    private Method m_Setter;

    /**
     * The GenericInput field used to retrieve an input from the user
     */
    private GenericInput m_Input;

    /**
     * The Object that houses the property this Object is used to manipulate.
     * The getter and setter are invoked on this Object to manipulate the
     * property value.
     */
    private Object m_Object;

    /**
     * Constructor.
     *
     * @param object The Object that houses the property to be manipulated.
     * @param property The descriptor of the property to be manipulated.
     */
    public PropertyModifier(Object object, PropertyDescriptor property) {
        m_Object = object;
        m_Property = property;
        m_Getter = property.getReadMethod();
        m_Setter = property.getWriteMethod();
        m_Input = GenericInput.getGenericInput(m_Object, m_Property);
        loadValue();
    }

    /**
     * Checks whether the property is manipulable; which is determine by
     * the existence of an input field.
     *
     * @return Whether the property is manipulable.
     */
    public boolean isValid() {
        return m_Input != null;
    }

    /**
     * Gets the input used to retrieve a value from the user to set the
     * property.
     *
     * @return The input region
     */
    public Region getInput() {
        return m_Input.getInput();
    }

    /**
     * Tries to load a value from the Object to the input region.
     */
    public void loadValue() {
        try {
            m_Input.setValue(m_Getter.invoke(m_Object));
        } catch (Exception e) {
            System.err.println(String.format("Could not load value [%s]", m_Property.getName()));
        }
    }

    /**
     * Tries to save a value from the input region to the Object.
     */
    public void saveValue() {
        try {
            m_Setter.invoke(m_Object, m_Input.getValue());
        } catch (Exception e) {
            System.err.println(String.format("Could not save value [%s : %s]", m_Property.getName(), m_Input.getValue()));
        }
    }


}