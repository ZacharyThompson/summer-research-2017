package com.research.wuil.gui.layout.console;

/**
 *
 * Log to be used in conjunction with the Filtered Output in which a
 * filter is used to determine whether the line is displayed,
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FilteredLog {
    /**
     * Filter for this FilteredLog instance.
     */
    private String m_Filter;

    /**
     * Line for this FilteredLog instance.
     */
    private String line;

    /**
     * Constructor.
     *
     * @param filter Filter associated with the line of input.
     * @param line Line of input.
     */
    public FilteredLog(String filter, String line) {
        this.m_Filter = filter;
        this.line = line;
    }

    /**
     * Get the line of input.
     *
     * @return Line of input.
     */
    public String getLine() { return line; }

    /**
     * Get the filter associated with the line of input.
     *
     * @return Filter.
     */
    public String getFilter() { return m_Filter; }
}
