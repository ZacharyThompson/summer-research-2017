package com.research.wuil.gui.application;

import com.research.wuil.gui.layout.PredictionPane;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * An application for the illustration of the prediction pane. Testing purposes.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class PredictionApplication extends Application {
    /**
     * The prediction pane
     */
    private PredictionPane m_Pane = new PredictionPane();

    /**
     * Set up to be executed on the JavaFX ui thread.
     *
     * @param primaryStage The primary stage.
     */
    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(m_Pane, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Update the prediction pane
     *
     * @param value The value to update the prediction pane
     */
    public void update(double value) {
        m_Pane.update(value);
    }

    /**
     * Entry point to the application.
     *
     * @param args Command line arguments.
     */
    public static void main (String args[]) {
        PredictionApplication pane = new PredictionApplication();
        Platform.runLater(() -> {
            try {
                pane.init();
                pane.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        pane.m_Pane.simulate();
    }
}
