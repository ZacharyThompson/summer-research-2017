package com.research.wuil.gui.layout.selection;

import javafx.scene.control.TreeItem;

/**
 *
 * TreeItem for a Class Object.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ClassTreeItem extends TreeItem<String> {
    /**
     * Class that this TreeItem represents
     */
    private Class m_Class;

    /**
     * Constructor
     *
     * @param item Class to build Object from.
     */
    public ClassTreeItem(Class item) {
        super(item.getSimpleName());
        m_Class = item;
    }

    /**
     * Compares equality between a given Object and itself.
     *
     * @param o Object to compare equality to.
     * @return Whether equal or not.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Class) {
            return this.m_Class.equals(o);
        } else if (o instanceof ClassTreeItem) {
            return this.m_Class.equals(((ClassTreeItem) o).m_Class);
        } else if (o instanceof String) {
            return this.m_Class.getName().equals(o);
        } else if (o instanceof TreeItem) {
            return this.getValue().equals(((TreeItem) o).getValue());
        }
        return false;
    }

    /**
     * HashCode for this Object is equal to that of the values hashcode.
     *
     * @return Hashcode.
     */
    @Override
    public int hashCode() {
        return this.getValue().hashCode();
    }

    /**
     * Get the Class type.
     *
     * @return Class type
     */
    public Class getClassType() {
        return m_Class;
    }
}