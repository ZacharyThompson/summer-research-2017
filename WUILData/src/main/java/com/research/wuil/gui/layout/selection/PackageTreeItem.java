package com.research.wuil.gui.layout.selection;

import javafx.scene.control.TreeItem;

/**
 *
 * TreeItem for a Package Object.
 *
 * @author Zachary Thompson
 */
public class PackageTreeItem extends TreeItem<String> {
    /**
     * Constructor.
     *
     * @param item Package for this Object to represent.
     */
    public PackageTreeItem(Package item) {
        this(item.getName());
    }

    /**
     * Constructor.
     *
     * @param name Name of this package level.
     */
    public PackageTreeItem(String name) {
        super(name.split("\\.")[0]);
        this.addBranch(name);
        this.setExpanded(true);
    }

    /**
     * Constructor.
     *
     * @param name Name of this package level.
     * @param leaf Leaf item for this package.
     */
    public PackageTreeItem(String name, TreeItem leaf) {
        super(name.split("\\.")[0]);
        this.addBranch(name, leaf);
    }

    /**
     * Compares equality of this Object and a given Object.
     *
     * @param o Object to compare equality to.
     * @return Whether the Objects are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof TreeItem) {
            return ((TreeItem) o).getValue().equals(this.getValue());
        }
        return false;
    }

    /**
     * Get the hash code of this Object. Implemented to ensure contract of equality
     * is maintained.
     *
     * @return Hash code of this Object.
     */
    @Override
    public int hashCode() {
        return this.getValue().hashCode();
    }

    /**
     * Add a branch to this TreeItem for each level of a given Package.
     *
     * @param p Package used to form a branch.
     */
    public void addBranch(Package p) {
        addBranch(p.getName());
    }

    /**
     * Add a branch for a given path.
     *
     * @param path Path to form a branch for.
     */
    public void addBranch(String path) {
        this.addBranch(path, null);
    }

    /**
     * Add a branch for a given Class, having the Class as a ClassTreeItem and as a leaf.
     *
     * @param c Class to form a branch for.
     */
    public void addBranch(Class c) {
        this.addBranch(c.getPackage().getName(), new ClassTreeItem(c));
    }

    /**
     * Adds a branch at a given path for a leaf node. recursive function used to form a
     * branch for each level of a path, assuming hierarchical order.
     *
     * @param path Path to form a branch from
     * @param leaf Leaf to add to the end of the branch
     * @return Whether it was added or not.
     */
    public boolean addBranch(String path, TreeItem<String> leaf) {
        if (path == null || path.equals("")) {
            return false;
        }
        String[] paths = path.split("\\.");
        if (paths[0].equals(this.getValue())) {
            String next = path.substring(path.indexOf(".") + 1, path.length());
            if (next.equals(path)) {
                if (leaf != null) {
                    this.getChildren().add(leaf);
                }
                return true;
            }
            boolean add = false;
            for (TreeItem child : this.getChildren()) {
                if (child instanceof PackageTreeItem) {
                    add |= ((PackageTreeItem) child).addBranch(next, leaf);
                }
            }
            if (!add) {
                this.getChildren().add(new PackageTreeItem(next, leaf));
            }
            return true;
        }
        return false;
    }
}