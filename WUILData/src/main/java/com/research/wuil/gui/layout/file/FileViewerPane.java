package com.research.wuil.gui.layout.file;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.research.wuil.classification.classifier.FileEventOneClassIBK;
import com.research.wuil.classification.classifier.RealTimeClassifier;
import com.research.wuil.classification.data.FileEventInstances;
import com.research.wuil.classification.data.FileEventObservation;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.apache.commons.collections4.ListUtils;
import org.hildan.fxgson.FxGson;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.core.Instances;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.*;

/**
 *
 *
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
        */
public class FileViewerPane extends VBox {
    /**
     * GSON FX Object for reading and writing the config file
     */
    private static final Gson FX_GSON = FxGson.fullBuilder().setPrettyPrinting().create();

    /**
     * Type of the config Object so it can be written to and from non-volatile memory.
     */
    private static final Type CONFIG_TYPE = new TypeToken<ArrayList<FileObject>>() {}.getType();

    /**
     * Root file for the TreeViewPane
     */
    private File m_Root;

    /**
     * The TreeView control
     */
    private TreeView<FileObject> m_TreeView;

    /**
     * ConsolePane for communicating with the front-end.
     */
    private FilteredConsolePane m_ConsolePane;

    /**
     * Configuration File that holds the classes associated with each file.
     */
    private File m_ConfigFile;

    /**
     * List of files that are blocked; ie that cannot be used as training.
     */
    private List<FileObject> m_BlockedFiles = new ArrayList<>();

    /**
     * The Real Time Classifier. Defaults to OCC IBK, though this can be configured by the Client.
     */
    private RealTimeClassifier m_RealTimeClassifier = new RealTimeClassifier(new FileEventOneClassIBK());

    /**
     * Constructor
     *
     * @param root The root file.
     * @param consolePane The Console Pane of the user.
     * @param blockedFiles List of blocked files.
     */
    public FileViewerPane(File root, FilteredConsolePane consolePane, List<FileObject> blockedFiles) {
        this.m_Root = root;
        this.m_ConsolePane = consolePane;
        this.m_BlockedFiles.addAll(blockedFiles);
        m_ConfigFile = new File(root, "classes.conf");
        refresh();
    }

    /**
     * Constructor.
     *
     * @param root The root file.
     * @param consolePane The Console Pane of the user.
     */
    public FileViewerPane(File root, FilteredConsolePane consolePane) {
        this.m_Root = root;
        this.m_ConsolePane = consolePane;
        m_ConfigFile = new File(root, "classes.conf");
        refresh();
    }

    /**
     * Refresh the pane, tearing down all implemented framework and readding it all. Used for when
     * significant changes are made to Objects and files; this will account for those. This can be expensive,
     * hence a new thread is used to avoid blocking the UI thread; The UI thread is respected and changes to
     * it are account for by executing those on the UI thread.
     */
    private void refresh() {
        Thread th = new Thread(new Task() {
            @Override
            protected Object call() throws Exception {
                TreeView<FileObject> newTreeView = new TreeView<>();

                List<FileObject> config = readFileObjectConfig(m_ConfigFile);
                ArrayList<TreeItem<FileObject>> files = getTreeItemListConfig(m_Root, config);

                TreeItem<FileObject> rootItem = (files.get(0) == null) ? new TreeItem<>(new FileObject()) : files.get(0);
                rootItem.setExpanded(true);
                newTreeView.setRoot(rootItem);
                newTreeView.setPrefHeight(10000);
                newTreeView.setCellFactory((param) ->  new FileTreeCell());

                Pane buttons = getButtons();


                Platform.runLater(()-> {
                    getChildren().clear();
                    getChildren().add(newTreeView);
                    getChildren().add(buttons);
                    m_TreeView = newTreeView;
                    writeFileObjectConfig(m_ConfigFile);
                });
                return null;
            }
        });
        th.start();
    }

    /**
     * Read the FileObject configuration form File.
     *
     * @param file File to read from.
     * @return Collection of FileObjects.
     */
    private List<FileObject> readFileObjectConfig(File file) {
        String output = "Reading configuration file ... ";
        List<FileObject> config = new ArrayList<>();
        try (FileReader reader = new FileReader(file)) {
            config = FX_GSON.fromJson(reader, CONFIG_TYPE);
            output += "success";
        } catch (Exception e) {
            output += "failed";
        }
        m_ConsolePane.log("Default", output);
        return ListUtils.sum(config, m_BlockedFiles);
    }

    /**
     * Write current data to a configuration File.
     *
     * @param file File to write to.
     */
    private void writeFileObjectConfig(File file) {
        String output = "Writing configuration file ... ";
        List<FileObject> objects = getAllFileObjects(m_TreeView.getRoot());
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(FX_GSON.toJson(objects));
            writer.flush();
            output += "success";
        } catch (Exception e) {
            output += "failed";
        }
        m_ConsolePane.log("Default", output);
    }

    /**
     * Get all FileObjects from the tree view if and only if they are valid files. Recursive search.
     *
     * @param root The root TreeItem.
     * @return Collection of FileObjects.
     */
    private ArrayList<FileObject> getAllFileObjects(TreeItem<FileObject> root) {
        ArrayList<FileObject> list = new ArrayList<>();
        if (root.getValue().isFile()) {
            list.add(root.getValue());
        } else {
            for (TreeItem<FileObject> child : root.getChildren()) {
                list.addAll(getAllFileObjects(child));
            }
        }
        return list;
    }

    /**
     * Get Collection of TreeItems for the TreeView using the configuration file to define how a FileObject
     * is instantiated. Searches for all files within the root directory, recursively.
     *
     * @param root The root File
     * @param config The configuration collection.
     * @return The collection of TreeItems.
     */
    private ArrayList<TreeItem<FileObject>> getTreeItemListConfig(File root, List<FileObject> config) {
        FileObject rootObject = new FileObject(root);
        if (root.isFile() && !m_BlockedFiles.contains(rootObject) && root.getPath().endsWith(".log") && root.length() == 0) {
            root.delete();
            return new ArrayList<>();
        }

        int index = config.indexOf(rootObject);
        if (index != -1) rootObject = config.get(index);

        ArrayList<TreeItem<FileObject>> items = new ArrayList<>();
        TreeItem<FileObject> item = new TreeItem<>(rootObject);
        items.add(item);

        File files[] = root.listFiles();
        if (files != null) {
            for (File child : files) {
                item.getChildren().addAll(getTreeItemListConfig(child, config));
            }
        }
        return items;
    }

    /**
     * Helper method used to create buttons which are added to a pane and equallty distributed.
     *
     * @return Pane of buttons.
     */
    private Pane getButtons() {
        ArrayList<Button> buttons = new ArrayList<>();
        GridPane grid = new GridPane();

        Button button = new Button("Save Config");
        button.setOnAction(event -> writeFileObjectConfig(m_ConfigFile));
        buttons.add(button);

        button = new Button("Refresh");
        button.setOnAction(event -> refresh());
        buttons.add(button);

        button = new Button("Classify Unknown");
        button.setOnMouseClicked(event -> {
            new Thread(() -> {
                classify();
            }).start();
        });
        buttons.add(button);

        for (int i = 0; i < buttons.size(); i++) {
            ColumnConstraints col = new ColumnConstraints();
            col.setPercentWidth(100.0 / buttons.size());
            grid.getColumnConstraints().add(col);

            button = buttons.get(i);
            button.setMaxWidth(Double.MAX_VALUE);
            grid.add(button , i, 0);
        }

        grid.setPrefWidth(Double.MAX_VALUE);
        return grid;
    }

    /**
     * Load the training data into memory from those files specified in this FileViewPane.
     *
     * @return FileEventInstances representative of the training data.
     */
    private Instances loadTrainData() {
        Instances instances = null;
        for (FileObject object : getAllFileObjects(m_TreeView.getRoot())) {
            if (object.isTrainData()) {
                instances = append(instances, FileEventObservation.RawInstancesFromFile(object.getFile(), object.getLabel()));
            }
        }
        return instances;
    }

    /**
     * Appends an instance set to another
     *
     * @param a First instance set
     * @param b Second instance set
     * @return The appended set
     */
    private static Instances append(Instances a, Instances b) {
        if (a == null) return b;
        if (b == null) return a;
        Instances instances = new Instances(a, a.size() + b.size());
        instances.addAll(0, a);
        instances.addAll(a.size(), b);
        return instances;
    }

    /**
     * Classify a given set of instances.
     *
     * @param instances Instances to classify.
     */
    public void classify(Instances instances) {
        if (m_RealTimeClassifier.isBuilt()) {
            m_RealTimeClassifier.predict(instances);
            return;
        }
        boolean build = m_RealTimeClassifier.buildClassifier(loadTrainData());
        if (build) {
            classify(instances);
        } else {
            System.err.println("Unable to build classifier");
        }
    }

    /**
     * Set the RealTimeClassifier base classifier to use.
     *
     * @param classifier Classifier to classify instances.
     */
    public void setClassifier(Classifier classifier) {
        m_RealTimeClassifier.setClassifier(classifier);
    }

    /**
     * Set the RealTimeClassifier writer. This defines where the output for the real
     * time classifier goes.
     *
     * @param writer PrintWriter to write to.
     */
    public void setWriter(PrintWriter writer) {
        m_RealTimeClassifier.setWriter(writer);
    }

    /**
     * Simple method of illustrating classification. Should be remove and replaced at some point/
     */
    // ToDo: Rewrite (encapsulate)
    private void classify() {
        // THIS METHOD IS BROKEN
        if (true) return;
        FileEventInstances trainData = null;
        HashMap<File, FileEventInstances> testData = new HashMap<>();

        m_ConsolePane.log("Evaluation", "---------------------");
        m_ConsolePane.log("Evaluation", "Evaluating Model");
        m_ConsolePane.log("Evaluation", "---------------------");

        for (FileObject object : getAllFileObjects(m_TreeView.getRoot())) {
            if (!object.isFile() || object.isIgnored()) continue;

            if (!object.getLabel().equals("unknown")) {
                trainData = FileEventInstances.append(trainData, new FileEventInstances("data", object.getFile(), object.getLabel()));
            } else {
                testData.put(object.getFile(), new FileEventInstances("data", object.getFile(), object.getLabel()));
            }
        }

        if (trainData == null) {
            m_ConsolePane.log("Evaluation", "Insufficient training data");
        } else if (testData.size() == 0) {
            m_ConsolePane.log("Evaluation","No testing data provided: Attempting cross-validation");

            try {
                Evaluation eval = new Evaluation(trainData);
                eval.crossValidateModel(new FileEventOneClassIBK(), trainData, 10, new Random(10));
                m_ConsolePane.log("Evaluation", eval.toSummaryString("\nResults\n======\n", false));
                m_ConsolePane.log("Evaluation", eval.toMatrixString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                Classifier model = new FileEventOneClassIBK();
                model.buildClassifier(trainData);

                for (Map.Entry<File, FileEventInstances> entry : testData.entrySet()) {
                    Evaluation evaluation = new Evaluation(entry.getValue());
                    evaluation.evaluateModel(model, entry.getValue());
                    ArrayList<Prediction> predictions = evaluation.predictions();
                    double positive = predictions.stream().mapToDouble(f -> f.predicted()).sum();
                    double total = predictions.size();
                    m_ConsolePane.log("Evaluation", entry.getKey().getName() + " : " + positive + " / " + total);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        m_ConsolePane.log("Evaluation", "Evaluation Complete");
    }
}
