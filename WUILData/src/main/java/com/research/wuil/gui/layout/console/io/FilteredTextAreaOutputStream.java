package com.research.wuil.gui.layout.console.io;

import com.research.wuil.gui.layout.console.FilteredLog;
import javafx.application.Platform;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextArea;
import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * Filtered text output stream used in tandem with a JavaFX text area.
 *
 * Inspired by post: https://stackoverflow.com/questions/342990/create-java-console-inside-a-gui-panel
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FilteredTextAreaOutputStream extends OutputStream {
    /**
     * Line Appender used to append lines to a Text Area.
     */
    private FilteredLineAppender m_Appender;

    /**
     * Text Area to append lines to.
     */
    private TextArea m_TextArea;

    /**
     * Constructor.
     *
     * @param textArea TextArea to append Strings from the OutputStream to.
     * @param maxLines The maximum number of lines to be used.
     * @param filters Any pre-existing m_Filters.
     */
    public FilteredTextAreaOutputStream(TextArea textArea, int maxLines, String[] filters) {
        m_Appender = new FilteredLineAppender(textArea, maxLines);
        this.m_TextArea = textArea;
        ContextMenu menu = createMenu(filters);
        textArea.setContextMenu(menu);
    }

    /**
     * Constructor. Defaults maximum number of lines as 1000 and no m_Filters.
     *
     * @param textArea TextArea to append Strings from the OutputStream to.
     */
    public FilteredTextAreaOutputStream(TextArea textArea) {
        this(textArea, 1000);
    }

    /**
     * Constructor. Defaults to no m_Filters.
     *
     * @param txtArea The TextArea to append Strings from the OutputStream to.
     * @param maxLines The maximum number of lines to be used.
     */
    public FilteredTextAreaOutputStream(TextArea txtArea, int maxLines) {
        this(txtArea, maxLines, new String[0]);
    }

    /**
     * Append integer to TextArea as a byte.
     *
     * @param b Value to be written.
     */
    @Override
    public synchronized void write(int b) {
        if (m_Appender != null) write(new byte[]{(byte)b}, 0, 1);
    }

    /**
     * Append byte array to TextArea as String.
     *
     * @param b Byte array to be used to construct the String.
     */
    @Override
    public synchronized void write(@NotNull byte[] b) {
        if (m_Appender != null) write(b, 0, b.length);
    }

    /**
     * Write byte array to TextArea as String using offset and length to construct
     * the String.
     *
     * @param b Byte array to be used to construct the String.
     * @param off Offset to begin in the byte array.
     * @param len Length of the String.
     */
    @Override
    public synchronized void write(@NotNull byte[] b, int off, int len) {
        if (m_Appender != null) m_Appender.append(bytesToString(b, off, len));
    }

    /**
     * Append String to TextArea with no filter.
     *
     * @param string String to be appended.
     */
    public synchronized void write(String string) {
        if (m_Appender != null) m_Appender.append(string);
    }

    /**
     * Append FilteredLog to TextArea.
     *
     * @param log Log to be appended.
     */
    public synchronized void log(FilteredLog log) {
        if (m_Appender != null) m_Appender.appendLog(log);
    }

    /**
     * Construct log from a filter and value and append to TextArea.
     *
     * @param filter Filter to be allocated to the FilteredLog.
     * @param value Value to be allocated to the FilteredLog.
     */
    public synchronized void log(String filter, String value) {
        String lines[] = value.split("\\r?\\n");
        for (String line : lines) {
            log (new FilteredLog(filter, line + System.lineSeparator()));
        }
    }

    /**
     * Add Filter to filter logs from the Output stream.
     *
     * @param filter Filter to be added.
     */
    public synchronized void addFilter(String filter) {
        if (m_Appender != null) m_Appender.addFilter(filter);
    }

    /**
     * Remove Filter to stop filtering logs from the Ouptut stream.
     *
     * @param filter Filter to be removed.
     */
    public synchronized void removeFilter(String filter) {
        if (m_Appender != null) m_Appender.removeFilter(filter);
    }

    /**
     * Toggle a given filter from being used. Used to simplify code.
     *
     * @param value Whether the filter should be added or removed.
     * @param filter Filter to be added or removed.
     */
    public synchronized void toggleFilter(boolean value, String filter) {
        if (value) {
            addFilter(filter);
        } else {
            removeFilter(filter);
        }
    }

    /**
     * Create menu to filter to allow certain m_Filters be toggled using a right click.
     *
     * @param filters Filters to be added to the right click menu.
     * @return The context menu to be added to the TextArea right click.
     */
    private ContextMenu createMenu(String ... filters) {
        ContextMenu menu = new ContextMenu();

        for (String filter : filters) {
            CheckMenuItem menuItem = new CheckMenuItem(filter);
            menuItem.setOnAction(event -> {
                this.toggleFilter(menuItem.isSelected(), menuItem.getText());
            });
            menuItem.setSelected(true);
            this.addFilter(menuItem.getText());
            menu.getItems().add(menuItem);
        }
        return menu;
    }

    /**
     * Flush content override. The object auto flushes always, hence this does nothing.
     */
    @Override
    public synchronized void flush() {}

    /**
     * Closes the output stream.
     */
    @Override
    public synchronized void close() {
        m_Appender = null;
    }

    /**
     * Clears all data from the TextArea.
     */
    public synchronized void clear() {
        if (m_Appender != null) m_Appender.clear();
    }

    /**
     * Converts a byte array to a String.
     *
     * @param b Byte array to be used.
     * @param off Offset to start the String from.
     * @param len Length of the String.
     * @return The String created.
     */
    private static String bytesToString(byte[] b, int off, int len) {
        try { return new String(b,off,len,"UTF-8"); } catch(UnsupportedEncodingException thr) { return new String(b,off,len); }
    }

    /**
     * Filtered Line Appender that handles the actual displaying of data to the Text Area.
     */
    private static class FilteredLineAppender implements Runnable {
        /**
         * Text Area to append data to.
         */
        private final TextArea m_TextArea;

        /**
         * Maximum number of lines.
         */
        private final int m_MaxLines;

        /**
         * New logs to be added once the GUI thread has the opportunity.
         */
        private final ArrayDeque<FilteredLog> m_ValuesNew;

        /**
         * Sequential history of logs added.
         */
        private final ArrayDeque<FilteredLog> m_ValuesHistory;

        /**
         * Filters current specified for this object.
         */
        private final Set<String> m_Filters;

        /**
         * Whether a clear request has been requested.
         */
        private boolean m_Clear = false;

        /**
         * Whether a queue request has been requested.
         */
        private boolean m_Queue = true;

        /**
         * Constructor.
         *
         * @param textArea TextArea to append lines to.
         * @param maxLines Maximum number of lines.
         */
        FilteredLineAppender(TextArea textArea, int maxLines) {
            this.m_TextArea = textArea;
            this.m_MaxLines = maxLines;
            this.m_ValuesNew = new ArrayDeque<>();
            this.m_ValuesHistory = new ArrayDeque<>();
            this.m_Filters = new HashSet<>();
            this.m_Filters.add("Default");
        }

        /**
         * Adds a filter to the set of filters for which logs are permitted.
         *
         * @param filter Filter to be added
         */
        synchronized void addFilter(String filter) {
            m_Filters.add(filter);
            rewrite();
        }

        /**
         * Removes a filter from the set of filters for which logs are permitted.
         *
         * @param filter Filter to be removed.
         */
        synchronized void removeFilter(String filter) {
            m_Filters.remove(filter);
            rewrite();
        }

        /**
         * Append line to TextArea, with a filter value set of "Default".
         *
         * @param value String to be appended.
         */
        synchronized void append(String value) {
            appendLog(new FilteredLog("Default", value));
        }

        /**
         * Append log to TextArea.
         *
         * @param log Log to be appended.
         */
        synchronized void appendLog(FilteredLog log) {
            m_ValuesNew.add(log);
            queue();
        }

        /**
         * Issue a clear request.
         */
        synchronized void clear() {
            m_Clear = true;
            m_ValuesNew.clear();
            m_ValuesHistory.clear();
            queue();
        }

        /**
         * Issue a rewrite request which involves clearing displayed String and refiltering the data.
         */
        synchronized void rewrite() {
            m_Clear = true;
            m_ValuesHistory.addAll(m_ValuesNew);
            m_ValuesNew.clear();
            m_ValuesNew.addAll(m_ValuesHistory);
            m_ValuesHistory.clear();
            queue();
        }

        /**
         * Issue a queue request.
         */
        synchronized void queue() {
            if (m_Queue) {
                m_Queue = false;
                Platform.runLater(this);
            }
        }

        /**
         * Writing method which is to be executed on the UI thread. Used to append or rewrite data
         * to the displayed TextArea.
         */
        @Override
        public synchronized void run() {
            if (m_Clear) m_TextArea.setText("");

            while (m_ValuesNew.size() > 0) {
                FilteredLog log = m_ValuesNew.pop();
                m_ValuesHistory.add(log);
                if (m_Filters.contains(log.getFilter())) {
                    m_TextArea.appendText(log.getLine());
                }
                if (m_ValuesHistory.size() > m_MaxLines) {
                    FilteredLog deleted = m_ValuesHistory.pop();
                    if (m_Filters.contains(deleted.getFilter())) {
                        m_TextArea.replaceText(0, deleted.getLine().length()-1, "");
                    }
                }
            }
            m_TextArea.setScrollTop(m_TextArea.getHeight());
            m_Clear = false;
            m_Queue = true;
        }
    }
}
