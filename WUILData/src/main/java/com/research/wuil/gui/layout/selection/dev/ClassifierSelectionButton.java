package com.research.wuil.gui.layout.selection.dev;

import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import weka.classifiers.AbstractClassifier;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

public class ClassifierSelectionButton extends HBox {
    public ClassifierSelectionButton(String name) {
        TreeItem<ClassifierItem> rootItem = new TreeItem<>(new ClassifierItem("classifiers"));
        rootItem.setExpanded(true);

        final TreeView<ClassifierItem> tree = new TreeView<>(rootItem);
        tree.setEditable(true);

        String rootPackage = "weka.classifiers";
        Reflections reflections = new Reflections(rootPackage, new SubTypesScanner(false));

        Set<Class<? extends AbstractClassifier>> allClasses = reflections.getSubTypesOf(AbstractClassifier.class);
        ArrayList<Class<? extends AbstractClassifier>> classifiers = new ArrayList<>(allClasses);
        classifiers.sort(Comparator.comparing((a) -> a.toString().toLowerCase()));

        for (Class classifier : classifiers) {
            String classifierName = classifier.getName().replace(rootPackage + ".", "");
            String[] nodes = classifierName.split("\\.");
            TreeItem<ClassifierItem> pointer = rootItem;
            for (String node : nodes) {
                ClassifierItem newItem = new ClassifierItem(node);
                int index = 0;
                for (TreeItem<ClassifierItem> item : pointer.getChildren()) {
                    if (item.getValue().equals(newItem)) break;
                    index++;
                }
                if (index < pointer.getChildren().size()) {
                    pointer = pointer.getChildren().get(index);
                } else {
                    TreeItem<ClassifierItem> newNode = new TreeItem<>(newItem);
                    pointer.getChildren().add(newNode);
                    pointer = newNode;
                }
            }
        }

        sort(rootItem);

        tree.setRoot(rootItem);
        tree.setShowRoot(true);

        CustomMenuItem menuItem = new CustomMenuItem(tree);
        menuItem.setHideOnClick(false);

        TextField textField = new TextField();
        textField.setEditable(false);
        textField.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2) {
                //ToDo System.out.println("EDIT");
            }
        });

        tree.setOnMouseClicked((event) -> {
            if (event.getClickCount() == 2 && tree.getSelectionModel().getSelectedItem().isLeaf()) {
                textField.setText(tree.getSelectionModel().getSelectedItem().getValue().toString());
            }
        });


        MenuButton button = new MenuButton("Choose");
        button.getItems().add(menuItem);

        this.getChildren().addAll(textField, button);
    }

    public static <T> void sort(TreeItem<T> item) {
        for (TreeItem<T> i : item.getChildren()) {
            sort(i);
        }
        item.getChildren().sort(Comparator.comparing(TreeItem::isLeaf));
    }


}
