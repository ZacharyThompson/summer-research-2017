package com.research.wuil.gui.layout.selection;


import com.google.common.reflect.TypeToken;
import com.research.wuil.gui.layout.selection.input.GenericInput;
import com.research.wuil.utils.ObjectCloner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ListPane<T extends GenericInput> extends VBox {
    private Class m_BaseObject;
    private ObservableList<T> list;

    public ListPane(@NotNull Class base) {
        m_BaseObject = base;

        list = FXCollections.observableArrayList();

        ListView<T> listView = new ListView<>();
        listView.setItems(list);
        listView.setCellFactory((a) -> new GenericListCell<>());
        getChildren().add(listView);

        CellInput<T> grid = new CellInput<>((T) GenericInput.getGenericInput(base), "Add");
        grid.setOnAction(e -> {
            if (grid.getItem().getValue() == null) return;
            list.add((T) grid.getItem().copy());
            grid.reset(m_BaseObject);
        });
        grid.prefWidthProperty().bind(this.widthProperty().subtract(40));
        getChildren().add(grid);

    }

    public final TypeToken<T> type = new TypeToken<T>(getClass()) {};

    public static class GenericListCell<T extends GenericInput> extends ListCell<T> {
        CellInput<T> input;
        Button delete;

        public GenericListCell() {
            super();
            delete = new Button("Delete");
            delete.setOnAction(e -> {
                getListView().getItems().remove(this.getItem());
            });
        }

        @Override
        protected void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                input = new CellInput<>(item, delete);
                input.prefWidthProperty().bind(getListView().widthProperty().subtract(40));
                setGraphic(input);
            }
        }
    }

    private static class CellInput<T extends GenericInput> extends GridPane {
        public T item;
        private Button m_Button;

        public CellInput(@NotNull T input, @NotNull String label) {
           this (input, new Button(label));
        }

        public CellInput(@NotNull T input, @NotNull Button button) {
            setPadding(new Insets(2));
            setHgap(2);
            setVgap(2);

            item = input;
            m_Button = button;
            add(input.getInput(), 0, 0);
            add(m_Button, 1, 0);

            m_Button.setPrefWidth(60);
            input.getInput().prefWidthProperty().bind(widthProperty().subtract(60));

            ColumnConstraints leftCol = new ColumnConstraints();
            ColumnConstraints rightCol = new ColumnConstraints();

            rightCol.setHalignment(HPos.RIGHT);
            rightCol.setHgrow(Priority.ALWAYS);

            getColumnConstraints().addAll(leftCol, rightCol);
        }

        public void setOnAction(EventHandler<ActionEvent> event) {
            m_Button.setOnAction(event);
        }

        public T getItem() {
            return item;
        }

        public void reset(Class base) {
            getChildren().remove(item.getInput());
            item = (T) GenericInput.getGenericInput(base);
            item.getInput().prefWidthProperty().bind(widthProperty().subtract(60));
            add(item.getInput(), 0, 0);
        }
    }

    public List<Object> getItems() {
        List<Object> list = new ArrayList<>();
        for (GenericInput input : this.list) {
            list.add(input.getValue());
        }
        return list;
    }

    public void addItems(List<Object> list) {
        for (Object o : list) {
            GenericInput input = GenericInput.getGenericInput(m_BaseObject);
            if (input != null) {
                input.setValue(ObjectCloner.deepCopy(o));
                this.list.add((T) input);
            }
        }
    }

    public void clear() {
        list.clear();
    }
}
