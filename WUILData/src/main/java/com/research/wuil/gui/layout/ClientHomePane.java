package com.research.wuil.gui.layout;

import com.research.wuil.gui.Client;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import javafx.application.Platform;
import javafx.scene.control.SplitPane;
import org.apache.commons.lang.StringUtils;
import org.apache.http.conn.util.InetAddressUtils;
import weka.classifiers.Classifier;

import java.io.File;

/**
 *
 * The home pane for the Client. This is a split pane that consists of two individual panes at a time.
 * On the left is a console pane that displays relevant information for the Client while on the right
 * is either a settings pane or a client session pane.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ClientHomePane extends SplitPane {
    /**
     * The Console Pane for which relevant information to the Client is displayed.
     */
    private FilteredConsolePane m_ConsolePane = new FilteredConsolePane("Default", "Server", "Osquery");

    /**
     * The Settings Pane that allows the user to configure the connection settings, the path to
     * monitor and the classifier to use.
     */
    private ClientSettingsPane m_SettingsPane = new ClientSettingsPane((event) -> connect());

    /**
     * The Client session for when a connection is established with the back end server.
     */
    private Client m_Client;

    /**
     * Constructor.
     */
    public ClientHomePane() {
        this.getItems().addAll(m_ConsolePane, m_SettingsPane);
        setDividerPosition(0, 0.75);
    }

    /**
     * Define the input events for the console pane
     *
     * @param consolePane Console pane to add input events to
     */
    private void setInputEvents(FilteredConsolePane consolePane) {
        consolePane.addInputHandler((input, level) -> {
            if (input.startsWith("connect ")) {
                // Add User Input control
            }
        });
    }

    /**
     * Attempts to connect to a back end server using the settings provided. Before attempting a connection,
     * the information is verified to ensure that it is valid.
     */
    private void connect() {
        Object classifier = m_SettingsPane.getParameter("Classifier");
        String ip = m_SettingsPane.getParameter("IP Address").toString();
        String port = m_SettingsPane.getParameter("Port").toString();
        String interval = m_SettingsPane.getParameter("OSQuery Interval").toString();
        File file = new File(m_SettingsPane.getParameter("Target Environment").toString());
        if (!InetAddressUtils.isIPv4Address(ip)) {
            m_ConsolePane.log("Server", "Invalid arguments: Address not valid");
        } else if (!StringUtils.isNumeric(port)) {
            m_ConsolePane.log("Server", "Invalid arguments: Port not valid");
        } else if (!file.exists()) {
            m_ConsolePane.log("Server", "Invalid arguments: Target Environment does not exist");
        } else if (!StringUtils.isNumeric(interval)) {
            m_ConsolePane.log("Server", "Invalid arguments: Interval no valid");
        } else if (!(classifier instanceof Classifier)) {
            m_ConsolePane.log("Server", "Invalid arguments: Classifier invalid");
        } else {
            int p = Integer.parseInt(port);
            int i = Integer.parseInt(interval);
            m_Client = new Client(m_ConsolePane, ip, p, file, (Classifier) classifier, i);
            this.getItems().remove(m_SettingsPane);
            this.getItems().add(m_Client);
            m_Client.runningProperty().addListener((e) -> {
                if (!m_Client.isRunning()) showSettings();
            });
            m_Client.setPrefWidth(this.getPrefWidth());
        }
    }

    /**
     * Used to show the settings. This is used when the Client has stopped running so that a new connection
     * can be established.
     */
    private void showSettings() {
        Platform.runLater(() -> {
            this.getItems().remove(m_Client);
            this.getItems().add(m_SettingsPane);
        });
    }
}
