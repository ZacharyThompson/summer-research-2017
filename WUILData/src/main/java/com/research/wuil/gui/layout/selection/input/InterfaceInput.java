package com.research.wuil.gui.layout.selection.input;

import com.research.wuil.gui.layout.selection.GenericSelectionButton;
import javafx.scene.layout.Region;

/**
 *
 * Object that creates a Selection input for Interface types.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class InterfaceInput extends GenericInput {
    /**
     * The Input Field.
     */
    private GenericSelectionButton m_Input;

    /**
     * The root class
     */
    private Class<?> m_Root;

    /**
     * Constructor.
     *
     * @param root The root item to be used to construct the GenericSelectionButton.
     */
    public InterfaceInput(Class<?> root) {
        m_Root = root;
        m_Input = new GenericSelectionButton(root);
    }

    /**
     * Get the value from the GenericSelectionButton.
     *
     * @return The selected item.
     */
    @Override
    public Object getValue() {
        return m_Input.getSelection();
    }

    /**
     * Set the value in the GenericSelectionButton.
     *
     * @param o Value to set.
     */
    @Override
    public void setValue(Object o) {
        m_Input.setSelection(o);
    }

    /**
     * Get the JavaFX input, a GenericSelectionButton, which is used to collect input from the user.
     *
     * @return GenericSelectionButton input.
     */
    @Override
    public Region getInput() {
        return m_Input;
    }

    /**
     * Create a copy of this Object.
     *
     * @return A copy of this Object
     */
    @Override
    public InterfaceInput copy() {
        InterfaceInput input = new InterfaceInput(m_Root);
        input.setValue(this.getValue());
        return input;
    }
}
