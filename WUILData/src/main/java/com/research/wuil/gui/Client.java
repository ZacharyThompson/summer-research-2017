package com.research.wuil.gui;

import com.research.wuil.gui.layout.PredictionPane;
import com.research.wuil.gui.layout.console.FilteredConsolePane;
import com.research.wuil.utils.OptionSerializer;
import com.research.wuil.utils.Osquery;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.lang.StringUtils;
import org.apache.http.conn.util.InetAddressUtils;
import weka.classifiers.Classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * Client runnable Class used to handle the delegation of basic tasks for
 * each client.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class Client extends AnchorPane implements Runnable {
    /**
     * The maximum permitted connection attempts before inferring a connection could not be established.
     * Although TCP can infer this from a single attempt, this allows a server to be started upon notifying
     * the Client that no server is launched (ie. Hybrid application).
     */
    private static final int MAX_ATTEMPTS = 5;

    /**
     * Whether the Thread is running. Used to stop the Thread if a user wants a safe stop.
     */
    private SimpleBooleanProperty m_Running = new SimpleBooleanProperty(false);

    /**
     * The ConsolePane for the client. This should be used to display relevant output.
     */
    private FilteredConsolePane m_ConsolePane;

    /**
     * The address to connect to.
     */
    private String m_Address;

    /**
     * The port to connect to.
     */
    private int m_Port;

    /**
     * The clients socket that should be bound outwards to the server.
     */
    private Socket m_Socket;

    /**
     * A print writer for writing to the socket output stream.
     */
    private PrintWriter m_Out;

    /**
     * A buffered reader for reading from the socket input stream.
     */
    private BufferedReader m_In;

    /**
     * Thread that will run this runnable class.
     */
    private Thread m_Thread;

    /**
     * The osquery instance that will begin monitoring file accesses.
     */
    private Osquery m_Osquery;

    /**
     * The selected classifier.
     */
    private Classifier m_Classifier;

    /**
     * Prediction Pane
     */
    private PredictionPane m_PredictionPane;

    /**
     * Constructor.
     *
     * @param consolePane The clients console pane.
     * @param input The connection input string collected from the console.
     * @param monitor The root file to monitor.
     */
    public Client(FilteredConsolePane consolePane, String input, File monitor) {
        this.m_ConsolePane = consolePane;
        input = input.replace("connect ", "");
        String components[] = input.split(" |:");
        if (components.length != 2) {
            consolePane.log("Server", "Invalid arguments: Expected 'connection [m_Address]:[m_Port]'");
        } else if (!InetAddressUtils.isIPv4Address(components[0])) {
            consolePane.log("Server","Invalid arguments: Address not valid");
        } else if (!StringUtils.isNumeric(components[1])) {
            consolePane.log("Server","Invalid arguments: Port not valid");
        } else {
            m_Address = components[0];
            m_Port = Integer.valueOf(components[1]);
        }
        m_Osquery = new Osquery(this, monitor, 30);
        start();
    }

    /**
     * Constructor.
     *
     * @param consolePane The clients console pane.
     * @param address The address to attempt to establish a connection to.
     * @param port The port to attempt to establish a connection to.
     * @param monitor The root file to monitor.
     */
    public Client(FilteredConsolePane consolePane, String address, int port, File monitor) {
        this.m_ConsolePane = consolePane;
        this.m_Address = address;
        this.m_Port = port;
        m_Osquery = new Osquery( this, monitor, 30);
        start();
    }

    /**
     * Constructor.
     *
     * @param consolePane The clients console pane.
     * @param address The address to attempt to establish a connection to.
     * @param port The port to attempt to establish a connection to.
     * @param monitor The root file to monitor.
     * @param classifier The classifier to send to the Server.
     */
    public Client(FilteredConsolePane consolePane, String address, int port, File monitor, Classifier classifier, int interval) {
        this.m_ConsolePane = consolePane;
        this.m_Address = address;
        this.m_Port = port;
        this.m_Classifier = classifier;
        m_Osquery = new Osquery(this, monitor, interval);
        start();
    }

    /**
     * Used to start a new Thread that will execute this runnable class.
     */
    private void start() {
        if (!isRunning()) {
            m_Running.set(true);
            m_Thread = new Thread(this);
            m_Thread.start();
        }
    }

    /**
     * Used to safely stop the current running thread.
     */
    public void stop() {
        m_Running.set(false);
    }

    /**
     * Checks whether the Client thread is running.
     *
     * @return Whether the client is running.
     */
    public boolean isRunning() { return m_Running.get(); }

    /**
     * Attempts a connection to the server identified by the given address:port.
     *
     * @param address The address of the server.
     * @param port The port the server is listening on.
     * @return Whether a connection was established.
     */
    private boolean connect(String address, int port) {
        m_ConsolePane.log("Server", String.format("Attempting connection of %s:%d ...", address, port));
        try {
            this.m_Socket = new Socket(address, port);
            this.m_In = new BufferedReader(new InputStreamReader(m_Socket.getInputStream()));
            this.m_Out = new PrintWriter(m_Socket.getOutputStream(), true);

            if (m_Classifier != null) {
                m_Out.println("Classifier:" + OptionSerializer.serialize(m_Classifier));
            }

            m_ConsolePane.addInputHandler((line, level) -> {
                if (level >= 3) {
                    String[] data = line.split(" ", 2);
                    switch (data[0]) {
                        case "exit":
                            stop();
                        case "clear":
                            m_ConsolePane.clear();
                        case "transmit":
                            if (data.length == 2 && m_Out != null) m_Out.println(data[1]);
                    }
                }
            });
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Method that will be run on a separate thread to avoid blocking the JavaFX UI thread.
     */
    @Override
    public void run() {
        try {
            for (int attempt = 0; !connect(m_Address, m_Port); attempt++) {
                if (attempt == MAX_ATTEMPTS) {
                    m_Running.set(false);
                    m_ConsolePane.log("Server", "Connection could not be established");
                    return;
                }
            }
            m_Osquery.start();

            Platform.runLater(() -> {
                m_PredictionPane = new PredictionPane();
                this.getChildren().add(m_PredictionPane);
                AnchorPane.setLeftAnchor(m_PredictionPane, 0.0);
                AnchorPane.setRightAnchor(m_PredictionPane, 0.0);
                AnchorPane.setBottomAnchor(m_PredictionPane, 0.0);
                AnchorPane.setTopAnchor(m_PredictionPane, 0.0);
            });

            for (String line; (line = m_In.readLine()) != null && isRunning();) {
                if (line.startsWith("prediction:")) {
                    line = line.replace("prediction:", "");
                    m_PredictionPane.update(new Double(line));
                }
                m_ConsolePane.log("Server", line);
            }
        } catch (Exception e) {
            m_Out.close();
        }
        m_ConsolePane.log("Server", "Connection Lost");
        m_Osquery.stop();
        m_Running.set(false);
    }

    /**
     * Print a line to the Server.
     *
     * @param line Line to print.
     */
    public synchronized void println(String line) {
        m_Out.println(line);
    }

    /**
     * Log a line to the ConsolePane.
     *
     * @param filter Filter name.
     * @param line Line to print.
     */
    public void log (String filter, String line) {
        m_ConsolePane.log(filter, line);
    }

    /**
     * Get the output stream to the server.
     *
     * @return Output stream as print writer.
     */
    public PrintWriter getOutputStream() {
        return m_Out;
    }

    /**
     * Get the running property.
     *
     * @return Running property.
     */
    public SimpleBooleanProperty runningProperty() {
        return m_Running;
    }
}
