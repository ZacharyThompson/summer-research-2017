package com.research.wuil.gui.layout.selection.input;

import javafx.scene.control.TextField;
import javafx.scene.layout.Region;

/**
 *
 * Object that creates an TextField input for String typed data.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class StringInput extends GenericInput<String> {
    /**
     * The Input Field
     */
    private TextField m_Input;

    /**
     * Constructor.
     */
    public StringInput() {
        m_Input = new TextField();
    }

    /**
     * Get the value from within the input.
     *
     * @return The text from the TextField.
     */
    @Override
    public String getValue() {
        return m_Input.getText();
    }

    /**
     * Set the value of the input.
     *
     * @param s Value to set.
     */
    @Override
    public void setValue(String s) {
        m_Input.setText(s);
    }

    /**
     * Get the JavaFX Input, a TextField, which is used to collect input from the user.
     *
     * @return TextField input.
     */
    @Override
    public Region getInput() {
        return m_Input;
    }

    /**
     * Create a copy of this Object.
     *
     * @return A copy of this Object
     */
    @Override
    public StringInput copy() {
        StringInput input = new StringInput();
        input.setValue(this.getValue());
        return input;
    }
}
