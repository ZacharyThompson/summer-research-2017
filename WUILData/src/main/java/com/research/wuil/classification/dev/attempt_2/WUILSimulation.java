package com.research.wuil.classification.dev.attempt_2;

import weka.classifiers.Evaluation;
import weka.classifiers.meta.OneClassClassifier;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WUILSimulation {
    private static final File path = new File("E:/Research/WUIL Logs/data/");
    private static final int segmentSize = 100;
    private static final String[] types = {"User Log", "Attack 1 Log", "Attack 2 Log", "Attack 3 Log"};
    private String user;
    private List<WUILDatum> data = new ArrayList();
    private List<WUILSession> sessions = new ArrayList<>();

    public WUILSimulation (String user) {
        this.user = user;

        for (String type : types) {
            String label = type.equals("User Log") ? "target" : "outlier";
            File file = new File(path, String.join("/", user, type, "log.txt"));
            if (!file.exists()) {
                continue;
            }

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;
                while((line = bufferedReader.readLine()) != null) {
                    data.add(new WUILDatum(line, label));
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

        data.sort(Comparator.comparing(WUILDatum::getLabel));
        data.sort(Comparator.comparing(WUILDatum::getuId));
        data.sort(Comparator.comparing(WUILDatum::getDate));

        WUILSession current = new WUILSession();
        sessions.add(current);
        for (WUILDatum datum : data) {
            if (!current.addData(datum)) {
                sessions.add(current = new WUILSession());
                current.addData(datum);
            }
        }

        List<WUILActivity> train = new ArrayList<>();
        List<WUILActivity> test = new ArrayList<>();
        int minTrainSize = 500;
        int maxTrainSize = 2000;

        for (WUILSession session : sessions) {
            File trainFile = new File("E:/Research/WUIL Logs/temp/train.arff");
            File testFile = new File("E:/Research/WUIL Logs/temp/test.arff");

            if (train.size() < minTrainSize) {
                while (session.hasNext()) {
                    WUILActivity activity = session.next();
                    train.add(activity);
                }
                continue;
            } else {
                test = new ArrayList<>();
                while (session.hasNext()) {
                    WUILActivity activity = session.next();
                    test.add(activity);
                }
            }

            Collections.shuffle(train);
            train = train.subList(0, Math.min(maxTrainSize, train.size()));

            try {
                PrintWriter trainWriter = new PrintWriter(trainFile);
                PrintWriter testWriter = new PrintWriter(testFile);

                trainWriter.println(WUILActivity.getHeader());
                trainWriter.flush();
                testWriter.println(WUILActivity.getHeader());
                testWriter.flush();

                for (WUILActivity activity : train) {
                    trainWriter.println(String.format("%s,%s", activity.toInstance(), "target"));
                    trainWriter.flush();
                }
                trainWriter.close();

                for (WUILActivity activity : test) {
                    testWriter.println(String.format("%s,%s", activity.toInstance(), session.label));
                    testWriter.flush();
                }
                testWriter.close();

                DataSource trainSource = new DataSource(trainFile.getPath());
                DataSource testSource = new DataSource(testFile.getPath());

                Instances trainData = trainSource.getDataSet();
                Instances testData = testSource.getDataSet();

                if (trainData.classIndex() == -1) {
                    trainData.setClassIndex(trainData.numAttributes()-1);
                }
                if (testData.classIndex() == -1) {
                    testData.setClassIndex(testData.numAttributes()-1);
                }

                OneClassClassifier occ = new OneClassClassifier();
                occ.setOptions(Utils.splitOptions("-num \"weka.classifiers.meta.generators.UniformDataGenerator -S 1 -L 0.0 -U 1.0\" -nom \"weka.classifiers.meta.generators.NominalGenerator -S 1\" -trr 0.1 -tcl target -cvr 10 -cvf 10.0 -P 0.5 -S 1 -W weka.classifiers.lazy.IBk -num-decimal-places 3 -- -K 1 -W 0 -I -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\""));
                occ.buildClassifier(trainData);

                System.out.println("-----" + session.label + "------");
                Evaluation eval = new Evaluation(testData);
                eval.evaluateModel(occ, testData);
                System.out.println(eval.toSummaryString("Results\n", false));

                for (int i = 0; i < testData.numInstances(); i++) {
                    double clsLabel = occ.classifyInstance(testData.instance(i));
                    System.out.print(clsLabel + " ");
                }
                System.out.println("");

                if (session.label.equals("target")) {
                        train.addAll(test);
                }

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    public static class WUILSession implements Iterator<WUILActivity> {
        public String label;
        public List<WUILDatum> data = new ArrayList<>();
        public static final int maxTimeDiff = 60;
        private int cursor = 0;

        public boolean addData (WUILDatum datum) {
            if (data.size() == 0) {
                data.add(datum);
                label = datum.getLabel();
                return true;
            } else {
                long diffInMinutes = ChronoUnit.MINUTES.between(data.get(data.size()-1).getDate(), datum.getDate());
                if (datum.getLabel().equals(label) && diffInMinutes <= maxTimeDiff) {
                    data.add(datum);
                    return true;
                } else {
                    return false;
                }
            }
        }

        @Override
        public String toString() {
            return label + " " + data.size() + " " + data.get(0).getDate() + " - " + data.get(data.size()-1).getDate();
        }

        @Override
        public boolean hasNext() {
            return cursor + segmentSize < data.size();
        }

        @Override
        public WUILActivity next() {
            WUILActivity activity = new WUILActivity(data.subList(cursor, cursor+segmentSize), label);
            cursor += segmentSize;
            return activity;
        }
    }
}
