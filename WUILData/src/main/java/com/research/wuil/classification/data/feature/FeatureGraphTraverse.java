package com.research.wuil.classification.data.feature;

/**
 *
 * Feature representing the the traversal within a directed graph. That is,
 * it covers the direct transition between two nodes by including a path
 * between those two nodes.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FeatureGraphTraverse extends FeatureGraph {
    /**
     * Constructor.
     */
    public FeatureGraphTraverse() { super("FeatureGraphTraverse"); }

    /**
     * Constructor.
     *
     * @param name Name of the feature.
     */
    public FeatureGraphTraverse(String name) {
        super(name);
        FeatureGraphNode node = new FeatureGraphNode("R");
        this.m_Nodes.put("R", node);
        this.m_Current = node;
    }

    /**
     * Consume the supplied data.
     *
     * @param targetKey The data to be consumed.
     */
    public void consume(String targetKey) {
        targetKey = "R" + "/" + targetKey;

        if (this.m_Current.getKey().equals(targetKey)) {
            this.m_Current.addConnection(this.m_Current);
            return;
        }

        String[] targetDirs = targetKey.split("/");
        String[] currentDirs = this.m_Current.getKey().split("/");

        FeatureGraphNode targetNode = m_Nodes.get(targetKey);
        if (targetNode == null) {
            targetNode = new FeatureGraphNode(targetKey);
            m_Nodes.put(targetKey, targetNode);
        }
        String intKey = "";
        for (int i = 0; i < targetDirs.length && i < currentDirs.length; i++) {
            if (!targetDirs[i].equals(currentDirs[i])) {
                break;
            }
            intKey = String.join("/", intKey, targetDirs[i]);
        }
        intKey = intKey.substring(1, intKey.length());
        FeatureGraphNode intNode = m_Nodes.get(intKey);
        if (intNode == null) {
            intNode = new FeatureGraphNode(intKey);
            m_Nodes.put(intKey, intNode);
        }
        m_Current.addConnection(intNode);
        intNode.addConnection(targetNode);
        this.m_Current = targetNode;
    }
}
