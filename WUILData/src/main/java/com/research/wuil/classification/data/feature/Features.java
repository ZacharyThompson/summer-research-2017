package com.research.wuil.classification.data.feature;

import com.research.wuil.classification.data.FileEventObservation;
import org.jetbrains.annotations.NotNull;
import weka.core.Attribute;

import java.io.Serializable;
import java.util.*;

/**
 *
 * Collection of Features. Used to form instances / data sets to be used for classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class Features implements Iterable<Feature>, Serializable {
    /**
     * Collection of Features
     */
    private List<Feature> m_Features = new ArrayList<>();

    /**
     * Constructor.
     */
    public Features() { }

    /**
     * Constructor.
     *
     * @param features The list of features to be used.
     */
    public Features(List<Feature> features) {
        m_Features = features;
    }

    /**
     * Get all the attributes.
     *
     * @return Collection of attributes.
     */
    public ArrayList<Attribute> getAttributes() {
        ArrayList<Attribute> attributes = new ArrayList<>();
        for (Feature feature : m_Features) {
            for (Map.Entry<String, Double> entry : feature.getValues().entrySet()) {
                attributes.add(new Attribute(feature.getName() + "_" + entry.getKey()));
            }
        }
        attributes.add(new Attribute("class", FileEventObservation.CLASS_VALUES));
        return attributes;
    }

    /**
     * Add feature to collection of features.
     *
     * @param feature Feature to be added
     */
    public void add(Feature feature) {
        m_Features.add(feature);
    }

    /**
     * Iterator of feature collection.
     *
     * @return Collection of features iterator.
     */
    @NotNull
    @Override
    public Iterator<Feature> iterator() {
        return m_Features.iterator();
    }

    /**
     * Creates a new features object appropriate for FileEvent classification.
     *
     * @return New Features Object populated with Features.
     */
    public static Features getDefaultFeatures() {
        Features features = new Features();
        Feature feature;

        feature = new FeatureGraphAccess("file_access");
        features.add(feature);

        feature = new FeatureGraphAccess("ext_access");
        feature.addPreprocess((Processor & Serializable) (k) -> {
            String[] parts = k.split("/");
            return Arrays.asList(parts[parts.length-1]);
        });
        features.add(feature);

        feature = new FeatureGraphAccess("dir_access");
        feature.addPreprocess((Processor & Serializable) (k) -> {
            String[] parts = k.split("/");
            List<String> dirs = new ArrayList<>();
            String dir = "";
            for (int i = 0; i < parts.length; i++) {
                dir = dir + parts[i];
                dirs.add(dir);
            }
            return dirs;
        });
        features.add(feature);

        feature = new FeatureGraphTraverse("dir_traverse");
        //features.add(feature);

        feature = new FeatureAccessStatistics("access_stats");
        features.add(feature);

        feature = new FeaturePathStatistics("path_stats");
        features.add(feature);

        return features;
    }

    /**
     * Get the list of features
     *
     * @return The collection of features.
     */
    public List<Feature> getFeatures() {
        return m_Features;
    }
}
