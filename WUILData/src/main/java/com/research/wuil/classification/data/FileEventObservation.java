package com.research.wuil.classification.data;

import org.jetbrains.annotations.NotNull;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * The FileEventObservation is an individual Observation of a collection of FileEvents
 * as gathered by OSQuery. That is, FileEvents collected from the same OSQuery file event
 * query are treated as a distinct observation for the purposes of classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileEventObservation implements Iterable<FileEvent> {
    /**
     * Collection of Class Labels
     */
    public static List<String> CLASS_VALUES = Arrays.asList("target", "outlier");

    /**
     * Collection of FileEvents that exist within this Observation.
     */
    private ArrayList<FileEvent> m_FileEvents = new ArrayList<>();

    /**
     * The Class Label of this particular Observation.
     */
    private String m_Label;

    /**
     * Constructor.
     * Creates an empty FileEventObservation
     *
     * @param label The class label of this Observation.
     */
    public FileEventObservation(String label) {
        if (CLASS_VALUES.contains(label)) {
            this.m_Label = label;
        } else {
            this.m_Label = null;
        }
    }

    /**
     * Add a FileEvent to this Observation if and only if the FileEvent Observation to be
     * added was obtained from the same OSQuery file event query as the other events present.
     *
     * @param event The FileEvent to be added.
     * @return Whether the FileEvent was added.
     */
    public boolean add(FileEvent event) {
        if (m_FileEvents.size() == 0 || m_FileEvents.get(0).isCompatible(event)) {
            m_FileEvents.add(event);
            return true;
        }
        return false;
    }

    /**
     * Iterator
     *
     * @return FileEvents iterator
     */
    @NotNull
    @Override
    public Iterator<FileEvent> iterator() {
        return m_FileEvents.iterator();
    }

    /**
     * Get Label
     *
     * @return class label.
     */
    public String getLabel() {
        return m_Label;
    }

    /**
     * Create a collection of FileEventObservations from reading a file.
     *
     * @param file The file to read the data from.
     * @param label The class label associated with the file.
     * @return The collection of FileEventObservations generated.
     */
    public static ArrayList<FileEventObservation> ObservationsFromFile(File file, String label) {
        ArrayList<FileEventObservation> observations = new ArrayList<>();
        FileEventObservation current = new FileEventObservation(label);
        observations.add(current);

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                FileEvent event;
                try {
                    event = new FileEvent(line);
                } catch (Exception e) {
                    continue;
                }
                if (!current.add(event)) {
                    current = new FileEventObservation(label);
                    observations.add(current);
                    current.add(event);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return observations;
    }

    /**
     * Create a collection of FileEventObservations from reading a collection of Strings.
     *
     * @param list Collection of OSQuery file event records.
     * @param label The class label associated with the file.
     * @return The collection of FileEventObservations generated.
     */
    public static ArrayList<FileEventObservation> ObservationsFromList(List<String> list, String label) {
        ArrayList<FileEventObservation> observations = new ArrayList<>();
        FileEventObservation current = new FileEventObservation(label);
        observations.add(current);

        for (String line : list) {
            FileEvent event;
            try {
                event = new FileEvent(line);
            } catch (Exception e) {
                continue;
            }
            if (!current.add(event)) {
                current = new FileEventObservation(label);
                observations.add(current);
                current.add(event);
            }
        }
        return observations;
    }

    /**
     * Converts a collection of FileEventObservations to a raw instance set
     *
     * @param observations The collection of FileEventObservations
     * @return The raw instance set
     */
    public static Instances RawInstancesFromCollection(List<FileEventObservation> observations) {
        Instances instances = getInstances(observations.size());
        observations.forEach(o -> instances.add(o.toInstance(instances)));
        return instances;
    }

    /**
     * Creates a raw instance set from a File, reading the file as a collection of FileEventObservations
     *
     * @param file The file to read the FileEventObservations from
     * @param label The class label to be assigned to each instance
     * @return The the raw instance set
     */
    public static Instances RawInstancesFromFile(File file, String label) {
        List<FileEventObservation> observations = ObservationsFromFile(file, label);
        return RawInstancesFromCollection(observations);
    }

    /**
     * Creates a default Weka instance using the information from this class.
     * The instance consists of a class label and a single predictor attribute that
     * combines the immediately relevant information regarding a FileEvent:
     * Type of access and path.
     *
     * @return Instance created.
     */
    public Instance toInstance(Instances instances) {
        Instance inst = new DenseInstance(instances.numAttributes());
        inst.setDataset(instances);
        inst.setValue(0, this.toString());
        if (CLASS_VALUES.contains(this.m_Label)) {
            inst.setValue(1, this.m_Label);
        }
        return inst;
    }

    /**
     * Static builder for new dataset using the FileEvents data. Used if you want to configure the model
     * using filters as opposed to the "hard" approach taken initially.
     *
     * @param capacity The capacity of the dataset
     * @return The dataset
     */
    public static Instances getInstances(int capacity) {
        ArrayList<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("file_events", (ArrayList<String>) null));
        attributes.add(new Attribute("class", CLASS_VALUES));
        Instances dataset= new Instances("data", attributes, capacity);
        dataset.setClassIndex(1);
        return dataset;
    }

    /**
     * String representation of this instance. Used when build to a two-attribute instance.
     * It is represented by a sequential list of file events with only the action type and
     * path used.
     *
     * @return The String representation.
     */
    @Override
    public String toString() {
        return m_FileEvents.stream().map((event) ->
                String.format("[%s:%s]", event.get("action"), event.getPath()))
                .collect(Collectors.joining(""));
    }

    public static void main (String args[]) {
        File file = new File("E:\\Research\\Repository\\resources\\server\\127.0.0.1\\logs\\1515316407124.log");
        List<FileEventObservation> observations = ObservationsFromFile(file, "target");
        Instances instances = getInstances(1);
        instances.add(observations.get(0).toInstance(instances));
    }
}
