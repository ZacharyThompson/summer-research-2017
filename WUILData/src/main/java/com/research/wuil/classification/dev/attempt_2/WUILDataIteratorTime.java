package com.research.wuil.classification.dev.attempt_2;

import weka.core.Attribute;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WUILDataIteratorTime implements Iterator<WUILActivity> {
    private static final File path = new File("E:/Research/WUIL Logs/data/");
    private static long timespan = 15;
    private int cursor = 0;
    private List<WUILActivity> data = new ArrayList<>();
    private Random rand = new Random(100);
    private String user;
    private String[] filenames;
    private HashMap<String, Integer> vocab;


    public WUILDataIteratorTime (String user, String... filenames) {
        this.user = user;
        this.filenames = filenames;

        List<WUILDatum> rawData = new ArrayList<>();
        for (String filename : filenames) {
            String label = filename.equals("User Log") ? "target" : "outlier";

            File file = new File(path, user + "/" + filename + "/log.txt");
            if (!file.exists()) {
                continue;
            }

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    WUILDatum datum = new WUILDatum(line, label);
                    rawData.add(datum);
                }

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

        rawData.sort(Comparator.comparing(WUILDatum::getuId));
        rawData.sort(Comparator.comparing(WUILDatum::getDate));

        WUILActivity activity = null;
        for (WUILDatum datum : rawData) {
            if (activity != null && activity.getLabel().equals(datum.getLabel())) {
                if (activity.startDate() != null) {
                    long delta = ChronoUnit.SECONDS.between(activity.startDate(), datum.getDate());
                    if (delta <= timespan) {
                        activity.addData(datum);
                        continue;
                    }
                } else {
                    activity.addData(datum);
                    continue;
                }
            }
            activity = new WUILActivity(datum.getLabel());
            activity.addData(datum);
            data.add(activity);
        }
    }

    public void append (WUILDataIteratorTime data) {
        while (data.hasNext()) {
            this.data.add(data.next());
        }
    }

    public WUILDataIteratorTime (List<WUILActivity> data) {
        this.data.addAll(data);
    }

    public WUILDataIteratorTime extract(int size) {
        Collections.shuffle(this.data, rand);
        WUILDataIteratorTime extract = new WUILDataIteratorTime(data.subList(0, Math.min(size, data.size())));
        this.data = data.subList(Math.min(size, data.size()), data.size());
        return extract;
    }

    public void trim(int size) {
        Collections.shuffle(this.data, rand);
        this.data = data.subList(0, Math.min(size, data.size()));
    }

    public void clean() {
        for (Iterator<WUILActivity> iter = data.iterator(); iter.hasNext();) {
            if (iter.next().getSize() <= 5) {
                iter.remove();
            }
        }
    }

    public Instances getDataset() {
        ArrayList<Attribute> attributes = WUILActivity.getAttributes();
        Instances instances = new Instances("data", attributes, 1000);

        while (this.hasNext()) {
            instances.add(this.next().getInstance(instances));
        }
        this.reset();
        instances.setClassIndex(instances.numAttributes()-1);
        return instances;
    }

    public HashMap<String, Integer> createVocab() {
        HashMap<String, Integer> vocab = new HashMap<>();

        for (WUILActivity activity : data) {
            for (WUILDatum datum : activity.getData()) {
                int count = vocab.containsKey(datum.getKey()) ? vocab.get(datum.getKey()) : 0;
                vocab.put(datum.getKey(), count+1);
            }
        }

        return vocab;
    }

    public void setVocab (HashMap<String, Integer> vocab) {
        this.vocab = vocab;
        for (WUILActivity activity : data) {
            activity.setVocab(vocab);
        }
    }

    @Override
    public boolean hasNext() {
        return (cursor < data.size());
    }

    @Override
    public WUILActivity next() {
        return data.get(cursor++);
    }

    @Override
    public void remove() {
        data.remove(cursor);
    }

    public void reset() {
        cursor = 0;
    }

    public int size() {
        return data.size();
    }
}
