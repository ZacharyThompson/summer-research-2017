package com.research.wuil.classification.data;

import com.research.wuil.classification.data.feature.Feature;
import com.research.wuil.classification.data.feature.Features;
import weka.core.DenseInstance;
import weka.core.Instances;

/**
 *
 * The File Event data instance which holds the set of m_Features to be
 * used for classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileEventInstance extends DenseInstance {
    /**
     * The FileEventObservation that this Instance is based upon.
     */
    private FileEventObservation m_Observation;

    /**
     * Features to consume the data from the FileEventObservation.
     */
    private Features m_Features;

    /**
     * Constructor
     *
     * @param m_Features The features to consume the data.
     * @param observation The FileEventObservation this Instance is based on.
     */
    public FileEventInstance (Features m_Features, FileEventObservation observation) {
        this (new Instances("data", m_Features.getAttributes(), 1000), m_Features, observation);
    }

    /**
     * Constructor.
     *
     * @param instances The instance to model this instance after.
     * @param m_Features The set of features for this Instance.
     * @param observation The observation this Instance is based upon.
     */
    public FileEventInstance (Instances instances, Features m_Features, FileEventObservation observation) {
        super(instances.numAttributes());
        this.m_Observation = observation;
        this.m_Features = m_Features;
        processData();

        this.setDataset(instances);

        int index = 0;
        for (Feature feature : m_Features) {
            for (Double value : feature.getValues().values()) {
                this.setValue(index++, value);
            }
        }

        if (observation.getLabel() != null) {
            this.setValue(index++, observation.getLabel());
        }
    }

    /**
     * Process the data from the FileEventInstance into each of the Features
     * to produce a feature set to be used with classification.
     */
    private void processData() {
        for (FileEvent event : m_Observation) {
            for (Feature feature : m_Features) {
                feature.consume(event.getPath());
            }
        }
    }
}
