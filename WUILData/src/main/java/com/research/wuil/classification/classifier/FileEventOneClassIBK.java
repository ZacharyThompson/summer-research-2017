package com.research.wuil.classification.classifier;

import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.OneClassClassifier;
import weka.classifiers.meta.generators.NominalGenerator;
import weka.classifiers.meta.generators.UniformDataGenerator;
import weka.core.Utils;

public class FileEventOneClassIBK extends OneClassClassifier {
    public FileEventOneClassIBK () {
        this.setTargetClassLabel("target");
        this.setNumericGenerator(new UniformDataGenerator());
        this.setNominalGenerator(new NominalGenerator());
        this.setTargetRejectionRate(0.0001);
        this.setProportionGenerated(0.5);
        this.setPercentageHeldout(10);

        IBk ibk = new IBk();
        try {
            ibk.setOptions(Utils.splitOptions("-I "));
        } catch (Exception e) { e.printStackTrace(); }
        this.setClassifier(ibk);
    }


}
