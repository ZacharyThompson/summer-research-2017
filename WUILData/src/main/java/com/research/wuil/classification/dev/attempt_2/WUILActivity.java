package com.research.wuil.classification.dev.attempt_2;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WUILActivity {
    private List<WUILDatum> data = new ArrayList<>();
    private String label;
    private HashMap<String, SummaryStatistics> statistics = new HashMap<>();
    private List <WUILGraph> graphs = new ArrayList<>();
    private HashMap<String, Integer> vocab = null;
    public List<Double> pathData = new ArrayList<>();

    public WUILActivity (String label) {
        this.label = label;
        this.statistics = newStatistics();
        this.graphs = newGraphs();
    }

    public WUILActivity (List<WUILDatum> data, String label) {
        this(label);
        this.data = data;
    }

    public void setVocab(HashMap<String, Integer> vocab) {
        this.vocab = vocab;
    }

    public void addData(String key) {
        WUILDatum datum = new WUILDatum(key, label);
        addData(datum);
    }

    public void addData(WUILDatum datum) {
        data.add(datum);
        for (WUILGraph graph : graphs) {
            graph.process(datum.getKey());
        }

        if (data.size() > 1) {
            long delta = ChronoUnit.SECONDS.between(data.get(data.size()-1).getDate(), data.get(data.size()-2).getDate());
            //statistics.get("time").addValue(delta);
        }
        pathData.add((double)datum.getDepth());
        statistics.get("path").addValue(datum.getDepth());
    }

    public int getSize() {
        return data.size();
    }

    public String toInstance() {
        return statisticsSummary() + "," + StringUtils.join(graphs, ",");
    }

    public List<WUILDatum> getData() {
        return data;
    }

    public Instance getInstance(Instances instances) {
        Instance inst = new DenseInstance(instances.numAttributes());
        inst.setDataset(instances);
        int index = 0;

        for (Double value : newFileStatistics()) {
            inst.setValue(index++, value);
        }

        for (String statistic : statistics.keySet()) {
            SummaryStatistics stats = statistics.get(statistic);
            inst.setValue(index++, stats.getMean());
            inst.setValue(index++, stats.getMax());
            inst.setValue(index++, stats.getMin());
            inst.setValue(index++, stats.getStandardDeviation());
            inst.setValue(index++, stats.getVariance());
        }

        for (WUILGraph graph : graphs) {
            for (Map.Entry<String, Double> attr : graph.getAttributes().entrySet()) {
                inst.setValue(index++, attr.getValue());
            }
        }

        String[] class_values = {"target", "outlier"};
        inst.setValue(index++, label);
        return inst;
    }

    public String getLabel() {
        return label;
    }

    public LocalDateTime startDate() {
        return data.size() > 0 ? data.get(0).getDate() : null;
    }

    public LocalDateTime endDate() {
        return data.size() > 0 ? data.get(data.size()-1).getDate() : null;
    }

    public String getTimespan() {
        return ((startDate() == null ? "NULL" : startDate())+ " - " +(endDate() == null ? "NULL" : endDate()));
    }

    public String statisticsSummary() {
        String summary = "";
        for (String key : statistics.keySet()) {
            SummaryStatistics stats = statistics.get(key);
            summary = String.format("%s,%.2f", summary, stats.getMean());
            summary = String.format("%s,%.2f", summary, stats.getMax());
            summary = String.format("%s,%.2f", summary, stats.getMin());
            summary = String.format("%s,%.2f", summary, stats.getStandardDeviation());
            summary = String.format("%s,%.2f", summary, stats.getVariance());
        }
        return summary.substring(0, summary.length());
    }

    public List<Double> newFileStatistics () {
        List<Double> values = new ArrayList<>();
        double unique = 0;
        double total = 0;
        if (vocab == null) {
            values.add(unique);
            values.add(total);
        } else {
            HashMap<String, Integer> replacements = new HashMap<>();
            for (WUILDatum datum : data) {
                int freq = vocab.containsKey(datum.getKey()) ? vocab.get(datum.getKey()) : 0;

                if (freq < 50) {
                    int count = replacements.containsKey(datum.getKey()) ? replacements.get(datum.getKey()) : 0;
                    replacements.put(datum.getKey(), count+1);
                    total++;
                }
            }
            unique = replacements.size();

            values.add(unique / (double) data.size());
            values.add(total / (double) data.size() );
        }

        return values;
    }

    private static List<WUILGraph> newGraphs () {

        List<WUILGraph> graphs = new ArrayList<>();
        WUILGraph graph;

        graph = new WUILGraphAccess("file_access");
        graphs.add(graph);

        graph = new WUILGraphAccess("ext_access");
        graph.addPreprocess((k) -> {
            String[] parts = k.split("\\\\");
            return Arrays.asList(parts[parts.length-1]);
        });
        graphs.add(graph);

        graph = new WUILGraphAccess("dir_access");
        graph.addPreprocess((k) -> {
            String[] parts = k.split("\\\\");
            List<String> dirs = new ArrayList<>();
            String dir = "";
            for (int i = 0; i < parts.length; i++) {
                dir = dir + parts[i];
                dirs.add(dir);
            }
            return dirs;
        });
        graphs.add(graph);

        graph = new WUILGraphTraverse("dir_traverse");
        graphs.add(graph);

        return graphs;
    }

    private static HashMap<String, SummaryStatistics> newStatistics() {
        HashMap<String, SummaryStatistics> statistics = new HashMap<>();
        //sstatistics.put("time", new SummaryStatistics());
        statistics.put("path", new SummaryStatistics());
        return statistics;
    }

    public static String getHeader() {
        String header = "@relation WUIL-data\n";

        header += String.format("@attribute new_files_unique numeric\n");
        header += String.format("@attribute new_files_total numeric\n");

        for (String statistic : newStatistics().keySet()) {
            header += String.format("@attribute %s_mean numeric\n", statistic);
            header += String.format("@attribute %s_max numeric\n", statistic);
            header += String.format("@attribute %s_min numeric\n", statistic);
            header += String.format("@attribute %s_std numeric\n", statistic);
            header += String.format("@attribute %s_var numeric\n", statistic);
        }

        for (WUILGraph graph : newGraphs()) {
            for (String attr : graph.getAttributes().keySet()) {
                header += String.format("@attribute %s", attr);
            }
        }
        header += "@attribute class {target, outlier}\n";
        header += "@data\n";
        return header;
    }

    public static ArrayList<Attribute> getAttributes() {
        ArrayList<Attribute> attributes = new ArrayList<>();

        attributes.add(new Attribute("new_files_unique"));
        attributes.add(new Attribute("new_files_total"));

        for (String statistic : newStatistics().keySet()) {
            attributes.add(new Attribute(String.format("%s_mean", statistic)));
            attributes.add(new Attribute(String.format("%s_max", statistic)));
            attributes.add(new Attribute(String.format("%s_min", statistic)));
            attributes.add(new Attribute(String.format("%s_std", statistic)));
            attributes.add(new Attribute(String.format("%s_var", statistic)));
        }

        for (WUILGraph graph : newGraphs()) {
            for (String attr : graph.getAttributes().keySet()) {
                attributes.add(new Attribute(attr));
            }
        }

        String[] class_values = {"target", "outlier"};
        attributes.add(new Attribute("class", Arrays.asList(class_values)));

        return attributes;
    }
}