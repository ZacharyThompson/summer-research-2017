package com.research.wuil.classification.classifier;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class RealTimeClassifier {
    private static final int MIN_SIZE = 20;
    private static final int WINDOW = 10;

    private Classifier classifier;
    private Instances instances;
    private ArrayList<Double> predictions = new ArrayList<>();
    private boolean built = false;
    private PrintWriter out;

    public RealTimeClassifier (Classifier classifier) {
        this.classifier = classifier;
    }

    public boolean isBuilt() {
        return built;
    }

    public boolean buildClassifier(Instances instances) {
        built = false;
        if (instances != null && instances.size() >= MIN_SIZE) {
            this.instances = instances;
            try {
                classifier.buildClassifier(instances);
                built = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return built;
    }

    public void reset(Instances instances) {
        built = false;
        predictions.clear();
        buildClassifier(instances);
    }

    public void reset() {
        built = false;
        predictions.clear();
    }

    public void predict(Instances instances) {
        if (built) {
            for (Instance instance : instances) {
                try {
                    double prediction = classifier.classifyInstance(instance);
                    predictions.add(prediction);
                    update();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setWriter(PrintWriter writer) {
        this.out = writer;
    }

    public void update() {
        int startIndex = (predictions.size() > WINDOW) ? predictions.size() - WINDOW : 0;
        List<Double> sub = predictions.subList(startIndex, predictions.size());

        Double average = sub.stream().mapToDouble(val -> val).average().getAsDouble();
        if (out != null) {
            out.println("prediction:" + average);
        }
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }
}
