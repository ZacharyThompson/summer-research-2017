package com.research.wuil.classification.data.feature;

import java.io.Serializable;
import java.util.List;

/**
 *
 * Processor interface; implementations will handle how the given data will determine a feature.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public interface Processor extends Serializable {
    /**
     * Processes the given data to a collection of Strings.
     *
     * @param data Data to be processed
     * @return Collection of Strings to be consumed by a Feature.
     */
    List<String> process(String data);
}
