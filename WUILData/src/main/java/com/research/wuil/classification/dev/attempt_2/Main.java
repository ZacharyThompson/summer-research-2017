package com.research.wuil.classification.dev.attempt_2;

import java.io.File;
import java.io.PrintWriter;

public class Main {
    public static void main (String args[]) {


        try {
            PrintWriter trainWriter = new PrintWriter(new File("E:/Research/WUIL Logs/train-wuil.arff"));
            PrintWriter testWriter = new PrintWriter(new File("E:/Research/WUIL Logs/test-wuil.arff"));

            trainWriter.println(WUILActivity.getHeader());
            trainWriter.flush();
            testWriter.println(WUILActivity.getHeader());
            testWriter.flush();

            int startUser = 1; int numUsers = 1;
            for (int i = startUser; i <= numUsers; i ++) {
                String user = "User" + i;
                System.out.println  ("Loading " + user);

                WUILDataIterator train = new WUILDataIterator(user, "target","User Log");
                WUILDataIterator negative = new WUILDataIterator(user, "outlier", "Attack 1 Log");
                WUILDataIterator positive = train.extract(negative.size());

                System.out.println(String.format("\tTrain Data : %d", train.size()));
                System.out.println(String.format("\tTest Data : %d + %d", negative.size(), positive.size()));

                while (train.hasNext()) {
                    trainWriter.println(String.format("%s,%s", train.next().toInstance(), positive.getLabel()));
                }

                while (positive.hasNext()) {
                    testWriter.println(String.format("%s,%s", positive.next().toInstance(), positive.getLabel()));
                }

                while (negative.hasNext()) {
                    testWriter.println(String.format("%s,%s", negative.next().toInstance(), negative.getLabel()));
                }

            }
            trainWriter.close();
            testWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
