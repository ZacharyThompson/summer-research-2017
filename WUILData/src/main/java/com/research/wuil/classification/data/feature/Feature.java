package com.research.wuil.classification.data.feature;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * Abstract Feature. Represents a unique feature used to produce a feature set for classification.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public abstract class Feature implements Serializable {
    /**
     * Name of the feature.
     */
    private String m_Name;

    /**
     * List of Preprocessors which handle how the data given is processed.
     */
    private Processor m_Preprocessor =  (Processor & Serializable) (e) -> Arrays.asList(e);

    /**
     * Constructor.
     *
     * @param name The name of this feature
     */
    public Feature(String name) {
        this.m_Name = name;
    }

    /**
     * Consume given data.
     *
     * @param key The data to be consumed.
     */
    public abstract void consume(String key);

    /**
     * Get Values.
     *
     * @return Feature values associated with this Feature.
     */
    public abstract Map<String, Double> getValues();

    /**
     * Add a preprocessor to handle consumed data.
     *
     * @param preprocessor The preprocessor to be added.
     */
    public void addPreprocess (Processor preprocessor) {
        this.m_Preprocessor = preprocessor;
    }

    /**
     * Process the data by iterating through each processor and consuming the the data produced.
     *
     * @param key The data to be processed and consequently consumed.
     */
    public void process(String key) {
        for (String string : m_Preprocessor.process(key)) {
            consume(key);
        }
    }

    /**
     * Get Name.
     *
     * @return Name of this Feature.
     */
    public String getName() {
        return m_Name;
    }

    /**
     * Sets the name.
     *
     * @param value New name.
     */
    public void setName(String value) {
        this.m_Name = value;
    }

    /**
     * The text tip for the name property.
     *
     * @return The text tip for the name property.
     */
    public String nameTipText() {
        return "The name of the feature";
    }

    /**
     * Retrieves the String representation of this Object.
     *
     * @return The Feature name.
     */
    @Override
    public String toString() {
        return this.getName();
    }

 }
