package com.research.wuil.classification.data.feature;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Feature representing the basic access statistics.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FeatureAccessStatistics extends Feature {
    /**
     * The collection of data that has been consumed. Used to generate statistics.
     */
    private List<String> m_Data = new ArrayList<>();

    /**
     * Constructor.
     */
    public FeatureAccessStatistics () { super("FeatureAccessStatistics"); }

    /**
     *  Constructor.
     *
     * @param name The name of this feature.
     */
    public FeatureAccessStatistics (String name) { super(name); }

    /**
     * Consume data implementation.
     *
     * @param key The data to be consumed.
     */
    @Override
    public void consume(String key) {
        m_Data.add(key);
    }

    /**
     * Get Values.
     *
     * @return Feature values associated with this Feature.
     */
    @Override
    public Map<String, Double> getValues() {
        LinkedHashMap<String, Double> values = new LinkedHashMap<>();
        values.put("access", (double) m_Data.size());
        return values;
    }
}
