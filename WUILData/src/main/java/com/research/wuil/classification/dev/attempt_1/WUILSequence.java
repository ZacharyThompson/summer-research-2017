package com.research.wuil.classification.dev.attempt_1;

import com.google.common.collect.Lists;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.util.ArrayList;
import java.util.List;

public class WUILSequence {
    public String label;
    public String user;
    private INDArray features;
    private INDArray labels;
    private INDArray mask;



    public WUILSequence(List<Float> sequence, String user, String label) {
        this.label = label;
        this.user = user;

        // Create Feature Vector
        float[] floatFeatures = new float[sequence.size()];
        for (int i = 0; i < sequence.size(); i++) {
            floatFeatures[i] = sequence.get(i);
        }
        this.features = Nd4j.create(floatFeatures, new int[]{1, sequence.size()}, 'f');

        // Create Label Vector
        this.labels = Nd4j.zeros(2, sequence.size(), 'f')
                .putScalar(new int[]{this.labelToInt(), sequence.size()-1}, 1);

    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < features.rows(); i++) {
            ret += features.getFloat(i) + " ";
        }
        return ret;
    }

    public String getLabel() {
        return label;
    }

    public int labelToInt() {
        return (label.equals("positive")) ? 1 : 0;
    }

    public static List<WUILSequence> SegmentSequence(List<Float> sequence, int segmentSize, String user, String type) {
        List<List<Float>> segments = new ArrayList<>(Lists.partition(sequence, segmentSize));
        List<WUILSequence> data = new ArrayList<>();
        String label = (type.equals("User Log")) ? "positive" : "negative";
        for (List<Float> segment : segments) {
            if (segment.size() < segmentSize) {
                continue;
            }
            data.add(new WUILSequence(segment, user, label));
        }
        return data;
    }

    public INDArray toFeatureVector() {
        return features;
    }

    public INDArray toLabelVector() {
        return labels;
    }

    public DataSet toDataSet() {
        DataSet data = new DataSet(this.toFeatureVector(), this.toLabelVector());
        //data.setLabelsMaskArray(Nd4j.zeros(sequence.size()).putScalar(new int[]{0, sequence.size() -1}, 1));
        return data;
    }
}