package com.research.wuil.classification.data.feature;

import com.research.wuil.classification.data.FileEvent;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Feature representing path statistics. More specifically, path depth.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FeaturePathStatistics extends Feature {
    /**
     * The data associated that has been consumed.
     */
    transient private List<String> m_Data = new ArrayList<>();

    /**
     * The statistics that have been collected of the path depth.
     */
    transient private SummaryStatistics m_Statistics = new SummaryStatistics();

    /**
     * Constructor.
     */
    public FeaturePathStatistics() { super("FeaturePathStatistics"); }

    /**
     * Constructor.
     *
     * @param name Name of the feature.
     */
    public FeaturePathStatistics(String name) {
        super (name);
    }

    /**
     * Consume the given data.
     *
     * @param key The data to be consumed.
     */
    @Override
    public void consume(String key) {
        m_Data.add(key);
        m_Statistics.addValue(FileEvent.pathDepth(key));
    }

    /**
     * Get the feature set.
     *
     * @return Map of names of features and their associated values.
     */
    @Override
    public Map<String, Double> getValues() {
        LinkedHashMap<String, Double> values = new LinkedHashMap<>();
        values.put("mean", m_Statistics.getMean());
        values.put("max", m_Statistics.getMax());
        values.put("min", m_Statistics.getMin());
        values.put("std", m_Statistics.getStandardDeviation());
        values.put("var", m_Statistics.getVariance());
        return values;
    }

    /**
     * Handles reinstantiation of transient member variables.
     * Avoids sending instance based data when the Object is serialized, as this information
     * should not be retained.
     *
     * @param in The input stream.
     * @throws IOException Exception thrown by invoking the default read object method.
     * @throws ClassNotFoundException Exception thrown by invoking the default read object method.
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        m_Data = new ArrayList<>();
        m_Statistics = new SummaryStatistics();
    }
}
