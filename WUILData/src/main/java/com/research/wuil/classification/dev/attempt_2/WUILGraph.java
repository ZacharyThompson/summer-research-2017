package com.research.wuil.classification.dev.attempt_2;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.*;

import static com.research.linear_algebra.BooleanOperations.*;

public abstract class WUILGraph {
    WUILGraphNode current = null;
    HashMap<String, WUILGraphNode> nodes = new HashMap<>();
    private String name;
    private Processor preprocessor =  (e) -> Arrays.asList(e);

    public static interface Processor {
        List<String> process (String data);
    }

    public WUILGraph(String name) {
        this.name = name;
    }

    public abstract void traverse(String key);

    public void print() {
        for (Iterator iter = nodes.entrySet().iterator(); iter.hasNext();) {
            System.out.println(iter.next());
        }
    }

    public int totalVertices() {
        return nodes.size() == 0 ? 1 : nodes.size();
    }

    public int directedEdges() {
        int edges = 0;
        for (Iterator iter = nodes.entrySet().iterator(); iter.hasNext();) {
            WUILGraphNode node = (WUILGraphNode) ((Map.Entry)iter.next()).getValue();
            edges += node.edges();
        }
        return edges;
    }

    public int undirectedEdges() {
        Set<String> edges = new HashSet<>();
        for (Iterator iter = nodes.entrySet().iterator(); iter.hasNext();) {
            WUILGraphNode node = (WUILGraphNode) ((Map.Entry)iter.next()).getValue();
            for (WUILGraphNode.WUILGraphNodeConnection connection : node.connections) {
                String s1 = connection.node.key + ":" + node.key;
                String s2 = node.key + ":" + connection.node.key;
                if (edges.contains(s1) || edges.contains(s2)) {
                    continue;
                }
                edges.add(s1);
            }
        }
        return edges.size();
    }

    public int totalEdges() {
        int edges = 0;
        for (Iterator iter = nodes.entrySet().iterator(); iter.hasNext();) {
            WUILGraphNode node = (WUILGraphNode) ((Map.Entry)iter.next()).getValue();
            edges += node.visits();
        }
        return edges;
    }

    public int maxUniqueOut() {
        int max = 0;
        for (WUILGraphNode node : nodes.values()) {
            max = Math.max(max, node.edges());
        }
        return max;
    }

    public int maxTotalOut() {
        int max = 0;
        for (WUILGraphNode node : nodes.values()) {
            max = Math.max(max, node.totalEdges());
        }
        return max;
    }

    public int maxUniqueIn() {
        HashMap<String, Integer> values = new HashMap<>();
        for (String key : nodes.keySet()) {
            values.put(key, 0);
        }
        int max = 0;
        for (WUILGraphNode node : nodes.values()) {
            for (WUILGraphNode.WUILGraphNodeConnection connection : node.connections) {
                String key = connection.node.getKey();
                int value = values.get(key) + 1;
                values.put(key, value);
                max = Math.max(value, max);
            }
        }
        return max;
    }

    public int maxTotalIn() {
        HashMap<String, Integer> values = new HashMap<>();
        for (String key : nodes.keySet()) {
            values.put(key, 0);
        }
        int max = 0;
        for (WUILGraphNode node : nodes.values()) {
            for (WUILGraphNode.WUILGraphNodeConnection connection : node.connections) {
                String key = connection.node.getKey();
                int value = values.get(key) + connection.visits;
                values.put(key, value);
                max = Math.max(value, max);
            }
        }
        return max;
    }

    public String toInstance() {
        String ret = "";
        for (Map.Entry<String, Double> entry : getAttributes().entrySet()) {
            ret += String.format("%s,%.2f", ret, entry.getValue());
        }
        return ret.substring(1, ret.length());
    }

    @Override
    public String toString() {
        return toInstance();
    }

    public double getVert() { return totalVertices(); }
    public double getEdgeRatio() { return totalEdges() / (double) totalVertices(); }
    public double getDirectedRatio() { return directedEdges() / (double) totalVertices(); }
    public double getUndirectedRatio() { return undirectedEdges() / (double) totalVertices(); }
    public double getMaxOutTotal() { return maxTotalOut() / (double) totalVertices(); }
    public double getMaxOutUnique() { return maxUniqueOut() / (double) totalVertices(); }
    public double getMaxInTotal() { return maxTotalIn() / (double) totalVertices(); }
    public double getMaxInUnique() { return maxUniqueIn() / (double) totalVertices(); }

    public RealMatrix toMatrix() {
        RealMatrix matrix = MatrixUtils.createRealMatrix(nodes.size(), nodes.size());

        int fKey = 0;
        for (WUILGraphNode fNode : nodes.values()) {
            int sKey = 0;
            for (WUILGraphNode sNode : nodes.values()) {
                WUILGraphNode.WUILGraphNodeConnection con = fNode.getConnection(sNode);
                matrix.addToEntry(fKey, sKey, con == null ? 0 : con.visits);
                sKey++;
            }
            fKey++;
        }
        return matrix;
    }

    public double determinant(RealMatrix matrix) {
        return new LUDecomposition(matrix).getDeterminant();
    }

    public static int connectedComponents(RealMatrix matrix) {
        RealMatrix trans = transitive(matrix);
        trans = intersection(trans, transpose(trans));
        return islandCount(trans);
    }

    public void addPreprocess (Processor preprocessor) {
        this.preprocessor = preprocessor;
    }

    public void process(String key) {
        for (String string : preprocessor.process(key)) {
            traverse(key);
        }
    }

    public String getName() {
        return name;
    }

    public HashMap<String, Double> getAttributes() {
        HashMap<String, Double> attributes = new LinkedHashMap<>();

        attributes.put(String.format("%s_vert", this.name), getVert());
        attributes.put(String.format("%s_edge_ratio", this.name), getEdgeRatio());
        attributes.put(String.format("%s_directed_ratio", this.name), getDirectedRatio());
        attributes.put(String.format("%s_undirected_ratio", this.name), getUndirectedRatio());
        attributes.put(String.format("%s_max_out_total", this.name), getMaxOutTotal());
        attributes.put(String.format("%s_max_out_unique", this.name), getMaxOutUnique());
        attributes.put(String.format("%s_max_in_total", this.name), getMaxInTotal());
        attributes.put(String.format("%s_max_in_unique", this.name), getMaxInUnique());

        return attributes;
    }

    public static class WUILGraphNode {
        private String key;
        public Set<WUILGraphNodeConnection> connections = new HashSet<>();
        public WUILGraphNode(String key) {
            this.key = key;
        }

        public void addConnection(WUILGraphNode node) {
            WUILGraphNodeConnection connection = null;
            connection = getConnection(node);
            if (connection == null) {
                connection = new WUILGraphNodeConnection(node);
                connections.add(connection);
            }
            connection.visits++;
        }

        public String getKey () {
            return key;
        }

        public int edges() {
            return connections.size();
        }

        public int totalEdges() {
            int count = 0;
            for (WUILGraphNodeConnection con : connections) {
                count += con.visits;
            };
            return count;
        }

        public int visits() {
            int visits = 0;
            for (WUILGraphNodeConnection connection : connections) {
                visits += connection.visits;
            }
            return visits;
        }

        @Override
        public String toString() {
            String ret = String.format("Node %d :", key);
            for (WUILGraphNodeConnection connection : connections) {
                ret += String.format(" [%d:%d]", connection.node.key, connection.visits);
            }
            return ret;
        }

        public boolean hasConnection(String key) {
            for (WUILGraphNodeConnection connection : connections) {
                if (connection.node.key == key) {
                    return true;
                }
            }
            return false;
        }

        public WUILGraphNodeConnection getConnection (WUILGraphNode node) {
            return getConnection(node.getKey());
        }

        public WUILGraphNodeConnection getConnection (String key) {
            for (WUILGraphNodeConnection con : connections) {
                if (con.node.getKey().equals(key)) {
                    return con;
                }
            }
            return null;
        }

        public static class WUILGraphNodeConnection {
            private WUILGraphNode node;
            private int visits;

            public WUILGraphNodeConnection(WUILGraphNode node) {
                this.node = node;
                this.visits = 0;
            }
        }
    }


}
