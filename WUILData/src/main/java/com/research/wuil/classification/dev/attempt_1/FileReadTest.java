package com.research.wuil.classification.dev.attempt_1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class FileReadTest {
    public static HashMap<String, List<List<Float>>> sequences = new HashMap<>();

    public static void main(String[] args) {
        getData();
    }

    public static WUILDataSetIterator getData() {
        String path = "E:/Research/WUIL Logs/data/";
        String user = "User1";

        List<String> files = new ArrayList<>();
        files.add("User Log");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        int maxLength = 0;
        for (String fileName : files) {
            System.out.println(fileName);
            List<List<Float>> sequence = new ArrayList<>();

            File file = new File(String.format("%s/%s/%s/log.txt", path, user, fileName));
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String line;
                Date start = null;
                String.valueOf(10);
                List<Float> seq = new ArrayList<>();
                while ((line = br.readLine()) != null) {
                    String[] data = line.split("\\|");
                    Date current = sdf.parse(data[1] + " " + data[2].replace(".", ""));
                    if (start == null) {
                        start = current;
                    }
                    long diffInMinutes = Math.abs(current.getTime() - start.getTime()) / (1000 * 60);
                    if (diffInMinutes > 1) {
                        maxLength = Math.max(maxLength, seq.size());
                        sequence.add(seq);
                        seq = new ArrayList<>();
                        start = null;
                    }
                    if (seq.size() > 200) {
                        maxLength = Math.max(maxLength, seq.size());
                        sequence.add(seq);
                        seq = new ArrayList<>();
                        start = current;
                    }
                    for (String val : data[5].split("\\\\")) {
                        seq.add(Float.parseFloat(val));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            sequences.put(fileName, sequence);
        }

        System.out.println("PADDING");
        WUILDataSetIterator data = new WUILDataSetIterator(100, maxLength);
        // Pad Sequences
        for (String key : sequences.keySet()) {
            System.out.println(key);
            for (List<Float> sequence : sequences.get(key)) {
                for (int i = sequence.size(); i < maxLength; i++) {
                    sequence.add(0f);
                }
                WUILSequence seq = new WUILSequence(sequence, user, key.equals("User Log") ? "positive" : "negative");
                data.AddSequence(seq);
            }
            sequences.put(key, null);
        }

        return data;

    }

}
