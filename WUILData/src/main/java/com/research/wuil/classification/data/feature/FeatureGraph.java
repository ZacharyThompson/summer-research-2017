package com.research.wuil.classification.data.feature;

import weka.classifiers.meta.OneClassClassifier;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 *
 * Feature representing the basic implementation of a directed graph used to extract
 * information from sequential events.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public abstract class FeatureGraph extends Feature {
    /**
     * Current m_Node
     */
    transient protected FeatureGraphNode m_Current = null;

    /**
     * Nodes within the graph.
     */
    transient protected HashMap<String, FeatureGraphNode> m_Nodes = new HashMap<>();

    /**
     * Constructor.
     *
     * @param name The name of the feature.
     */
    public FeatureGraph(String name) {
        super(name);
    }

    /**
     * Consume the supplied data.
     *
     * @param key The data to be consumed.
     */
    public abstract void consume(String key);

    /**
     * Get the total number of vertices.
     *
     * @return The total number of vertices.
     */
    private int totalVertices() {
        return m_Nodes.size() == 0 ? 1 : m_Nodes.size();
    }

    /**
     * Get the number of directed uniqueEdges.
     *
     * @return The number of directed uniqueEdges.
     */
    private int directedEdges() {
        int edges = 0;
        for (Map.Entry<String, FeatureGraphNode> entry : m_Nodes.entrySet()) {
            FeatureGraphNode node = entry.getValue();
            edges += node.uniqueEdges();
        }
        return edges;
    }

    /**
     * Get te number of undirected uniqueEdges.
     *
     * @return The number of undirected uniqueEdges.
     */
    private int undirectedEdges() {
        Set<String> edges = new HashSet<>();
        for (Map.Entry<String, FeatureGraphNode> entry : m_Nodes.entrySet()) {
            FeatureGraphNode node = entry.getValue();
            for (FeatureGraphNode.FeatureGraphNodeConnection connection : node.connections) {
                String s1 = connection.m_Node.m_Key + ":" + node.m_Key;
                String s2 = node.m_Key + ":" + connection.m_Node.m_Key;
                if (edges.contains(s1) || edges.contains(s2)) {
                    continue;
                }
                edges.add(s1);
            }
        }
        return edges.size();
    }

    /**
     * Get the number of total uniqueEdges.
     *
     * @return The number of total uniqueEdges.
     */
    private int totalEdges() {
        int edges = 0;
        for (Map.Entry<String, FeatureGraphNode> entry : m_Nodes.entrySet()) {
            FeatureGraphNode node = entry.getValue();
            edges += node.visits();
        }
        return edges;
    }

    /**
     * Get the maximum unique uniqueEdges out of a m_Node.
     *
     * @return The maximum unique uniqueEdges out of a m_Node.
     */
    private int maxUniqueOut() {
        int max = 0;
        for (FeatureGraphNode node : m_Nodes.values()) {
            max = Math.max(max, node.uniqueEdges());
        }
        return max;
    }

    /**
     * Get the maximum total uniqueEdges out of a m_Node.
     *
     * @return The maximum total uniqueEdges out of a m_Node.
     */
    private int maxTotalOut() {
        int max = 0;
        for (FeatureGraphNode node : m_Nodes.values()) {
            max = Math.max(max, node.totalEdges());
        }
        return max;
    }

    /**
     * Get the maximum unique uniqueEdges into a m_Node.
     *
     * @return The maximum unique uniqueEdges into a m_Node.
     */
    private int maxUniqueIn() {
        HashMap<String, Integer> values = new HashMap<>();
        for (String key : m_Nodes.keySet()) {
            values.put(key, 0);
        }
        int max = 0;
        for (FeatureGraphNode node : m_Nodes.values()) {
            for (FeatureGraphNode.FeatureGraphNodeConnection connection : node.connections) {
                String key = connection.m_Node.getKey();
                int value = values.get(key) + 1;
                values.put(key, value);
                max = Math.max(value, max);
            }
        }
        return max;
    }

    /**
     * Get the maximum total uniqueEdges into a m_Node.
     *
     * @return The maximum total uniqueEdges into a m_Node.
     */
    private int maxTotalIn() {

        HashMap<String, Integer> values = new HashMap<>();
        for (String key : m_Nodes.keySet()) {
            values.put(key, 0);
        }
        int max = 0;
        for (FeatureGraphNode node : m_Nodes.values()) {
            for (FeatureGraphNode.FeatureGraphNodeConnection connection : node.connections) {
                String key = connection.m_Node.getKey();
                int value = values.get(key) + connection.m_Visits;
                values.put(key, value);
                max = Math.max(value, max);
            }
        }
        return max;
    }

    /**
     * Convert this feature to an instance to be used for WEKA classification.
     *
     * @return The instance representation of this feature.
     */
    public String toInstance() {
        String ret = "";
        for (Map.Entry<String, Double> entry : getValues().entrySet()) {
            ret += String.format("%s,%.2f", ret, entry.getValue());
        }
        return ret.substring(1, ret.length());
    }

    /**
     * The String representation of this Feature.
     *
     * @return The instance version of this instance.
     */
    @Override
    public String toString() {
        return this.getName();
    }

    private double getVert() { return totalVertices(); }
    private double getEdgeRatio() { return totalEdges() / (double) totalVertices(); }
    private double getDirectedRatio() { return directedEdges() / (double) totalVertices(); }
    private double getUndirectedRatio() { return undirectedEdges() / (double) totalVertices(); }
    private double getMaxOutTotal() { return maxTotalOut() / (double) totalVertices(); }
    private double getMaxOutUnique() { return maxUniqueOut() / (double) totalVertices(); }
    private double getMaxInTotal() { return maxTotalIn() / (double) totalVertices(); }
    private double getMaxInUnique() { return maxUniqueIn() / (double) totalVertices(); }

    /**
     * Return the values of the features associated with this instance.
     *
     * @return Map mapping a name of a feature to it's value.
     */
    @Override
    public HashMap<String, Double> getValues() {
        LinkedHashMap<String, Double> attributes = new LinkedHashMap<>();

        if (m_NumberVertices) attributes.put(String.format("%s_vert", this.getName()), getVert());
        if (m_TotalEdgeRatio) attributes.put(String.format("%s_edge_ratio", this.getName()), getEdgeRatio());
        if (m_DirectedEdgeRatio) attributes.put(String.format("%s_directed_ratio", this.getName()), getDirectedRatio());
        if (m_UndirectedEdgeRatio) attributes.put(String.format("%s_undirected_ratio", this.getName()), getUndirectedRatio());
        if (m_MaxTotalEdgesOut) attributes.put(String.format("%s_max_out_total", this.getName()), getMaxOutTotal());
        if (m_MaxUniqueEdgesOut) attributes.put(String.format("%s_max_out_unique", this.getName()), getMaxOutUnique());
        if (m_MaxTotalEdgesIn) attributes.put(String.format("%s_max_in_total", this.getName()), getMaxInTotal());
        if (m_MaxUniqueEdgesIn) attributes.put(String.format("%s_max_in_unique", this.getName()), getMaxInUnique());
        return attributes;
    }

    /**
     * Handles reinstantiation of transient member variables.
     * Avoids sending instance based data when the Object is serialized, as this information
     * should not be retained.
     *
     * @param in The input stream.
     * @throws IOException Exception thrown by invoking the default read object method.
     * @throws ClassNotFoundException Exception thrown by invoking the default read object method.
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        m_Nodes = new HashMap<>();
    }

    // The properties
    private boolean m_NumberVertices = true;
    private boolean m_TotalEdgeRatio = true;
    private boolean m_DirectedEdgeRatio = true;
    private boolean m_UndirectedEdgeRatio = true;
    private boolean m_MaxTotalEdgesOut = true;
    private boolean m_MaxTotalEdgesIn = true;
    private boolean m_MaxUniqueEdgesOut = true;
    private boolean m_MaxUniqueEdgesIn = true;

    // The setters for the defined properties
    public void setNumberVertices (boolean b) { m_NumberVertices = b; }
    public void setTotalEdgeRatio (boolean b) { m_TotalEdgeRatio = b; }
    public void setDirectedEdgeRatio (boolean b) { m_DirectedEdgeRatio = b; }
    public void setUndirectedEdgeRatio (boolean b) { m_UndirectedEdgeRatio = b; }
    public void setMaxTotalEdgesOut (boolean b) { m_MaxTotalEdgesOut = b; }
    public void setMaxTotalEdgesIn (boolean b) { m_MaxTotalEdgesIn = b; }
    public void setMaxUniqueEdgesOut (boolean b) { m_MaxUniqueEdgesOut = b; }
    public void setMaxUniqueEdgesIn (boolean b) { m_MaxUniqueEdgesIn = b; }

    // The getters for the defined properties
    public boolean getNumberVertices () { return m_NumberVertices; }
    public boolean getTotalEdgeRatio () { return m_TotalEdgeRatio; }
    public boolean getDirectedEdgeRatio () { return m_DirectedEdgeRatio; }
    public boolean getUndirectedEdgeRatio () { return m_UndirectedEdgeRatio; }
    public boolean getMaxTotalEdgesOut () { return m_MaxTotalEdgesOut; }
    public boolean getMaxTotalEdgesIn () { return m_MaxTotalEdgesIn; }
    public boolean getMaxUniqueEdgesOut () { return m_MaxUniqueEdgesOut; }
    public boolean getMaxUniqueEdgesIn () { return m_MaxUniqueEdgesIn; }

    // The tip text for the defined properties
    public String numberVerticesTipText() { return "Whether to use the total number of vertices from the directed graph"; }
    public String totalEdgeRatioTipText() { return "Whether to use the total edge ratio from the directed graph"; }
    public String directedEdgeRatioTipText() { return "Whether to use the directed edge ratio from the directed graph"; }
    public String undirectedEdgeRatioTipText() { return "Whether to use the undirected edge ratio from the directed graph"; }
    public String maxTotalEdgesOutTipText() { return "Whether to use the total edges out from the directed graph"; }
    public String maxTotalEdgesInTipText() { return "Whether to use the total edges in from the directed graph"; }
    public String maxUniqueEdgesOutTipText() { return "Whether to use the unique edge out from the directed graph"; }
    public String maxUniqueEdgesInTipText() { return "Whether to use the unique edges in from the directed graph"; }

    /**
     * Returns a string describing this classes ability.
     *
     * @return A description of the method.
     */
    public String globalInfo() {
        return
                "A feature to be used in conjunction with the StringToFileEvent filter.\n\n"
                + "This feature is the base class for a directed graph which takes a series of File Events "
                + "as input and consumes the events, creating a series of nodes and connections which are "
                + "representative of the File Event sequence. \n"
                + "From the directed graph, various statistics can be extracted which were otherwise obscured "
                + "by the linear representation of the File Event sequence that was previously used.";
    }

    /**
     * Node within the Feature Directed Graph.
     */
    public static class FeatureGraphNode implements Serializable {
        /**
         * The key associated with this m_Node.
         */
        private String m_Key;

        /**
         * The set of connections associated with this m_Node.
         */
        private Set<FeatureGraphNodeConnection> connections = new HashSet<>();

        /**
         * Constructor
         *
         * @param key The key of this m_Node.
         */
        protected FeatureGraphNode(String key) {
            this.m_Key = key;
        }

        /**
         * Adds a connection to the specified m_Node.
         *
         * @param node The target m_Node.
         */
        public void addConnection(FeatureGraphNode node) {
            FeatureGraphNodeConnection connection = null;
            connection = getConnection(node);
            if (connection == null) {
                connection = new FeatureGraphNodeConnection(node);
                connections.add(connection);
            }
            connection.m_Visits++;
        }

        /**
         * Gets the key of this m_Node.
         *
         * @return The m_Node's key.
         */
        public String getKey () {
            return m_Key;
        }

        /**
         * Gets the number of unique edges.
         *
         * @return The number of unique edges.
         */
        public int uniqueEdges() {
            return connections.size();
        }

        /**
         * Gets the total number of edges.
         *
         * @return The number of total edges.
         */
        public int totalEdges() {
            int count = 0;
            for (FeatureGraphNodeConnection con : connections) {
                count += con.m_Visits;
            };
            return count;
        }

        /**
         * Gets the number of m_Visits to this m_Node.
         *
         * @return The number of m_Visits.
         */
        public int visits() {
            int visits = 0;
            for (FeatureGraphNodeConnection connection : connections) {
                visits += connection.m_Visits;
            }
            return visits;
        }

        /**
         * String representation of this Object as its number and the number of connections to it.
         *
         * @return This m_Node and add connections made to it.
         */
        @Override
        public String toString() {
            String ret = String.format("Node %s :", m_Key);
            for (FeatureGraphNodeConnection connection : connections) {
                ret += String.format(" [%s:%d]", connection.m_Node.m_Key, connection.m_Visits);
            }
            return ret;
        }

        /**
         * Determines whether this m_Node has a connection to a given m_Node.
         *
         * @param key Key associated with the target m_Node.
         * @return Whether there is an established connection.
         */
        public boolean hasConnection(String key) {
            for (FeatureGraphNodeConnection connection : connections) {
                if (connection.m_Node.m_Key.equals(key)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Gets the connection associated with a m_Node.
         *
         * @param node The m_Node to get the connection to.
         * @return The connection between the two m_Nodes.
         */
        private FeatureGraphNodeConnection getConnection (FeatureGraphNode node) {
            return getConnection(node.getKey());
        }

        /**
         * Gets the connection associated with a m_Node, using a provided String.
         *
         * @param key The key of the m_Node.
         * @return The connection between the two m_Nodes.
         */
        private FeatureGraphNodeConnection getConnection (String key) {
            for (FeatureGraphNodeConnection con : connections) {
                if (con.m_Node.getKey().equals(key)) {
                    return con;
                }
            }
            return null;
        }

        /**
         * Connection to another feature node.
         */
        private static class FeatureGraphNodeConnection implements Serializable {
            /**
             * The node this connection is connected to.
             */
            private FeatureGraphNode m_Node;
            /**
             * Number of times this connection has been travelled.
             */
            private int m_Visits;

            /**
             * Constructor.
             *
             * @param node Node to connect to.
             */
            private FeatureGraphNodeConnection(FeatureGraphNode node) {
                this.m_Node = node;
                this.m_Visits = 0;
            }
        }

    }

}
