package com.research.wuil.classification.data;

import com.research.wuil.classification.data.feature.Features;
import weka.core.Attribute;
import weka.core.Instances;

import java.io.File;
import java.util.List;

/**
 *
 * The class representing a data set populated with FileEventInstance (many instances).
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileEventInstances extends Instances {

    /**
     * Constructor.
     * Will read data from a given file and parse to a set of Instances if they
     * are of appropriate format.
     *
     * @param name The name to be given to the data set.
     * @param file The file of which data should be read from.
     * @param label The class label to be assigned to the instances present.
     */
    public FileEventInstances(String name, File file, String label) {
        this(name, Features.getDefaultFeatures(), file, label);
    }

    /**
     * Constructor.
     * Will read data from a given file and parse to a set of Instances if they
     * are of appropriate format.
     *
     * @param name The name to be given to the data set.
     * @param features The set of features that will represent the present instances.
     * @param file The file of which data should be read the data from.
     * @param label The class label to be assigned to instances present.
     */
    private FileEventInstances(String name, Features features, File file, String label) {
        this(name, features, FileEventObservation.ObservationsFromFile(file, label));
    }

    /**
     * Constructor.
     * Creates a data set from a collection of FileEventObservations.
     *
     * @param name The name to be given to the data set.
     * @param data The collection of FileEventObservations.
     */
    public FileEventInstances(String name, List<FileEventObservation> data) {
        this(name, Features.getDefaultFeatures(), data);
    }

    /**
     * Constructor.
     * Creates a data set from a collection of FileEventObservations.
     *
     * @param name The name to be given to the data set.
     * @param features The set of features that will represent the present instances.
     * @param data The collection of FileEventObservations.
     */
    private FileEventInstances(String name, Features features, List<FileEventObservation> data) {
        super(name, features.getAttributes(), data.size());

        for (FileEventObservation observation : data) {
            this.add(new FileEventInstance(this, features, observation));
        }

        List<Attribute> attributes = features.getAttributes();
        this.setClassIndex(attributes.size()-1);
    }

    /**
     * Constructor.
     * Merge two FileEventInstances into a single, maintaining respective class labels.
     *
     * @param inst1 The first Object of FileEventInstances.
     * @param inst2 The second Object of FileEventInstances.
     */
    public FileEventInstances(FileEventInstances inst1, FileEventInstances inst2) {
        super (inst1, inst1.size() + inst2.size());
        this.addAll(inst1);
        this.addAll(inst2);
        this.setClassIndex(inst1.classIndex());
    }

    /**
     * Append one FileEventInstances Object to another, returning the result as a new Object.
     *
     * @param inst1 The first Object of FileEventInstances.
     * @param inst2 The second Object of FileEventInstances.
     * @return The combined FileEventInstances as a distinct Object.
     */
    public static FileEventInstances append(FileEventInstances inst1, FileEventInstances inst2) {
        if (inst1 == null) return inst2;
        if (inst2 == null) return inst1;
        return new FileEventInstances(inst1, inst2);
    }
}
