package com.research.wuil.classification.dev.attempt_2;

public class WUILGraphAccess extends WUILGraph {
    public WUILGraphAccess(String name) {
        super(name);
    }

    public void traverse(String key) {
        if (!nodes.containsKey(key)) {
            WUILGraphNode node = new WUILGraphNode(key);
            nodes.put(key, node);
            if (current == null) {
                current = node;
                return;
            }
        }
        WUILGraphNode target = nodes.get(key);
        current.addConnection(target);
        this.current = target;
    }
}
