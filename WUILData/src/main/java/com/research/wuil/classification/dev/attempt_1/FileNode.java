package com.research.wuil.classification.dev.attempt_1;

import java.util.ArrayList;
import java.util.List;

public class FileNode {
    private List<FileNode> children = new ArrayList<FileNode>();
    private FileNode parent = null;
    private int key;
    private int count = 0;

    public FileNode(int key) {
        this.key = key;
        this.parent = this;
    }

    public FileNode (int key, FileNode parent) {
        this.key = key;
        this.parent = parent;
    }

    public void AddFile (List<Integer> dirs) {
        count++;
        if (this.getKey() == dirs.get(dirs.size() - 1)) {
            return;
        }
        int index = dirs.indexOf(this.key);
        if (index >= 0) {
            dirs = dirs.subList(index+1, dirs.size());
            index = indexOfChild(dirs.get(0));
            if (index >= 0) {
                children.get(index).AddFile(dirs);
            } else {
                FileNode node = new FileNode(dirs.get(0));
                this.addChild(node);
                node.AddFile(dirs);
            }

        } else {
            count--;
            if (this.parent != this) {
                this.parent.AddFile(dirs);
            }
        }
    }

    public void AddPath(String path) {
        List<Integer> dirs = new ArrayList<Integer>();
        for (String dir : path.split("\\\\")) {
            dirs.add(Integer.parseInt(dir));
        }
        this.AddFile(dirs);
    }

    public void setParent(FileNode parent) {
        this.parent = parent;
    }

    public void addChild(int key) {
        FileNode child = new FileNode(key, this);
        children.add(child);
    }

    public void addChild(FileNode child) {
        child.setParent(this);
        children.add(child);
    }

    public FileNode getParent() {
        return parent;
    }

    public List<FileNode> getChildren() {
        return children;
    }

    public int getKey() {
        return key;
    }

    public int getCount() {
        return count;
    }

    public void incrementCount() {
        count++;
    }

    public void decrementCount() {
        count--;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (FileNode.class.isAssignableFrom(obj.getClass())) {
            return ((FileNode) obj).getKey() == this.getKey();
        }
        if (obj instanceof Integer){
            return ((Integer) obj) == this.getKey();
        }

        return false;
    }

    public void print(String indent) {
        System.out.println(indent + this.getKey() + " : " + this.getChildren().size() + " : " + this.getCount());
        for (FileNode child : children) {
            child.print(indent + " ");
        }
    }

    public int indexOfChild (int key) {
        int i = 0;
        for (FileNode node : children) {
            if (node.getKey() == key) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int indexOfChild (FileNode child) {
        return indexOfChild(child.getKey());
    }

    public List<Float> getSequence(String path) {
        List<Integer> dirs = new ArrayList<Integer>();
        for (String dir : path.split("\\\\")) {
            dirs.add(Integer.parseInt(dir));
        }
        return getSequence(dirs);
    }

    public List<Float> getSequence(List<Integer> dirs) {
        List<Float> seq = new ArrayList<Float>();
        seq.add((float)this.getCount()/this.parent.getCount());
        int index = dirs.indexOf(this.key);
        if (this.getKey() == dirs.get(0) && dirs.size() == 1) {
            return seq;
        }
        if (index >= 0) {
            dirs = dirs.subList(index+1, dirs.size());
            index = indexOfChild(dirs.get(0));
            if (index >= 0) {
                List<Float> ret = children.get(index).getSequence(dirs);
                if (ret == null) {
                    return null;
                }
                seq.addAll(ret);
                return seq;
            } else {
                System.out.print(" -1 ");
                return null;
            }
        } else {
            if (this.parent != this) {
                return this.parent.getSequence(dirs);
            }
        }
        return null;
    }

    public FileNode traverseTo(String path, List<Float> sequence) {
        List<Integer> dirs = new ArrayList<Integer>();
        for (String dir : path.split("\\\\")) {
            dirs.add(Integer.parseInt(dir));
            //sequence.add(Float.parseFloat(dir));
        }

        //String[] paths = path.split("\\\\");

        //sequence.add(Float.parseFloat(paths[paths.length-1]));
        //return this;
        return traverseTo(dirs, sequence);
    }

    public FileNode traverseTo(List<Integer> dirs, List<Float> sequence) {
        float seq = (float)this.getCount()/this.parent.getCount();
        int index = dirs.indexOf(this.key);
        if (dirs.get(dirs.size() - 1) == this.getKey()) {
            sequence.add(seq);
            return this;
        }
        if (index >= 0) {
            sequence.add(seq);
            dirs = dirs.subList(index+1, dirs.size());
            index = indexOfChild(dirs.get(0));
            if (index >= 0) {
                return this.getChildren().get(index).traverseTo(dirs, sequence);
            } else {
                return this;
            }
        } else {
            sequence.add((float) 0);
            if (this.parent != this) {
                return this.parent.traverseTo(dirs, sequence);
            }
        }
        return null;
    }

}