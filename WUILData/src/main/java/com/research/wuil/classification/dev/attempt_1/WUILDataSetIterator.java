package com.research.wuil.classification.dev.attempt_1;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class WUILDataSetIterator implements DataSetIterator {
    private int batchSize;
    private int current = 0;
    private int segmentSize;
    private String path;
    private List<String> files = new ArrayList<>();
    public List<WUILSequence> data = new ArrayList<>();
    public DataSetPreProcessor preProcessor;
    private Mode mode = Mode.RECURRENT;
    private LearnType learnType = LearnType.SUPERVISED;
    private String name = "Dataset";

    public enum Mode {
        RECURRENT, CONVOLUTIONAL
    }
    public enum LearnType {
        SUPERVISED, UNSUPERVISED
    }

    public WUILDataSetIterator(String path, int batchSize, int segmentSize) {
        this.batchSize = batchSize;
        this.path = path;
        this.segmentSize = segmentSize;
        files.add("Attack 1 Log");
        files.add("Attack 2 Log");
        files.add("Attack 3 Log");
        files.add("User Log");
    }

    public WUILDataSetIterator(int batchSize, int segmentSize) {
        this.batchSize = batchSize;
        this.segmentSize = segmentSize;
    }

    public void LoadUser(String user) {
        /* Create User Tree */
        String fileName = "User Log";
        File file = new File(String.format("%s/%s/%s/log.txt", path, user, fileName));
        FileNode root = new FileNode(0);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                line = line.split("\\|")[5];
                root.AddPath(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (String type : files) {
            file = new File(String.format("%s/%s/%s/log.txt", path, user, type));
            List<Float> fullSeq = new ArrayList<Float>();
            FileNode node = root;
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = br.readLine()) != null) {
                    line = line.split("\\|")[5];
                    node = root.traverseTo(line, fullSeq);
                    if (node == null) {
                        node = root;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            data.addAll(WUILSequence.SegmentSequence(fullSeq, segmentSize, user, type));
        }
        Collections.shuffle(data);
    }

    public String getName() {
        return name;
    }

    public void EvenlyDistribute() {
        List<WUILSequence> positive = new ArrayList();
        List<WUILSequence> negative = new ArrayList();
        for (WUILSequence sequence : data) {
            if (sequence.label.equals("positive")) {
                positive.add(sequence);
            } else {
                negative.add(sequence);
            }
        }
        positive = positive.subList(0, Math.min(positive.size(), negative.size()));
        negative = negative.subList(0, Math.min(positive.size(), negative.size()));

        positive.addAll(negative);
        data = positive;
    }

    public void AddSequence(WUILSequence seq) {
        data.add(seq);
    }

    public void setName(String name) {
        this.name = name;
    }

    public WUILDataSetIterator ExtractIterator (int size, List<String> labels, boolean EvenDistribution) {
        List<List<String>> lblChecker;
        if (EvenDistribution) {
            lblChecker = chopped(labels, 1);
        } else {
            lblChecker = chopped(labels, labels.size());
        }

        WUILDataSetIterator extracted = new WUILDataSetIterator(batchSize, segmentSize);
        float stopSize = 0;
        for (List<String> lbl : lblChecker) {
            stopSize += size / lblChecker.size();
            for (Iterator<WUILSequence> iter = data.iterator(); iter.hasNext() && extracted.totalExamples() < stopSize;) {
                WUILSequence seq = iter.next();
                if (lbl.contains(seq.label)) {
                    extracted.AddSequence(seq);
                    iter.remove();
                }
            }
            this.shuffle();
            extracted.shuffle();
        }
        return extracted;
    }

    public void shuffle() {
        Collections.shuffle(data);
    }

    public void printStatistics() {
        int positive = 0;
        int negative = 0;
        for (WUILSequence seq : data) {
            if (seq.getLabel().equals("positive")) {
                positive++;
            } else {
                negative++;
            }
        }

        System.out.println("Statistics : " + this.name);
        System.out.println("  Positive Count : " + positive);
        System.out.println("  Negative Count : " + negative);

    }

    public DataSet next(int i) {
        i = Math.min(i, totalExamples() - current - 1);

        INDArray features = Nd4j.create(new int[]{i, 1, segmentSize}, 'f');
        INDArray labels = Nd4j.create(new int[]{i, 2, segmentSize}, 'f');
        //INDArray labels = Nd4j.zeros(i, 2, segmentSize);
        INDArray mask = Nd4j.create(new int[]{i, segmentSize}, 'f');
        //INDArray mask = Nd4j.zeros(i, segmentSize);
        for (int iter = 0; iter < i; iter++) {
            mask.putScalar(iter, segmentSize-1, 1);
        }
        current++;
        for (int count = 0; count < i; count++) {
            features.putRow(count, data.get(current).toFeatureVector());
            labels.putRow(count, data.get(current).toLabelVector());
            current++;
            if (current >= totalExamples()) {
                break;
            }
        }
        DataSet ret;
        if (learnType == LearnType.SUPERVISED) {
            ret = new DataSet(features, labels);
            ret.setLabelsMaskArray(mask);
        } else {
            ret = new DataSet(features, features);
        }
        if (this.preProcessor != null) {
            this.preProcessor.preProcess(ret);
        }
        return ret;
    }

    public int totalExamples() {
        return data.size();
    }

    public int inputColumns() {
        return data.size();
    }

    public int totalOutcomes() {
        return data.size();
    }

    public boolean resetSupported() {
        return false;
    }

    public boolean asyncSupported() {
        return false;
    }

    public void reset() {
        current = 0;
    }

    public int batch() {
        return batchSize;
    }

    public int cursor() {
        return 0;
    }

    public int numExamples() {
        return data.size();
    }

    public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
        this.preProcessor = dataSetPreProcessor;
    }

    public DataSetPreProcessor getPreProcessor() {
        return preProcessor;
    }

    public List<String> getLabels() {
        return null;
    }

    public boolean hasNext() {
        return (current < totalExamples());
    }

    public DataSet next() {
        return next(batchSize);
    }

    public void remove() {
        //ToDo
    }

    static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }
    public void setLearnType(LearnType learnType) {
        this.learnType = learnType;
    }
}
