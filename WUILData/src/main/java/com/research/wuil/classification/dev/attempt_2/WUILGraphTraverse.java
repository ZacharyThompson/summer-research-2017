package com.research.wuil.classification.dev.attempt_2;

public class WUILGraphTraverse extends WUILGraph {
    public WUILGraphTraverse(String name) {
        super(name);

        WUILGraphNode node = new WUILGraphNode("R");
        this.nodes.put("R", node);
        this.current = node;
    }

    public void traverse(String targetKey) {
        targetKey = "R" + "\\" + targetKey;

        if (this.current.getKey().equals(targetKey)) {
            this.current.addConnection(this.current);
            return;
        }

        String[] targetDirs = targetKey.split("\\\\");
        String[] currentDirs = this.current.getKey().split("\\\\");

        WUILGraphNode targetNode = nodes.get(targetKey);
        if (targetNode == null) {
            targetNode = new WUILGraphNode(targetKey);
            nodes.put(targetKey, targetNode);
        }
        String intKey = "";
        for (int i = 0; i < targetDirs.length && i < currentDirs.length; i++) {
            if (!targetDirs[i].equals(currentDirs[i])) {
                break;
            }
            intKey = String.join("\\", intKey, targetDirs[i]);
        }
        intKey = intKey.substring(1, intKey.length());
        WUILGraphNode intNode = nodes.get(intKey);
        if (intNode == null) {
            intNode = new WUILGraphNode(intKey);
            nodes.put(intKey, intNode);
        }
        current.addConnection(intNode);
        intNode.addConnection(targetNode);
        this.current = targetNode;
    }
}
