package com.research.wuil.classification.data.feature;

/**
 *
 * Feature representative of file access.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FeatureGraphAccess extends FeatureGraph {
    /**
     * Constructor.
     */
    public FeatureGraphAccess() { super("FeatureGraphAccess"); }

    /**
     * Constructor.
     *
     * @param name Name of feature.
     */
    public FeatureGraphAccess(String name) {
        super(name);
    }

    /**
     * Consume the provided data.
     *
     * @param key The data to be consumed.
     */
    public void consume(String key) {
        if (!m_Nodes.containsKey(key)) {
            FeatureGraphNode node = new FeatureGraphNode(key);
            m_Nodes.put(key, node);
            if (m_Current == null) {
                m_Current = node;
                return;
            }
        }
        FeatureGraphNode target = m_Nodes.get(key);
        m_Current.addConnection(target);
        this.m_Current = target;
    }
}
