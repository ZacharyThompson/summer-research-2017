package com.research.wuil.classification.dev.attempt_2;

import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.meta.OneClassClassifier;
import weka.classifiers.meta.generators.NominalGenerator;
import weka.classifiers.meta.generators.UniformDataGenerator;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils;
import weka.filters.AllFilter;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

import static java.nio.file.StandardOpenOption.APPEND;

public class Tester {
    public static final File trainFile = new File("E:/Research/WUIL Logs/train-wuil.csv");
    public static final File testFile = new File("E:/Research/WUIL Logs/test-wuil.csv");

    public static void main(String[] args) throws Exception {
        for (int i = 1; i <= 76; i++) {
            TrainUser("User" + i);
        }
    }

    public static void TrainUser(String user) throws Exception {
        WUILDataIteratorTime train = new WUILDataIteratorTime(user, "User Log");
        WUILDataIteratorTime test = new WUILDataIteratorTime(user, "Attack 1 Log", "Attack 2 Log", "Attack 3 Log");

        test.append(train.extract(test.size()));
        train.trim(1000);
        HashMap<String, Integer> vocab = train.createVocab();

        if (train.size() < 100) {
            return;
        }


        train.setVocab(vocab);
        test.setVocab(vocab);

        Instances trainData = train.getDataset();
        Instances testData = test.getDataset();

        FilteredClassifier model = new FilteredClassifier();
        model.setFilter(new AllFilter());

        OneClassClassifier occ = new OneClassClassifier();
        occ.setTargetClassLabel("target");
        occ.setNumericGenerator(new UniformDataGenerator());
        occ.setNominalGenerator(new NominalGenerator());
        occ.setTargetRejectionRate(0.1);
        occ.setProportionGenerated(0.5);
        occ.setPercentageHeldout(10);

        IBk ibk = new IBk();
        ibk.setOptions(Utils.splitOptions("-I "));
        occ.setClassifier(ibk);
        model.setClassifier(occ);

        model.buildClassifier(trainData);

        Evaluation eval = new Evaluation(testData);
        eval.evaluateModel(model, testData);
        System.out.println(user + " : " + Arrays.deepToString(eval.confusionMatrix()) + " : " + eval.pctCorrect());
        //System.m_Out.println(eval.toSummaryString(String.format("----- %s -----", user), false));
    }

    public static void createDatasets() throws Exception {

        OutputStream stream = Files.newOutputStream(Paths.get("E:/Research/WUIL Logs/train-wuil.csv"), APPEND);
        PrintWriter trainWriter = new PrintWriter(stream);

        PrintWriter testWriter = new PrintWriter(testFile);
        trainWriter.append(WUILActivity.getHeader());
        trainWriter.flush();
        testWriter.append(WUILActivity.getHeader());
        testWriter.flush();

        System.exit(0);

        int startUser = 1; int numUsers = 1;
        for (int i = startUser; i <= numUsers; i ++) {
            String user = "User" + i;
            System.out.println  ("Loading " + user);


            WUILDataIteratorTime train = new WUILDataIteratorTime(user, "User Log");
            WUILDataIteratorTime negative = new WUILDataIteratorTime(user, "Attack 1 Log");
            train.clean();
            negative.clean();
            WUILDataIteratorTime positive = train.extract(20);
            train.trim(500);

            System.exit(0);

            System.out.println(String.format("\tTrain Data : %d", train.size()));
            System.out.println(String.format("\tTest Data : %d %d", negative.size(), positive.size()));

            while (train.hasNext()) {
                WUILActivity instance = train.next();
                break;
                //trainWriter.println(String.format("%s,%s", instance.toInstance(), "target"));
            }

            while (negative.hasNext()) {
                WUILActivity instance = negative.next();
                String line = "0";
                double last = -1;

                for (double val : instance.pathData) {
                    if (last == val) {
                        continue;
                    }
                    line = line.format("%s,%.2f", line, val);
                }
                trainWriter.print(line + "\n");
                //testWriter.println(String.format("%s,%s", instance.toInstance(), "outlier"));
            }

            while (positive.hasNext()) {
                WUILActivity instance = positive.next();
                String line = "1";
                double last = -1;
                for (double val : instance.pathData) {
                    if (last == val) {
                        continue;
                    }
                    line = line.format("%s,%.2f", line, val);
                }
                trainWriter.print(line + "\n");
                //testWriter.println(String.format("%s,%s", instance.toInstance(), "target"));
            }

        }

        trainWriter.close();
        testWriter.close();
    }


    public static void simulate() throws Exception {
        int startUser = 1; int numUsers = 76;
        for (int i = startUser; i <= numUsers; i ++) {
            PrintWriter trainWriter = new PrintWriter(trainFile);
            PrintWriter testWriter = new PrintWriter(testFile);


            trainWriter.println(WUILActivity.getHeader());
            trainWriter.flush();
            testWriter.println(WUILActivity.getHeader());
            testWriter.flush();

            String user = "User" + i;
            System.out.println  ("Loading " + user);

            WUILDataIteratorTime train = new WUILDataIteratorTime(user, "User Log");
            WUILDataIteratorTime negative = new WUILDataIteratorTime(user, "Attack 1 Log");

            train.clean();
            train.trim(500);

            System.out.println(String.format("\tTrain Data : %d", train.size()));
            System.out.println(String.format("\tTest Data : %d", negative.size()));

            while (train.hasNext()) {
                WUILActivity instance = train.next();
                if (instance.getSize() <= 5) {
                    continue;
                }
                trainWriter.println(String.format("%s,%s", instance.toInstance(), "target"));
            }

            while (negative.hasNext()) {
                WUILActivity instance = negative.next();
                if (instance.getSize() <= 5) {
                    continue;
                }
                testWriter.println(String.format("%s,%s", instance.toInstance(), "outlier"));
            }
            trainWriter.close();
            testWriter.close();

            // Evaluate
            ConverterUtils.DataSource trainSource = new ConverterUtils.DataSource(trainFile.getPath());
            ConverterUtils.DataSource testSource = new ConverterUtils.DataSource(testFile.getPath());

            Instances trainData = trainSource.getDataSet();
            Instances testData = testSource.getDataSet();

            if (testData.size() == 0 || trainData.size()     == 0) {
                continue;
            }

            if (trainData.classIndex() == -1) {
                trainData.setClassIndex(trainData.numAttributes()-1);
            }
            if (testData.classIndex() == -1) {
                testData.setClassIndex(testData.numAttributes()-1);
            }

            OneClassClassifier occ = new OneClassClassifier();
            occ.setOptions(Utils.splitOptions("-num \"weka.classifiers.meta.generators.UniformDataGenerator -S 1 -L 0.0 -U 1.0\" -nom \"weka.classifiers.meta.generators.NominalGenerator -S 1\" -trr 0.1 -tcl target -cvr 10 -cvf 10.0 -P 0.5 -S 1 -W weka.classifiers.lazy.IBk -num-decimal-places 3 -- -K 1 -W 0 -I -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\""));
            occ.buildClassifier(trainData);

            Evaluation eval = new Evaluation(testData);
            eval.evaluateModel(occ, testData);
            System.out.println(eval.toSummaryString(String.format("----- %s -----", user), false));

        }
    }

    static{
        libsvm.svm.svm_set_print_string_function(new libsvm.svm_print_interface() {
            @Override
            public void print(String s) {
            } // Disables svm output
        });
    }
}
