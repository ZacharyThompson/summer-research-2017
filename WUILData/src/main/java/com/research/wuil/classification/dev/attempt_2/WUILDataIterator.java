package com.research.wuil.classification.dev.attempt_2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class WUILDataIterator implements Iterator<WUILActivity> {
    private static final File path = new File("E:/Research/WUIL Logs/data/");
    private static final int segmentSize = 50;
    private static final Random random = new Random(100);
    private List<WUILActivity> data = new ArrayList<>();
    private String label;
    private int cursor = 0;

    public WUILDataIterator(String user, String label, String... filenames) {
        this.label = label;
        for (String filename : filenames) {
            File file = new File(path, user + "/" + filename + "/log.txt");
            if (!file.exists()) {
                return;
            }

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;
                WUILActivity current = new WUILActivity(label);
                data.add(current);
                while ((line = bufferedReader.readLine()) != null) {
                    if (current.getSize() < segmentSize) {
                        current.addData(line);
                    } else {
                        current = new WUILActivity(label);
                        data.add(current);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            if (data.get(data.size()-1).getSize() < segmentSize) {
                data.remove(data.size()-1);
            }
        }
    }

    public WUILDataIterator(List<WUILActivity> data, String label) {
        this.data = data;
        this.label = label;
    }

    public WUILDataIterator extract(int size) {
        WUILDataIterator extract = new WUILDataIterator(data.subList(0, Math.min(size, data.size())), label);
        this.data = data.subList(Math.min(size, data.size()), data.size());
        return extract;
    }

    public int size () {
        return data.size();
    }

    public void trim(int size) {
        Collections.shuffle(data, random);
        data = data.subList(0, Math.min(data.size(), size));
    }

    public String getLabel() {
        return label;
    }

    @Override
    public boolean hasNext() {
        return cursor < this.size();
    }

    @Override
    public WUILActivity next() {
        return (cursor < size()) ? data.get(cursor++) : null;
    }

}
