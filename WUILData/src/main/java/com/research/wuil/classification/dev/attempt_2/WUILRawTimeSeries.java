package com.research.wuil.classification.dev.attempt_2;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class WUILRawTimeSeries {
    private static File path = new File("E:/Research/WUIL Logs/data/");

    //private HashMap<LocalDateTime, List<WUILDatum>> data = new HashMap();
    private List<WUILDatum> data = new ArrayList<>();
    private Set<LocalDateTime> times = new HashSet<>();
    List<WUILObservation> observations = new ArrayList<>();


    public WUILRawTimeSeries(String user, String filename) {
        File file = new File(path, user + "/" + filename + "/log.txt");
        if (!file.exists()) { return; }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                WUILDatum datum = new WUILDatum(line);
                times.add(datum.date);
                data.add(datum);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        String label = filename.equals("User Log") ? "target" : "outlier";
        observations = getObservations(times, 30, label);

        for (WUILObservation observation : observations) {
            observation.addData(data);
            observation.graph();
        }

        Collections.shuffle(observations);
    }

    public List<WUILObservation> getObservations(Set<LocalDateTime> timeSet, int interval, String label) {
        List<LocalDateTime> times = new ArrayList<>(timeSet);
        Collections.sort(times);
        List<WUILObservation> ranges = new ArrayList<>();

        for (int startI = 0; startI < times.size(); startI++) {
            LocalDateTime start = times.get(startI);
            for (int endI = startI + 1; endI < times.size(); endI++) {
                LocalDateTime end = times.get(endI);
                long duration = Duration.between(start, end).toMinutes();
                if (duration >= 0 && duration < interval) {
                    ranges.add(new WUILObservation(start, end, label));
                }
            }
        }
        for (Iterator<WUILObservation> iter = ranges.iterator(); iter.hasNext();) {
            WUILObservation range1 = iter.next();
            for (WUILObservation range2: ranges) {
                if (!range1.equals(range2) && range2.contains(range1)) {
                    iter.remove();
                    break;
                }
            }
        }
        return ranges;
    }

    public int size () {
        return observations.size();
    }

    public int activitySize() {
        int size = 0;
        for (WUILObservation observation : observations) {
            size += observation.size();
        }
        return size;
    }

    public void trim(int size) {
        Collections.shuffle(observations);
        observations = observations.subList(0, Math.min(observations.size(), size));
    }

    public void trimActivity(int size) {
        int currentSize = this.activitySize();


        while (1.10 * size < currentSize) {
            currentSize -= this.removeLast();
        }
    }

    private int removeLast() {
        int size = 0;
        if (this.observations.size() > 0) {
            size = observations.get(observations.size()-1).size();
            observations = observations.subList(0, observations.size()-1);
        }
        return size;
    }

    public static class WUILDatum {
        private static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        private int uId;
        private int bId;
        private int depth;
        private String file;
        private LocalDateTime date;

        public WUILDatum (String line) {
            String[] data = line.split("\\|");
            if (data.length == 6) {
                try {
                    this.uId = Integer.parseInt(data[0]);
                    this.bId = Integer.parseInt(data[3]);
                    this.depth = Integer.parseInt(data[4]);
                    this.file = data[5];
                    String date = data[1] + " " + data[2].substring(0, data[2].length() - 5);
                    this.date = LocalDateTime.parse(date, dateFormat);
                    this.date = this.date.truncatedTo(ChronoUnit.MINUTES);
                } catch (Exception e) {
                    e.printStackTrace();
                    this.uId = -1;
                }
            }
        }

        public boolean isValid () {
            return uId >= 0;
        }

        @Override
        public String toString() {
            return file;
        }

        public String getKey() {
            return file;
        }
    }

    public static class WUILObservation {
        private LocalDateTime start;
        private LocalDateTime end;
        private String label;
        private List<WUILActivity> activity = new ArrayList<>();
        private static int maxVisitsPerActivity = 100;
        private static int minVisitsPerActivity = 2;

        public WUILObservation(LocalDateTime start, LocalDateTime end, String label) {
            this.start = start;
            this.end = end;
            this.label = label;
            activity.add(new WUILActivity());
        }

        public boolean contains(WUILObservation range) {
            return !(range.start.isBefore(start) || range.end.isAfter(end));
        }

        public boolean contains(WUILDatum datum) {
            return this.contains(datum.date);
        }

        public boolean contains(LocalDateTime time) {
            return !(time.isBefore(start) || time.isAfter(end));
        }

        public void addData(List<WUILDatum> data) {
            for (WUILDatum datum : data) {
                if (this.contains(datum)) {
                    if (this.getActiveActivity().getSize() >= maxVisitsPerActivity) {
                        this.activity.add(new WUILActivity());
                    }
                    this.getActiveActivity().addData(datum);
                }
            }
        }

        public WUILActivity getActiveActivity() {
            return activity.get(activity.size()-1);
        }

        public int size() {
            return activity.size();
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof WUILObservation) {
                WUILObservation range = (WUILObservation) o;
                return (range.start == this.start && range.end == this.end);
            }
            return false;
        }

        public void graph() {
            for (Iterator<WUILActivity> iter = activity.iterator(); iter.hasNext();) {
                WUILActivity act = iter.next();
                act.graph();
                if (act.getSize() < minVisitsPerActivity) {
                    iter.remove();
                }
            }
        }

        public String toInstance() {
            String instance = "";

            for (WUILActivity activity : this.activity) {
                instance += String.format("%s,%s\n", label, activity.toInstance());
            }
            instance = StringUtils.chomp(instance);
            return instance;
        }

        public static class WUILActivity {
            private List<WUILDatum> data = new ArrayList<>();
            private WUILGraph graph = new WUILGraphAccess("file access");

            public void addData(WUILDatum datum) {
                this.data.add(datum);
            }

            public int getSize() {
                return data.size();
            }

            public void graph() {
                for (WUILDatum datum : data) {
                    graph.traverse(datum.getKey());
                }
            }

            public String toInstance() {
                return String.format("%d,%.2f,%.2f,%.2f", graph.totalVertices(), (double)graph.totalVertices()/graph.totalEdges(), (double)graph.totalVertices()/graph.undirectedEdges(), (double)graph.totalVertices()/graph.directedEdges());
            }
        }
    }


}
