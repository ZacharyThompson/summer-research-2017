package com.research.wuil.classification.dev.attempt_1;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.util.ArrayList;
import java.util.List;

public class Test {
    private static int numLabelClasses = 2;
    private static int nEpochs = 500;
    private static int segmentSize = 100;
    private static int lstmLayerSize = 10;

    public static void main (String args[]) throws Exception {
        //DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
        CudaEnvironment.getInstance().getConfiguration()
                .setMaximumDeviceCacheableLength(1024 * 1024 * 1024L)
                .setMaximumDeviceCache(2L * 1024 * 1024 * 1024L)
                .setMaximumHostCacheableLength(1024 * 1024 * 1024L)
                .setMaximumHostCache(2L * 1024 * 1024 * 1024L);

        System.out.println("----- Beginning -----");

        String path = "E:/Research/WUIL Logs/data/";
        WUILDataSetIterator data = new WUILDataSetIterator(path, 1000, segmentSize);

        for (int i = 2; i <= 11; i++) {
            System.out.println("Loading User : User" + i);
            data.LoadUser("User" + i);
        }
        //WUILDataSetIterator data = FileReadTest.getData();

        data.shuffle();
        data.EvenlyDistribute();
        int size = (int) (data.numExamples() * 0.8);

        List<String> labels = new ArrayList<>();
        labels.add("positive");
        labels.add("negative");

        WUILDataSetIterator train = data.ExtractIterator(size, labels, true);
        data.shuffle();
        train.setName("Train Set");

        // Create Test Sets
        List<WUILDataSetIterator> testSets = new ArrayList<>();
        for (String label : labels) {
            List<String> lbl = new ArrayList<>();
            lbl.add(label);
            WUILDataSetIterator test = data.ExtractIterator(data.numExamples(), lbl, false);
            test.setName(String.format("Test Set (%s)", label));
            testSets.add(test);
        }

        // Print Statistics
        train.printStatistics();
        for (WUILDataSetIterator testSet : testSets) {
            testSet.printStatistics();
        }

        // Normalize Data
        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(train);
        train.reset();
        train.setPreProcessor(normalizer);
        for (WUILDataSetIterator testSet : testSets) {
           testSet.setPreProcessor(normalizer);
        }

        RNNModel(train, testSets, true);
    }

    public static void RNNModel(WUILDataSetIterator trainData, List<WUILDataSetIterator> testData, boolean visualize) throws Exception {
        trainData.setMode(WUILDataSetIterator.Mode.RECURRENT);
        trainData.setLearnType(WUILDataSetIterator.LearnType.SUPERVISED);
        for (WUILDataSetIterator testSet : testData) {
            testSet.setMode(WUILDataSetIterator.Mode.RECURRENT);
            testSet.setLearnType(WUILDataSetIterator.LearnType.SUPERVISED);
        }

        /* ---- Configure Network ---- */
        System.out.println("----- Configuring RNN -----");
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(123)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.NESTEROVS)
                .learningRate(0.001)
                .regularization(true)
                .l2(0.01)
                .list()
                .layer(0, new GravesLSTM.Builder().activation(Activation.TANH)
                        .nIn(1).nOut(lstmLayerSize).build())
                .layer(1, new GravesLSTM.Builder().activation(Activation.TANH)
                        .nIn(lstmLayerSize).nOut(lstmLayerSize).build())
                .layer(2, new GravesLSTM.Builder().activation(Activation.TANH)
                        .nIn(lstmLayerSize).nOut(lstmLayerSize).build())
                .layer(3, new GravesLSTM.Builder().activation(Activation.TANH)
                        .nIn(lstmLayerSize).nOut(lstmLayerSize).build())
                .layer(4, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .activation(Activation.SOFTMAX).nIn(lstmLayerSize).nOut(numLabelClasses).build())
                .pretrain(false)
                .backprop(true)
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();

        net.setListeners(new ScoreIterationListener(5));
        if (visualize) {
            /*UIServer uiServer = UIServer.getInstance();
            StatsStorage statsStorage = new InMemoryStatsStorage();
            uiServer.attach(statsStorage);
            net.setListeners(new StatsListener(statsStorage));
            */
        }

        /* ---- Train Network */
        System.out.println("----- Training RNN -----");
        String str = "Evaluation on (%s) at epoch %d: Accuracy = %.2f, F1 = %.2f";
        for (int i = 0; i < nEpochs; i++) {
            long start = System.currentTimeMillis();
            // Fit Training Data
            net.fit(trainData);
            // Evaluate on the test sets:
            for (WUILDataSetIterator testSet : testData) {
                Evaluation evaluation = net.evaluate(testSet);
                System.out.println(String.format(str, testSet.getName(), i, evaluation.accuracy(), evaluation.f1()));
                testSet.reset();
            }
            // Reset iterators
            trainData.reset();

            System.out.println(String.format("Length of epoch %d: %d", i, System.currentTimeMillis() - start));
        }
        System.out.println("----- RNN Complete -----");
    }

    public static void AutoEncoder (WUILDataSetIterator trainData, WUILDataSetIterator testData, boolean visualize) throws Exception {
        trainData.setMode(WUILDataSetIterator.Mode.RECURRENT);
        testData.setMode(WUILDataSetIterator.Mode.RECURRENT);

        trainData.setLearnType(WUILDataSetIterator.LearnType.UNSUPERVISED);
        testData.setLearnType(WUILDataSetIterator.LearnType.UNSUPERVISED);

        /* ---- Configure Network ---- */
        System.out.println("----- Configuring RNN -----");
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(123)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.NESTEROVS)
                .learningRate(0.01)
                .regularization(true)
                .l2(0.01)
                .list()
                .layer(0, new GravesLSTM.Builder().activation(Activation.TANH)
                        .nIn(1).nOut(lstmLayerSize).build())
                .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .activation(Activation.SOFTMAX).nIn(lstmLayerSize).nOut(numLabelClasses).build())
                .pretrain(false)
                .backprop(true)
                .build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();

        net.setListeners(new ScoreIterationListener(20));
        if (visualize) {
            UIServer uiServer = UIServer.getInstance();
            StatsStorage statsStorage = new InMemoryStatsStorage();
            uiServer.attach(statsStorage);
            net.setListeners(new StatsListener(statsStorage));
        }

        /* ---- Train Network */
        System.out.println("----- Training RNN -----");
        String str = "Test set evaluation at epoch %d: Accuracy = %.2f, F1 = %.2f";
        for (int i = 0; i < nEpochs; i++) {
            // Fit Training Data
            net.fit(trainData);
            // Evaluate on the test set:
            Evaluation evaluation = net.evaluate(testData);
            System.out.println(String.format(str, i, evaluation.accuracy(), evaluation.f1()));
            // Reset iterators
            testData.reset();
            trainData.reset();
        }
        System.out.println("----- RNN Complete -----");
    }
}
