package com.research.wuil.classification.dev.attempt_1;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.List;

public class WUILSequenceDynamic {
    public String label;
    public String user;
    private INDArray features;
    private INDArray labels;
    private INDArray labelsMask;
    private INDArray featuresMask;
    private int sequenceLength;

    public WUILSequenceDynamic(List<Float> sequence, String user, String label, int sequenceLength) {
        this.label = label;
        this.user = user;
        this.sequenceLength = sequenceLength;

        // Create Feature Vector
        float[] floatFeatures = new float[sequence.size()];
        for (int i = 0; i < sequence.size(); i++) {
            floatFeatures[i] = sequence.get(i);
        }
        this.features = Nd4j.create(floatFeatures, new int[]{1, sequence.size()}, 'f');

        // Create Label Vector
        this.labels = Nd4j.zeros(2, sequence.size(), 'f');
        this.labels.putScalar(new int[]{this.labelToInt(), sequenceLength - 1}, 1);

        // Features Mask
        this.featuresMask = Nd4j.zeros(sequenceLength);
        for (int i = 0; i < sequenceLength; i++) {
            featuresMask.putScalar(i, 1);
        }

        // Labels Mask
        this.labelsMask = Nd4j.zeros(sequenceLength);
        this.labelsMask.putScalar(sequenceLength - 1, 1);
    }

    public String getLabel() {
        return label;
    }

    public int labelToInt() {
        return (label.equals("positive")) ? 1 : 0;
    }

    public INDArray getFeatures() {
        return features;
    }

    public INDArray getLabels() {
        return labels;
    }

    public INDArray getFeaturesMask() {
        return featuresMask;
    }

    public INDArray getLabelsMask() {
        return labelsMask;
    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < features.rows(); i++) {
            ret += features.getFloat(i) + " ";
        }
        return ret;
    }
}