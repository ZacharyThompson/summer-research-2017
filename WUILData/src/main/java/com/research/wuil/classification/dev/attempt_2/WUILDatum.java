package com.research.wuil.classification.dev.attempt_2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WUILDatum {
    private static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private int uId;
    private int bId;

    public int getDepth() {
        return depth;
    }

    private int depth;
    private String file;
    private LocalDateTime date;
    private String label;
    public WUILDatum (String line, String label) {
        this.label = label;
        String[] data = line.split("\\|");
        if (data.length == 6) {
            try {
                this.uId = Integer.parseInt(data[0]);
                this.bId = Integer.parseInt(data[3]);
                this.depth = Integer.parseInt(data[4]);
                this.file = data[5];
                String date = data[1] + " " + data[2].substring(0, data[2].length() - 5);
                this.date = LocalDateTime.parse(date, dateFormat);
                //this.date = this.date.truncatedTo(ChronoUnit.MINUTES);
            } catch (Exception e) {
                e.printStackTrace();
                this.uId = -1;
            }
        }
    }

    public boolean isValid () {
        return uId >= 0;
    }

    @Override
    public String toString() {
        return file;
    }

    public String getKey() {
        return file;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getLabel() {
        return label;
    }

    public int getuId() {
        return uId;
    }
}