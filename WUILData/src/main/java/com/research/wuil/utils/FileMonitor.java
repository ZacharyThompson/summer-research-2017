package com.research.wuil.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;

/**
 *
 * A persistent file monitor for monitoring OSQuery logs.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class FileMonitor implements Runnable {
    /**
     * Boolean value deterministic of whether the thread is running.
     */
    private boolean m_Running = false;

    /**
     * The file to be monitored.
     */
    private File m_File;

    /**
     * The outwards PrintWriter to send results.
     */
    private PrintWriter m_Writer;

    /**
     * Constructor.
     *
     * @param file File to be monitored.
     * @param writer The PrintWriter to write log results to.
     */
    public FileMonitor(File file, PrintWriter writer) {
        try {
            this.m_File = file;
            this.m_Writer = writer;
            this.m_Running = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to be executed in a different thread.
     */
    @Override
    public void run() {
        // If the file doesn't exist, try wait for it otherwise return.
        while (!m_File.exists()) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
                m_Running = false;
                return;
            }
        }
        // Open a stream to the file
        try (BufferedReader reader = new BufferedReader(new FileReader(m_File))) {
            // While the method is allowed to run
            while (m_Running) {
                // Build line from output
                StringBuilder output = new StringBuilder("");
                String line;
                while ((line = reader.readLine()) != null) {
                    output.append(line);
                    output.append("<SEPARATOR>");
                }
                if (output.toString().length() > 0) {
                    m_Writer.println("query_results:" + output.toString());
                }
                // Cool down period.
                Thread.sleep(10000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stop the thread.
     */
    public void stop() {
        this.m_Running = false;
    }

    /**
     * Start the thread.
     */
    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }
}
