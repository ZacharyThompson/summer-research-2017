package com.research.wuil.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * JSON loader reader and writer for working with Configuration files.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class JSON {
    /**
     * Read a JSONObject from a file.
     *
     * @param configFile Configuration file to read JSONObject from.
     * @return JSONObject read from file.
     */
    public static JSONObject readConfig(File configFile) {
        JSONParser parser = new JSONParser();
        JSONObject config = null;
        try (FileReader reader = new FileReader(configFile)){
            config = (JSONObject) parser.parse(reader);
        } catch (Exception e) {
            System.err.println(String.format("Unable to read JSONObject from configuration file [%s]",
                    configFile.getPath()));
        }
        return config;
    }

    /**
     * Writer a JSONObject to a file.
     *
     * @param configFile Configuration file to write JSONObject to.
     * @param config JSONObject to writer to file.
     */
    public static void saveConfig(File configFile, JSONObject config) {
        try (FileWriter writer = new FileWriter(configFile)){
            writer.write(config.toJSONString());
            writer.flush();
        } catch (Exception e) {
            System.err.println(String.format("Unable to writer JSONObject to configuration file [%s]",
                    configFile.getPath()));
        }
    }
}
