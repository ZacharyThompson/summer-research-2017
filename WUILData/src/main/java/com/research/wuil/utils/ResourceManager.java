package com.research.wuil.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Objects;

/**
 *
 * A resource management tool that provides file management for both the server and client.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class ResourceManager {
    /**
     * The prefix to append to any temporary folders.
     */
    private static final String TEMP_PREFIX = "temp";

    /**
     * The root file for each resource manager instance.
     */
    private final File m_Root;

    /**
     * Constructor.
     *
     * @param name The name of the resource manager. Used to find the root.
     */
    private ResourceManager(String name) {
        this.m_Root = new File("../resources/", name);
    }

    /**
     * Static method to get a resource manager instance for the server.
     *
     * @return Server resource manager.
     */
    public static ResourceManager getServer() {
        return new ResourceManager("server");
    }

    /**
     * Static method to get a resource manager instance for the client.
     *
     * @return Client resource manager.
     */
    public static ResourceManager getClient() {
        return new ResourceManager("client");
    }

    /**
     * Get the root of the resource manager instance.
     *
     * @return Root file.
     */
    public File getRoot() {
        return m_Root;
    }

    /**
     * Determines whether a specific file exists in current root.
     *
     * @param path The path of a file to check for existence.
     * @return Whether the file exists.
     */
    public boolean hasFile(String path) {
        return new File(m_Root, path).exists();
    }

    /**
     * Get a file from a path using the root file as the parent directory.
     *
     * @param path The path of the file.
     * @return The file instance.
     */
    public File getFile(String path) {
        return new File(m_Root, path);
    }

    /**
     * Create a temporary folder using the temp prefix and current time.
     *
     * @param name The name to inject into the temporary folders name.
     * @return The new temporary folder
     */
    public File createTempFolder(String name) {
        File dir = new File(m_Root, String.format("%s-%s-%s", TEMP_PREFIX , name, System.currentTimeMillis() + ""));
        dir.mkdir();
        return dir;
    }

    /**
     * Iterates through the temporary folders created and deletes them.
     */
    public void deleteTempFolders() {
        if (m_Root.listFiles() != null) {
            for (File file : Objects.requireNonNull(m_Root.listFiles())) {
                if (file.isDirectory() && file.getName().startsWith("temp-")) {
                    File db = new File(file, "osquery.db");
                    try {
                        if (db.exists()) {
                            //Files.setPosixFilePermissions(db.toPath(), PosixFilePermissions.fromString("rwxrwx---"));
                            db.setWritable(true);
                            db.setReadable(true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        FileUtils.cleanDirectory(file);
                        file.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
