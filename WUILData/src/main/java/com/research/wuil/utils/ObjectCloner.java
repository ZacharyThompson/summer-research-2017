package com.research.wuil.utils;

import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.meta.OneClassClassifier;
import weka.filters.StringToFileEvents;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;

public class ObjectCloner {
    public ObjectCloner(){ }

    public static Object deepCopy(Object o) {
        ObjectOutputStream os;
        ObjectInputStream is;

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            os.writeObject(o);
            os.flush();

            is = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));

            return is.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }

    public static String serialize(Object o) {
        ObjectOutputStream os;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            os.writeObject(o);
            os.flush();
            os.close();
            return Base64.getEncoder().encodeToString(bos.toByteArray());
        } catch (Exception e) {
            return "";
        }
    }

    public static Object deserialize(String s) {
        ObjectInputStream is;
        try {
            byte[] data = Base64.getDecoder().decode(s);
            is = new ObjectInputStream(new ByteArrayInputStream(data ));
            Object o  = is.readObject();
            is.close();
            return o;
        } catch (Exception e) {
            return "";
        }
    }

    public static void main (String args[]) {
        FilteredClassifier classifier = new FilteredClassifier();
        classifier.setClassifier(new OneClassClassifier());
        classifier.setFilter(new StringToFileEvents());

        System.out.println(serialize(classifier));
        System.out.println(deserialize(serialize(classifier)));
    }
}
