package com.research.wuil.utils;

import weka.core.OptionHandler;
import weka.core.Utils;

/**
 *
 * Used to serialize and deserialize an OptionHandler object.
 * Taken from weka.gui.PropertyPanel.java
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class OptionSerializer {
    private static final boolean useOptions = false;

    /**
     * If the Object is an instance of OptionHandler, convert it to a String that can
     * be used to re-instantiate the Object with the same options.
     *
     * @param o The object to serialize.
     * @return The String result of serialization.
     */
    public static String serialize(Object o) {
        if(!useOptions) return ObjectCloner.serialize(o);

        String str = o.getClass().getName();
        if (o instanceof OptionHandler) {
            str += " " + Utils.joinOptions(((OptionHandler) o).getOptions());
        }
        return str.trim();
    }

    /**
     * If the String is a serialize form of an Object that extends OptionHandler, convert
     * it back to the Original object.
     *
     * @param str Serialized form of the Object.
     * @return Deserialized Object.
     */
    public static Object deserialize(String str) {
        if (!useOptions) return ObjectCloner.deserialize(str);

        if (str != null) {
            try {
                String[] options = Utils.splitOptions(str);
                String classname = options[0];
                options[0] = "";
                return Utils.forName(Object.class, classname, options);
            } catch (Exception ex) {
                System.err.println("Error parsing");
            }
        }
        return null;
    }
}
