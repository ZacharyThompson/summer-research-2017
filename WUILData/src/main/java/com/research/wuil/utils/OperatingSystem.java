package com.research.wuil.utils;

/**
 *
 * Operating System class used to help the user identify what type of Operating System
 * the program is running on.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class OperatingSystem {
    /**
     * The Operating System.
     */
    private static String OS = System.getProperty("os.name").toLowerCase();

    /**
     * Checks whether the operating system is Unix.
     *
     * @return Whether the operating system is Unix.
     */
    public static boolean isUnix() {
        return OS.contains("nix") || OS.contains("nux") || OS.contains("aix");
    }

    /**
     * Checks whether the operating system is Windows.
     *
     * @return Whether the operating system is Windows.
     */
    public static boolean isWindows() {
        return OS.contains("win");
    }

    /**
     * Checks whether the operating system is Mac.
     *
     * @return Whether the operating system is Mac.
     */
    public static boolean isMac() {
        return OS.contains("mac");
    }

    /**
     * Checks whether the operating system is Solaris.
     *
     * @return Whether the operating system is Solaris.
     */
    public static boolean isSolaris() {
        return OS.contains("sunos");
    }
}
