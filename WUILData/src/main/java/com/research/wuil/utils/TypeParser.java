package com.research.wuil.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;
import java.util.HashMap;
import java.util.function.Function;

/**
 *
 * Relatively dirty approach at type parsing a String to an Object of a specified Class.
 * Based upon: https://stackoverflow.com/questions/36368235/java-get-valueof-for-generic-subclass-of-java-lang-number-or-primitive
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class TypeParser {
    /**
     * Maps a Class to a function that is used to convert a String to that Class.
     */
    private static HashMap<Class<?>, Function<String,?>> PARSER = new HashMap<>();
    static {
        PARSER.put(boolean.class       , Boolean::parseBoolean);
        PARSER.put(byte.class          , Byte::parseByte);
        PARSER.put(short.class         , Short::parseShort);
        PARSER.put(int.class           , Integer::parseInt);
        PARSER.put(long.class          , Long::parseLong);
        PARSER.put(double.class        , Double::parseDouble);
        PARSER.put(float.class         , Float::parseFloat);
        PARSER.put(Boolean.class       , Boolean::valueOf);
        PARSER.put(Byte.class          , Byte::valueOf);
        PARSER.put(Short.class         , Short::valueOf);
        PARSER.put(Integer.class       , Integer::valueOf);
        PARSER.put(Long.class          , Long::valueOf);
        PARSER.put(Double.class        , Double::valueOf);
        PARSER.put(Float.class         , Float::valueOf);
        PARSER.put(String.class        , String::valueOf);
        PARSER.put(BigDecimal.class    , BigDecimal::new);
        PARSER.put(BigInteger.class    , BigInteger::new);
        PARSER.put(LocalDate.class     , LocalDate::parse);
        PARSER.put(LocalDateTime.class , LocalDateTime::parse);
        PARSER.put(LocalTime.class     , LocalTime::parse);
        PARSER.put(MonthDay.class      , MonthDay::parse);
        PARSER.put(OffsetDateTime.class, OffsetDateTime::parse);
        PARSER.put(OffsetTime.class    , OffsetTime::parse);
        PARSER.put(Year.class          , Year::parse);
        PARSER.put(YearMonth.class     , YearMonth::parse);
        PARSER.put(ZonedDateTime.class , ZonedDateTime::parse);
        PARSER.put(ZoneId.class        , ZoneId::of);
        PARSER.put(ZoneOffset.class    , ZoneOffset::of);
    }

    /**
     * Parses a String to an Object of a specified Class; if this
     * cannot be achieved, return null.
     *
     * @param argString The String to parse
     * @param param The Class to attempt to parse the String to.
     * @return The parse Object.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Object parse(String argString, Class param) {
        Function<String,?> func = PARSER.get(param);
        if (func != null) return func.apply(argString);
        if (param.isEnum()) return Enum.valueOf(param, argString);
        return null;
    }

}
