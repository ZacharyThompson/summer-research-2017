package com.research.wuil.utils;

import com.research.wuil.gui.Client;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;

/**
 *
 *  Osquery runnable Object. This class is used to host the process of Osquery
 *  and handle the delegation of the Osquery related tasks of the Application.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class Osquery implements Runnable {
    /**
     * The parameters used to start the OSQuery process.
     */
    private String m_Params[];

    /**
     * Whether the process and thread are running.
     */
    private boolean m_Running = false;

    /**
     * The client associated with this OSQuery process.
     */
    private Client m_Client;

    /**
     * A File monitor used to monitor the log file.
     */
    private FileMonitor m_Monitor;

    /**
     * The log file to be monitored.
     */
    private File m_Log;

    /**
     * Constructor.
     *
     * @param client The client associated with this OSQuery process.
     * @param monitor The file to monitor with OSQuery.
     */
    @SuppressWarnings("unchecked")
    public Osquery(Client client, File monitor, int interval) {
        if (!OperatingSystem.isUnix()) {
            m_Running = true;
            return;
        }
        if (!monitor.exists()) {
            m_Running = true;
            return;
        }

        this.m_Client = client;
        ResourceManager resources = ResourceManager.getClient();

        resources.deleteTempFolders();
        File temp = resources.createTempFolder("osquery");
        m_Log = new File(temp, "logs");
        File db = new File(temp, "osqueryd.db");
        if (!m_Log.exists()) m_Log.mkdir();
        if (!db.exists()) db.mkdir();

        File osqueryd = resources.getFile("osqueryd");
        try {
            osqueryd = Files.copy(osqueryd.toPath(), new File(temp, "/osqueryd").toPath()).toFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        File configFile = ResourceManager.getClient().getFile("osquery-base.conf");
        JSONObject config = JSON.readConfig(configFile);
        JSONObject file_paths = (JSONObject) config.get("file_paths");
        JSONArray files = new JSONArray();
        files.add(monitor.getAbsolutePath() + "/%%");
        file_paths.put("files", files);

        JSONObject schedule = (JSONObject) config.get("schedule");
        JSONObject file_events = (JSONObject) schedule.get("file_events");
        file_events.put("interval", interval);

        configFile = new File(temp, "osquery.conf");
        JSON.saveConfig(configFile, config);

        m_Params = new String[]{
                osqueryd.getAbsolutePath(),
                String.format("--database_path=%s", db.getAbsolutePath()),
                String.format("--pidfile=%s/osqueryd.pidfile", temp.getAbsolutePath()),
                String.format("--config_path=%s", configFile.getAbsolutePath()),
                String.format("--logger_path=%s/", m_Log.getAbsolutePath()),
                "--verbose=true",
                "--allow-unsafe=true"
        };
    }

    /**
     * Runs the OSQuery daemon process in a new thread.
     */
    @Override
    public void run() {
        m_Running = true;
        try {
            ProcessBuilder pb = new ProcessBuilder(m_Params);
            pb.redirectErrorStream(true);
            Process process = pb.start();
            m_Monitor = new FileMonitor(new File(m_Log, "/osqueryd.results.log"), m_Client.getOutputStream());
            m_Monitor.start();
            try (BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                for (String line; (line = input.readLine()) != null && m_Running;) {
                    m_Client.log("Osquery", line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            m_Client.log("Osquery", "Osquery exception " + e);
        }
        m_Client.log("Osquery", "Osquery process stopped");
        m_Running = false;
        m_Monitor.stop();
    }

    /**
     * Stop the OSQuery process and thread.
     */
    public void stop() {
        this.m_Running = false;
    }

    /**
     * Start a new thread with the OSQuery process.
     */
    public void start() {
        if (!m_Running) {
            Thread thread = new Thread(this);
            thread.start();
        }
    }
}
