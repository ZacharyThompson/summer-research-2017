package com.research.wuil.utils;

import java.io.File;
import java.util.Random;
/**
 *
 * Original monitored environment for testing functionality, not classification performance.
 *
 * @author Zachary Thompson (zaachary3@hotmail.com)
 */
public class MonitoredEnvironment {
    public static void main (String args[]) {
        File root = new File("E:\\Research\\shared\\Monitored Environment");
        //CreateEnvironment(root, 10);
    }

    static int maxFiles = 10;
    static int maxSubDir = 5;
    static Random rand = new Random();

    public static void CreateEnvironment(File root, int max) {
        int files = rand.nextInt(maxFiles);
        int subdir = rand.nextInt(maxSubDir);

        for (int i = 0; i < files; i++) {
            File file = new File(root, "file" + i + ".txt");
            try {
                file.createNewFile();
            } catch (Exception e) {
                continue;
            }
        }


        max --;
        for (int i = 0; i < subdir; i++) {
            File file = new File(root, "dir" + i);
            try {
                file.mkdir();
                if (max > 0) {
                    CreateEnvironment(file, max);
                }
            } catch (Exception e) {
                continue;
            }
        }
    }


}
