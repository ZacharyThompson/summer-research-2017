package com.research.linear_algebra;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class BooleanOperations {

    public static RealMatrix union (RealMatrix m1, RealMatrix m2) {
        if (!dimensionsMatch(m1, m2)) {
            return null;
        }
        RealMatrix matrix = MatrixUtils.createRealMatrix(m1.getRowDimension(), m1.getColumnDimension());
        for (int column = 0; column < m1.getColumnDimension(); column++) {
            for (int row = 0; row < m1.getRowDimension(); row++) {
                matrix.setEntry(row, column, m1.getEntry(row, column) > 0 || m2.getEntry(row, column) > 0 ? 1 : 0);
            }
        }
        return matrix;
    }

    public static RealMatrix intersection (RealMatrix m1, RealMatrix m2) {
        if (!dimensionsMatch(m1, m2)) {
            return null;
        }
        RealMatrix matrix = MatrixUtils.createRealMatrix(m1.getRowDimension(), m1.getColumnDimension());
        for (int column = 0; column < m1.getColumnDimension(); column++) {
            for (int row = 0; row < m1.getRowDimension(); row++) {
                matrix.setEntry(row, column, m1.getEntry(row, column) * m2.getEntry(row, column));
            }
        }
        return matrix;
    }

    public static RealMatrix transpose (RealMatrix m1) {
        return m1.transpose();
    }

    public static RealMatrix fullConnections (RealMatrix m1) {
        RealMatrix matrix = MatrixUtils.createRealMatrix(m1.getRowDimension(), m1.getColumnDimension());

        for (int column = 0; column < matrix.getColumnDimension(); column++) {
            for (int row = 0; row < matrix.getRowDimension(); row++) {
                double val = m1.getEntry(row, column) * m1.getEntry(column, row);
                matrix.setEntry(row, column, val);
                matrix.setEntry(column, row, val);
            }
        }
        return matrix;
    }

    public static int islandCount (RealMatrix matrix) {
        System.out.println(matrix.getColumnDimension() + " " + matrix.getRowDimension());
        RealMatrix visited = MatrixUtils.createRealMatrix(matrix.getRowDimension(), matrix.getColumnDimension());
        int count = 0;
        for (int column = 0; column < matrix.getColumnDimension(); column++) {
            for (int row = 0; row < matrix.getRowDimension(); row++) {
                if (visited.getEntry(row, column) == 0 && matrix.getEntry(row, column) == 1) {
                    DFS(matrix, row, column, visited);
                    count++;
                }
            }
        }
        return count;
    }

    private static void DFS (RealMatrix matrix, int row, int column, RealMatrix visited) {
        column = (column + matrix.getColumnDimension()) % matrix.getColumnDimension();
        row = (row + matrix.getRowDimension()) % matrix.getRowDimension();

        if (matrix.getEntry(row, column) == 0 || visited.getEntry(row, column) == 1) {
            visited.setEntry(row, column, 1);
            return;
        } else {
            visited.setEntry(row, column, 1);
        }
        DFS(matrix, row, column+1, visited);
        DFS(matrix, row, column-1, visited);
        DFS(matrix, row+1, column, visited);
        DFS(matrix, row-1, column, visited);
    }

    public static RealMatrix toBinary (RealMatrix m1) {
        RealMatrix matrix = MatrixUtils.createRealMatrix(m1.getRowDimension(), m1.getColumnDimension());
        for (int column = 0; column < matrix.getColumnDimension(); column++) {
            for (int row = 0; row < matrix.getRowDimension(); row++) {
                matrix.setEntry(row, column, m1.getEntry(row, column) == 0 ? 0 : 1);
            }
        }
        return matrix;
    }

    public static boolean isBinary (RealMatrix m1) {
        for (int column = 0; column < m1.getColumnDimension(); column++) {
            for (int row = 0; row < m1.getRowDimension(); row++) {
                if (m1.getEntry(row, column) != 0 && m1.getEntry(row, column) != 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean dimensionsMatch (RealMatrix m1, RealMatrix m2) {
        return (m1.getRowDimension() == m2.getRowDimension() && m1.getColumnDimension() == m2.getColumnDimension());
    }

    public static RealMatrix transitive (RealMatrix matrix) {
        if (matrix.getColumnDimension() != matrix.getRowDimension()) {
            return null;
        }
        RealMatrix transitive = matrix.copy();
        for (int k = 0; k < transitive.getRowDimension(); k++) {
            RealMatrix union = MatrixUtils.createRealMatrix(transitive.getRowDimension(), transitive.getColumnDimension());
            for (int column = 0; column < transitive.getColumnDimension(); column++) {
                RealMatrix col = transitive.getColumnMatrix(k).scalarMultiply(transitive.getEntry(k, column));
                union.setColumnMatrix(column, col);
            }
            transitive = union(transitive, union);
        }
        return transitive;
    }


}
