package com.research;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Random;

public class FileAccessSimulation implements Runnable {
    private static int DELAY = 30000;

    private File m_Working;
    private int m_MaxEvents;
    private int m_MinEvents;
    private int m_MaxDepth;
    private boolean m_Running;
    private double m_ChangeDir;
    private double m_MoveDown;
    private Random random = new Random();

    private HashMap<File, File[]> dirs = new HashMap<>();
    private HashMap<File, File[]> files = new HashMap<>();

    public FileAccessSimulation(File working, int maxEvents, int minEvents, int maxDepth, double changeDir, double moveDown) {
        m_Working = working;
        m_MaxEvents = maxEvents;
        m_MinEvents = minEvents;
        m_MaxDepth = maxDepth;
        m_ChangeDir = changeDir;
        m_MoveDown = moveDown;
    }

    private static void accessFile(File file) {
        System.out.println("accessing " + file);
        try (InputStream is = new FileInputStream(file)) { } catch (Exception e) { }
    }

    @Override
    public void run() {
        m_Running = true;
        File current = m_Working;
        long now = System.currentTimeMillis();
        long end = now + (60 * 60 * 1000);
        while (System.currentTimeMillis() < end && m_Running) {
            try {
                int delay = DELAY / (random.nextInt(m_MaxEvents - m_MinEvents) + m_MinEvents);
                accessFile(current = move(current));
                Thread.sleep(delay);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Simulation Finished");
    }

    private File move(File file) {
        File next = null;
        if (file.isFile())
            file = file.getParentFile();

        if (random.nextDouble() < m_ChangeDir)
            next = nextDir(file);

        if (next == null)
            next = nextFile(file);

        return (next == null) ? file : next;
    }

    private File nextDir(File file) {
        if ((random.nextDouble() > m_MoveDown && !file.equals(m_Working)) ||
                distanceBetweenFile(m_Working, file) >= m_MaxDepth)
            return file.getParentFile();

        File[] dirs = this.dirs.get(file);
        if (dirs == null)
            dirs = file.listFiles(File::isDirectory);
            this.dirs.put(file, dirs);

        if (dirs == null || dirs.length == 0)
            return null;

        return dirs[random.nextInt(dirs.length)];
    }

    private File nextFile(File file) {
        File[] files = this.files.get(file);
        if (files == null)
            files = file.listFiles(File::isFile);
            this.files.put(file, files);

        if (files == null || files.length == 0)
            return null;

        return files[random.nextInt(files.length)];
    }

    private static int distanceBetweenFile(File fileA, File fileB) {
        return Math.abs(fileA.toPath().getNameCount() - fileB.toPath().getNameCount());
    }

    public static void main (String args[]) {
        File dir = new File("/home/zac/Documents/Monitored Environment/dir0/dir0");
        FileAccessSimulation sim = new FileAccessSimulation(dir, 40, 20, 3, 0.2, 0.5);
        Thread thread = new Thread(sim);
        thread.start();
    }
}
