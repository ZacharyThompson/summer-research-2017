import java.io.*;
import java.net.Socket;

public class CompromiseDetectionClient {
    public static void main (String args[]) throws Exception {
        String address;
        int port;
        File osquery;
        File database_path;
        if (args.length != 4) {
            address = "192.168.204.1";
            port = 8080;
            osquery = new File("E:/Research/shared/osquery");
            database_path = new File("E:/Research/shared/osquery/osquery.db");
        } else {
            address = args[0];
            port = Integer.parseInt(args[1]);
            osquery = new File(args[2]);
            database_path = new File(args[3]);
        }

        /* Connect to com.Research.WUIL.UI.Server.Server */
        Client client = new Client(address, port);
        Thread clientThread = new Thread(client);
        clientThread.start();

        /* Start Osqueryd process */

        // Clean logging directory
        File logging_path = new File(osquery, "/log/");
        cleanDirectory(logging_path);

        // Establish process parameters
        String params[] = {
                "osqueryd",
                String.format("--pidfile=%s/osqueryd.pidfile", osquery.getPath()),
                String.format("--database_path=%s/osqueryd.db", database_path.getPath()),
                String.format("--logger_path=%s/", logging_path.getPath()),
                String.format("--config_path=%s/osquery.conf", osquery.getPath()),
                String.format("--verbose=true"),
                String.format("--allow-unsafe")
        };

        //Start process
        OSQuery query = new OSQuery(params);

        /* Monitor logging file and provide output stream */
        FileMonitor monitor = new FileMonitor(new File(osquery, "/log/osqueryd.results.log"), client);
        Thread monitorThread = new Thread(monitor);
        monitorThread.start();

        /* Join Client Thread */
        clientThread.join();
        query.stop();
        monitor.stop();
    }

    public static class OSQuery {
        Process process;
        Thread thread;
        boolean running = true;

        public OSQuery(String[] params) {
            ProcessBuilder pb = new ProcessBuilder(params);
            pb.redirectErrorStream(true);
            try {
                process = pb.start();
                Runnable task = () -> {
                    try {
                        String line;
                        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        while ((line = input.readLine()) != null && running) {
                            System.out.println(line);
                        }
                        input.close();
                    } catch (IOException e) {
                        System.out.println(" process not read " + e);
                    }
                };
                thread = new Thread(task);
                thread.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void stop() {
            process.destroyForcibly();
            running = false;
        }
    }

    public static class Client implements Runnable {
        Socket client;
        PrintWriter out;
        BufferedReader in;
        boolean running = true;
        String address;
        int port;

        public Client(String address, int port) throws Exception {
            this.address = address;
            this.port = port;

            int attempts = 0;
            while (!this.connect()) {
                Thread.sleep(1000);
                attempts++;
                if (attempts == 5) {
                    System.out.println("Could not establish connection with server");
                    System.exit(-1);
                }
            }
        }

        public boolean connect() {
            System.out.print(String.format("Attempting connection on %s:%d ... ", address, port));
            try {
                client = new Socket(address, port);
                out = new PrintWriter(client.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (Exception e) {
                System.out.println(String.format("Connection Failed"));
                return false;
            }
            System.out.println("");
            return true;
        }

        @Override
        public void run() {
            try {
                String line;
                while ((line = in.readLine()) != null && running) {
                    if (line.equals("exit")) {
                        return;
                    }
                    System.out.println(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void stop() {
            running = false;
        }

        public void write(char c) {
            out.write(c);
        }
    }

    public static class FileMonitor implements Runnable {
        boolean running = true;
        File file;
        Client client;

        public FileMonitor(File file, Client client) {
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.file = file;
            this.client = client;
        }

        @Override
        public void run() {
            try {
                BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
                while (true && running) {
                    if (reader.available() > 0) {
                        client.write((char) reader.read());
                    } else {
                        Thread.sleep(500);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void stop() {
            running = false;
        }
    }


    public static void cleanDirectory(File dir) {
        if (dir != null && dir.listFiles() != null) {
            for (File file : dir.listFiles()) {
                file.delete();
            }
        }
    }

    public static void deleteOld(File dir) {
        for (File file : dir.listFiles()) {
            if (file.getName().contains("osqueryd.db")) {
                System.out.println(file.getName());
                cleanDirectory(file);
                System.out.println(file.delete());
            }
        }
    }
}
