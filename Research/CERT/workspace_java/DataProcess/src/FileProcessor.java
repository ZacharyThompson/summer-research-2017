import java.io.File;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileProcessor extends Processor {
	private static List<String> activity = new ArrayList<String>();	
	String attr[];
	FileEvent file;
	SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy H:mm");
	
	public FileProcessor(File fInput, File fOutput) {
		super(fInput, fOutput);
		
		activity.add("'File Write'");
		activity.add("'File Open'");
		activity.add("'File Copy'");
		activity.add("'File Delete'");
	}

	@Override
	public String readLine() {
		String ret = super.readLine();
		attr = ret.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
		for (int i = 0; i < attr.length; i++) {
			attr[i] = attr[i].replace("\"", "'");
		}
		file = new FileEvent(getDate(), attr[4], attr[5], attr[6].equals("True"), attr[7].equals("True"), attr[8]);
		return ret;	
	}	
	
	@Override
	public String getHeader() {
		String nominal_users = String.join(",", users);
		String nominal_activity = String.join(",", activity);
		
		header.add("@RELATION file_data");
		header.add(String.format("@ATTRIBUTE user {%s}", nominal_users));
		header.add("@ATTRIBUTE pc string");
		header.add("@ATTRIBUTE file string");
		header.add(String.format("@ATTRIBUTE activity {%s}", nominal_activity));
		header.add("@ATTRIBUTE to_removable_media {True, False}");
		header.add("@ATTRIBUTE from_removable_media {True, False}");
		header.add("@ATTRIBUTE content string");
		header.add("@DATA");		
		
		String ret = "";
		for (String line : header) {
			ret += line + System.lineSeparator();
		}		
		return ret;
	}
	
	@Override
	public String toInstance(String line) {
		try {
			if (users.contains(attr[2])) {
				return MessageFormat.format("{0},\"{1}\",\"{2}\",\"{3}\",{4},{5},\"{6}\"", attr[2], attr[3], attr[4], attr[5], attr[6], attr[7], attr[8]);
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public User getUser() {
		return new User(attr[2]);
	}
	
	public Date getDate() {
		try {
			return parser.parse(attr[1]);
		} catch (ParseException e) {
		
		}
		return null;
	}	
	
	public FileEvent getEvent() {
		return file;
	}
	
}
