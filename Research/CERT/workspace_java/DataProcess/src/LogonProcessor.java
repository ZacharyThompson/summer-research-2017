import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogonProcessor extends Processor {
	SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy H:mm");
	public LogonProcessor(File fInput, File fOutput) {
		super(fInput, fOutput);
	}
	
	@Override
	public User getUser() {
		String[] attr = currentLine.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
		return new User(attr[2]);
	}
	
	public Date getDate() {
		String date = currentLine.split(",")[1];
		try {
			return parser.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public String getComputer() {
		String pc = currentLine.split(",")[3];
		return pc;
	}
	
	public String getActivity() {
		String activity = currentLine.split(",")[4];
		return activity;
	}
	
}
