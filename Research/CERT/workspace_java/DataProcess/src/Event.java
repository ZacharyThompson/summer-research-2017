import java.util.Date;

public interface Event {
	public Date getDate();
	public void setDate(Date date);
	
	public String toInstance();
}
