import java.io.File;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HttpProcessor extends Processor {
	

	private static List<String> computers = new ArrayList<String>();
	private static List<String> activity = new ArrayList<String>();
	String[] attr;
	HttpEvent http;
	SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy H:mm");
	
	public HttpProcessor(File fInput, File fOutput) {
		super(fInput, fOutput);
		
		activity.add("'WWW Visit'");
		activity.add("'WWW Download'");
		activity.add("'WWW Upload'");
	}
	
	@Override
	public String getHeader() {
		String nominal_users = String.join(",", users);
		String nominal_computers = String.join(",", computers);
		String nominal_activity = String.join(",", activity);
				
		header.add("@RELATION http_data");
		header.add("@ATTRIBUTE url string");
		header.add(String.format("@ATTRIBUTE activity {%s}", nominal_activity));
		header.add("@ATTRIBUTE content string");
		header.add(String.format("@ATTRIBUTE pc string", nominal_computers));
		header.add(String.format("@ATTRIBUTE user {%s}", nominal_users));
		header.add("@DATA");		
		
		String ret = "";
		for (String line : header) {
			ret += line + System.lineSeparator();
		}		
		return ret;
	}
	
	@Override
	public String readLine() {
		String ret = super.readLine();
		attr = ret.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
		for (int i = 0; i < attr.length; i++) {
			attr[i] = attr[i].replace("\"", "'");
		}
		try {
			http = new HttpEvent(getDate(), getUrl(), getActivity(), getContent());
		} catch (Exception e) {}
		return ret;
	}
	
	@Override
	public String[] splitLine(String line) {
		return (new String[] { attr[4], attr[5], attr[6], attr[3], attr[2]});
	}
	
	@Override
	public String toInstance(String line) {
		String url = attr[4];
		url = url.replaceAll("http://", "");

		url = url.split("/")[0];
			
		if (users.contains(attr[2])) {
			computers.add(attr[3]);
			return MessageFormat.format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",{4}", url, attr[5], attr[6], attr[3], attr[2]);
		} else {
			return "";
		}
	}
	
	
	@Override
	public User getUser() {
		return new User(attr[2]);
	}
	
	public String getComputer() {
		return attr[3];		
	}
	
	public String getUrl() {
		return attr[4];
	}
	
	public String getActivity() {
		return attr[5];
	}
	
	public String getContent() {
		return attr[6];
	}
	
	public Date getDate() {
		try {
			return parser.parse(attr[1]);
		} catch (ParseException e) {
			return null;
		}
	}	
	
	public HttpEvent getEvent() {
		return http;
	}
}
