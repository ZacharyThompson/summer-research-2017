import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Session {
	private Date logon;
	private Date logoff;
	private String pc;
	
	List<HttpEvent> http = new ArrayList<HttpEvent>();
	
	private List<Event> event = new ArrayList<Event>();
	
	public Session(String pc) {
		this.pc = pc;
	}
	
	public Session(String pc, Date date) {
		this.pc = pc;
		this.logon = date;
	}
	
	public void Login(Date date) {
		this.logon = date;
	}
	
	public void Logoff(Date date) {
		this.logoff = date;
	}
	
	public Date getLogin() {
		return logon;
	}
	
	public Date getLogoff() {
		return logoff;
	}
	
	
	@Override
	public String toString() {
		String sLogon = (logon == null ? "undefined" : logon.toString());
		String sLogoff = (logoff == null ? "undefined" : logoff.toString());		
		return pc + " : " + sLogon + " - " + sLogoff + " : " + http.size();
	}
	
	public boolean contains(Date date) {
		if (logon == null || logoff == null) {
			return false;
		}
		return (!logon.after(date) && !logoff.before(date));
	}
	
	public void addHttp(HttpEvent event) {
		http.add(event);
	}
	
	public void addEvent(Event event) {
		this.event.add(event);
	}
	
	public int count() {
		return http.size() + event.size();
	}
	
	public String getInstance() {
		int files = 0;
		int activity[] = new int[4];
		int from_removable = 0;
		int to_removable = 0;
		
		for (Event e : event) {
			if (e instanceof FileEvent) {
				files++;
				switch (((FileEvent) e).getActivity()) {
					case "File Open": activity[0]++;
					case "File Copy": activity[1]++;
					case "File Delete": activity[2]++;
					case "File Writer": activity[3]++;
				}
				from_removable += ((FileEvent) e).isFrom_removable_media() ? 1 : 0;
				to_removable += ((FileEvent) e).isTo_removable_media() ? 1 : 0;			
			}
		}
		return (files + "," + activity[0] + "," + activity[1] + "," + activity[2] + "," + activity[3] + "," + from_removable + "," + to_removable);
	}
	
	
	public String getHttpInstance() {
		if (http.size() == 0) {
			return "";
		}	
		
		String urls = "";
		for (HttpEvent h : http) {
			urls += "&" + h.getUrl();
		}
		urls = urls.substring(0, urls.length()-1);
		String content = "";
		for (HttpEvent h : http) {
			content += "&" + h.getContent();
		}
		content = content.substring(0, content.length()-1);
		return "\"" + urls + "\",\"" + content + "\"";
	}	
}
