import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Processor {
	private File fInput;
	private File fOutput;
	protected List<String> users = new ArrayList<String>();
	protected List<String> header = new ArrayList<String>();
	private BufferedReader bReader;
	private PrintWriter pWriter;
	
	protected String currentLine;
	
	public Processor(File fInput, File fOutput) {
		this.fInput = fInput;
		this.fOutput = fOutput;
	}
	
	public void openInput() {
		try {
			bReader = new BufferedReader (new FileReader(fInput));		
			// Discard header line
			readLine();
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	public void openOutput() {
		try {
			if (fOutput.exists()) {
				fOutput.delete();
			}
			fOutput.createNewFile();
			pWriter = new PrintWriter (new OutputStreamWriter(new FileOutputStream(fOutput)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeHeader() {
		try {
			pWriter.print(this.getHeader());
			pWriter.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getHeader() {
		return "";
	}
	
	public String readLine() {
		try {
			return (currentLine = bReader.readLine());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean writeLine(String line) {
		try {
			if (line == "") { return false; }
			pWriter.println(line);
			pWriter.flush();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String[] splitLine(String line) {
		return (new String[] { line });
	}
	
	public String toInstance(String line) {
		return line;
	}
	
	public User getUser() {
		return null;
	}
	

	public void addUser(String... users) {
		for (String user : users) {
			this.users.add(user);
		}
	}
}
