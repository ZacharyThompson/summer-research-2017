import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class User {
	private String name;
	private List<Session> sessions = new ArrayList<Session>();	
	private Processor p;
	Boolean print = false;
	

	public User(String name) {
		this.name = name;
	}
	
	public void setProcessor(Processor p) {
		this.p = p;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!User.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		final User other = (User) obj;
		return (this.name.equals(other.name));
	}
	
	public void addLogon(String activity, String pc, Date date) {
		//System.out.println(activity + " " + pc + " " + date.toString());
		if (activity.equals("Logon")) {
			sessions.add(new Session(pc, date));
		} else if (activity.equals("Logoff")) {
			sessions.get(sessions.size()-1).Logoff(date);
		}
	}
	
	public void printSessions() {
		for (Session session : sessions) {
			System.out.println(session.toString());
		}
	}
	
	public void addHttp(HttpEvent event) {
		List<Session> rotateSessions = new ArrayList<Session>();
				
		for (Iterator<Session> iterator = sessions.iterator(); iterator.hasNext();) {
			Session session = iterator.next();
			if (session.contains(event.getDate())) {
				session.addHttp(event);
				break;
			} else if (session.getLogoff() == null) {
				iterator.remove();
			} else if (session.getLogoff().before(event.getDate())) {
				iterator.remove();
				if (print && session.count() > 0) {
					writeSession(session);	
				} else {
					rotateSessions.add(session);
				}
			}
		}
		if (!print) {
			sessions.addAll(rotateSessions);
		}
	}
	
	public void addEvent(Event event) {
		for (Iterator<Session> iterator = sessions.iterator(); iterator.hasNext();) {
			Session session = iterator.next();
			if (session.contains(event.getDate())) {
				session.addEvent(event);
				return;
			} else if (session.getLogoff() == null) {
				iterator.remove();
			} else if (session.getLogoff().before(event.getDate())) {
				if (session.count() > 0) {
				}
				iterator.remove();
			}
		}
	}
	
	public void fixSessions() {
		Iterator<Session> iterator = sessions.iterator();
		if (!iterator.hasNext()) return;
		Session current = iterator.next();
		if (!iterator.hasNext()) return;
		Session next = iterator.next();
		while (iterator.hasNext()) {	
			if (current.getLogoff() == null) {
				current.Logoff(new Date(next.getLogin().getTime() - 1));
			}
			current = next;
			next = iterator.next();
		}
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public List<Session> getSessions() {
		return sessions;
	}
	
	public enum Type {
		SESSION, HTTP, FILE, EMAIL, UCI
	}
	private List<Type> types = new ArrayList<Type>();
	
	public void addType (Type type) {
		if (!types.contains(type)) {
			types.add(type);
		}
	}
	
	public void removeType (Type type) {
		types.remove(type);
	}
	
	public void writeSession(Session session) {
		if (session.http.size() == 0 || p == null || !print) {
			return;
		}

		switch (types.size()) {
			case 1:
				if (types.contains(Type.HTTP)) {
					for (HttpEvent http : session.http) {
						p.writeLine(name + ",\"" + http.getUrl() + "\",\"" + http.getContent() + "\"");
					}
				} else if (types.contains(Type.FILE)) {
					
				} else if (types.contains(Type.UCI)) {
					p.writeLine("**SOF**");
					for (HttpEvent http : session.http) {
						try {
							URI uri = new URI(http.getUrl());
							p.writeLine(uri.getHost());
						} catch (URISyntaxException e) {
							p.writeLine("unknown");
						}				
					}			
					p.writeLine("**EOF**");
					return;
				}
			case 2:
				if (types.contains(Type.SESSION) && types.contains(Type.HTTP)) {
					String urls = "";
					String content = "";
					for (HttpEvent http : session.http) {
						urls += http.getUrl() + "&";
						content += "\'" + http.getContent() + "\'&";			
					}
					p.writeLine(name + ",\"" + urls + "\",\"" + content + "\"");		
				} else if (types.contains(Type.SESSION) && types.contains(Type.UCI)) {
					String urls = "";
					for (HttpEvent http : session.http) {
						String url = http.getUrl();
						try {
							URI uri = new URI(url);
							urls += uri.getHost() + " ";
						} catch (URISyntaxException e) {
							urls += "unknown ";
							System.out.println("URI error");
						}				
					}			
					p.writeLine(urls);
					return;
				}
		}	
	}
}
