import java.io.File;

public class UserProcessor extends Processor {
	
	public UserProcessor(File fInput, File fOutput) {
		super(fInput, fOutput);
	}
	
	@Override
	public User getUser() {
		String user = currentLine.split(",")[1];		
		return new User(user);
	}
}
