import java.util.Date;

public class HttpEvent implements Event {
		private String url;
		private String activity;
		private String content;
		private Date date;
		
		public HttpEvent(Date date, String url, String activity, String content) {
			this.url = url;
			this.activity = activity;
			this.content = content;
			this.date = date;
			
			this.content = this.content.replace("\"", "");
			this.content = this.content.replace("\'", "");
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getActivity() {
			return activity;
		}

		public void setActivity(String activity) {
			this.activity = activity;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public Date getDate() {
			return date;
		}

		@Override
		public void setDate(Date date) {
			this.date = date;
		}

		@Override
		public String toInstance() {
			return null;
		}
	}