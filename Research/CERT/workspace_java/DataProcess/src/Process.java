import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
 * Performance Enhancements:
 * 	1. Rolling session history - Current attempt bit clunky
 *  2. Users as HashMap
 *  3. 
 */


public class Process {
	public static String dirData = "E:/Research/CERT Division/data/r6.2/";
	
	public static int logonLines = 3540000;
	public static int fileLines = 1000000000;
	public static int httpLines = 1000000000;
	
	public static int logonMax = 3530285;
	public static int httpMax = 0;
	
	public static int progressInt = 500;
	public static int progressHTTP = 100000;

	public static void main (String args[]) {
		if (args.length > 0) {
			dirData = args[0];
		}
		
		List<User> users = new ArrayList<User>();
		List<User.Type> types = new ArrayList<User.Type>();
		
		//Get 100 users
		
		UserProcessor up = new UserProcessor(new File(dirData + "LDAP/2009-12.csv"), null);
		up.openInput();
		for (int i = 0; i < 100; i++) {
			up.readLine();
			users.add(up.getUser());
		}
		
		users.add(new User("SDH2394"));
		users.add(new User("VJB1928"));
		users.add(new User("IMC3296"));
		users.add(new User("LJC0191"));
		
		for (User user : users) {
			System.out.println("\""+user.toString() + "\" : 0,");
		}
		
		
		
		//uciHTTP(users);
		
		/*String header = "@relation none" + System.lineSeparator();
		header += "@attribute user {" + listToNominal(users) + "}" + System.lineSeparator();
		header += "@attribute url String" + System.lineSeparator();
		header += "@attribute content String" + System.lineSeparator();
		header += "@data";*/
		//types.add(User.Type.SESSION);
		//types.add(User.Type.HTTP);
		//processHTTP(header, users, types)
	}
	
	public static void uciHTTP(List<User> users) {
		//String tensorPath = "E:/Research/Tensorflow Models/cnn-text-classification/data/cert-data/";
		String uciPath = "E:/Research/UCI Unix/data/cert_data/http/";
		
		HttpProcessor p = new HttpProcessor(new File(dirData + "http.csv"), null);
		p.openInput();
		
		for (User user : users) {
			p.addUser(users.toString());
			Processor proc = new Processor(null, new File(uciPath + user));
			proc.openOutput();
			user.setProcessor(proc);
			user.print = true;
			user.addType(User.Type.UCI);
		}
		
		getSessions(users);
		System.out.print("Http Progress : \r");
		int progress = 0;
		/* Read Input */
		int i = 0;
		for (i = 0; (i < httpLines) && (p.readLine() != null); i++) {
			for (User user : users) {
				// Check if the user matches the user of the current instance
				if (user.equals(p.getUser())) {
					// Add event to the user
					user.addHttp(p.getEvent());
					break;
				}
			}
			if (i >= progress) {
				System.out.print("HTTP Progress : \t" + i + "\r");
				progress += httpLines / progressHTTP;
			}
		}
		System.out.println("Sessions Progress : \t" + i);
		
	}
	

	public static void processHTTP(String header, List<User> users, List<User.Type> types) {
		String oFileName = getFileName(users, types);

		HttpProcessor p = new HttpProcessor(new File(dirData + "http.csv"), new File(dirData + "/" + oFileName));
		p.openInput();
		p.openOutput();
		
		p.writeLine(header);
		
		for (User user : users) {
			p.addUser(users.toString());
			user.setProcessor(p);
			user.print = true;
			for (User.Type type : types) {
				user.addType(type);
			}
		}
		
		getSessions(users);
		
		System.out.print("Http Progress : \r");
		int progress = 0;
		/* Read Input */
		int i = 0;
		for (i = 0; (i < httpLines) && (p.readLine() != null); i++) {
			for (User user : users) {
				// Check if the user matches the user of the current instance
				if (user.equals(p.getUser())) {
					// Add event to the user
					user.addHttp(p.getEvent());
					break;
				}
			}
			if (i >= progress) {
				System.out.print("HTTP Progress : \t" + i + "\r");
				progress += httpLines / progressHTTP;
			}
		}
		System.out.println("Sessions Progress : \t" + i);
	}

	public static String getFileName(List<User> users, List<User.Type> types) {
		String fileName = "";
		fileName += (types.contains(User.Type.SESSION) ? "session_" : "");
		fileName += (types.contains(User.Type.HTTP) ? "http_" : "");
		fileName += (types.contains(User.Type.FILE) ? "file_" : "");
		fileName += (types.contains(User.Type.EMAIL) ? "email_" : "");
		fileName += "(" + users.size() + ").arff";

		return "arff/" + fileName;
	}

	public static String listToNominal(List<?> users) {
		String su = "";
		for (Object user : users) {
			su += user.toString() + ",";
		}
		return su.substring(0, su.length()-1);
	}
	
	public static void getSessions(List<User> users) {
		File logon_input = new File(dirData + "logon.csv");
		File logon_output = null;
		LogonProcessor lp = new LogonProcessor(logon_input, logon_output);
		lp.openInput();
		
		System.out.print("Sessions Progress : \r");
		int progress = logonLines / progressInt;
		int i = 0;
		for (i = 0; i < logonLines && lp.readLine() != null; i++) {
			for (User user : users) {
				if (user.equals(lp.getUser())) {
					user.addLogon(lp.getActivity(), lp.getComputer(), lp.getDate());
				};				
			}
			if (i >= progress) {
				System.out.print("Sessions Progress : \t" + i + "\r");
				//System.out.print("*");
				progress += logonLines / progressInt;
			}
		}
		System.out.println("Sessions Progress : \t" + i);
		
		for (User user : users) {
			user.fixSessions();
		}
	}
	
	public static int lineCount(Processor p) {
		int i = 0;
		while (p.readLine() != null) {
			i++;
		}
		return i;
	}
}
