import java.util.Date;

public class FileEvent implements Event {
	Date date;
	String filename;
	String activity;
	String content;
	boolean to_removable_media;
	boolean from_removable_media;
	
	public FileEvent(Date date, String filename, String activity, boolean to_removable_media, boolean from_removable_media, String content) {
		this.date = date;
		this.filename = filename;
		this.activity = activity;
		this.content = content;
		this.to_removable_media = to_removable_media;
		this.from_removable_media = from_removable_media;
		
	}

	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isTo_removable_media() {
		return to_removable_media;
	}

	public void setTo_removable_media(boolean to_removable_media) {
		this.to_removable_media = to_removable_media;
	}

	public boolean isFrom_removable_media() {
		return from_removable_media;
	}

	public void setFrom_removable_media(boolean from_removable_media) {
		this.from_removable_media = from_removable_media;
	}

	@Override
	public String toInstance() {
		// TODO Auto-generated method stub
		return null;
	}
}
