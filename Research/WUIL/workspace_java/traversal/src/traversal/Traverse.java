package traversal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Traverse {
	public static void main (String args[]) {
		File file = new File("E:/Research/WUIL Logs/data/User1/User Log/log.txt");
		
		FileNode root = new FileNode(0);
		root.resetSequence();
		
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	line = line.split("\\|")[5];
		    	root.AddPath(line);
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		FileNode node = root;
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
		    String line;
		    int i = 0;
		    while ((line = br.readLine()) != null && i < 1000) {
		    	line = line.split("\\|")[5];
		    	node = node.traverseTo(line);
		    	i++;
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		System.out.println(root.getSequence());
		
	}
}
