package fireesc;

import java.io.*;
import java.util.*;
 
class FireEsc
{
	int[][] adj;
	int n, member;
	
	public void dfs(int u, boolean[] visited, FireEsc f)
	{
		visited[u]=true;
		f.member++;
		for(int i=0; i<f.n; i++)
			if(f.adj[u][i]==1 && !visited[i])
				dfs(i, visited, f);
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t, m, i, j, u, v, count, result;
		t=sc.nextInt();
		for(i=0; i<t; i++)
		{
			FireEsc f = new FireEsc();
			f.n=sc.nextInt();
			f.adj=new int[f.n][f.n];
			m=sc.nextInt();
			for(j=0; j<m; j++)
			{
				u=sc.nextInt();
				v=sc.nextInt();
				f.adj[u-1][v-1]=1;
				f.adj[v-1][u-1]=1;
			}
			count=0;
			result=1;
			Vector<Integer> vct = new Vector<Integer>();
			boolean[] visited = new boolean[f.n];
			for(j=0; j<f.n; j++)
			{
				if(!visited[j])
				{
					f.member=0;
					f.dfs(j, visited, f);
					count++;
					vct.add(f.member);
				}
			}
			for(Integer k:vct)
				result=result*k;
			result=result%1000000007;
			System.out.println(count+" "+result);
		}
	}
} 