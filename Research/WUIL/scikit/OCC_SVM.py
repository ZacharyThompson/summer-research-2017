import pandas as pd
import scipy as sc
import numpy as np
from sklearn.preprocessing import MinMaxScaler, StandardScaler, Normalizer

path = "E:/Research/WUIL Logs/data.csv"
dataframe = pd.read_csv(path)
array = dataframe.values

X = array[:,1:5]
Y = array[:,0]

# Rescale Data
#scaler = MinMaxScaler(feature_range=(0, 1))
#X = scaler.fit_transform(X)

# Standardize Data
#scaler = StandardScaler().fit(X)
#X = scaler.transform(X)

# Normalize Data
#scaler = Normalizer().fit(X)
#X = scaler.transform(X)


np.set_printoptions(precision=3)
print(X[0:5,:])