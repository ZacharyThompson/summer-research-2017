from __future__ import print_function
import re
import json
import sys
import os.path
import csv
import datetime
import numpy as np
import contextlib
import zipfile
from os import walk


def SplitLine(line):
	line = line.replace("\n", "")
	ret = (line.split("|"))
	return ret
	
def SegmentArray(array, size):
	for i in range(0, len(array), size):
		yield array[i:i + size]
		
def RandomData(vocab, length, size):
	total = 0
	paths = []
	for key in vocab:
		total += vocab[key]
		paths.append(key)
		
	p = [0] * len(paths)
	for index in range(len(paths)):
		p[index] = vocab[paths[index]] / total
	
	data = []
	for value in range(size):
		data.append(np.random.choice(paths, length, p=p))
	return data
	

def UniqueUser(input_path, files, maxSizeTrain = 2000, maxSizeTest = 2000, randomSize = 100):
	size = 50
	
	data = dict()
	for class_ in files:
		data[class_] = dict()
		for filename in files[class_]:
			data[class_][filename] = []
			print(input_path + "/" + filename)
			with open(input_path + "/" + filename, "r") as f:
				for line in f.readlines():
					data[class_][filename].append(SplitLine(line)[5])
	
	
	rawData = dict()
	
	for class_ in data:
		rawData[class_] = dict()
		for filename in data[class_]:
			rawData[class_][filename] = []
			for item in SegmentArray(data[class_][filename], size):
				rawData[class_][filename].append(item)
			
	trainData = dict()
	testData  = dict()
	
	for class_ in rawData:
		trainData[class_] = []
		testData[class_] = []
		for filename in rawData[class_]:
			array = rawData[class_][filename]
			np.random.shuffle(array)
			split = int(len(array) * files[class_][filename])
			train, test = array[:split], array[split:]
			trainData[class_].extend(train)
			testData[class_].extend(test)
			
	for class_ in files:
		np.random.shuffle(trainData[class_])
		trainData[class_] = trainData[class_][:maxSizeTrain]
		np.random.shuffle(testData[class_])
		testData[class_] = testData[class_][:maxSizeTest]

	output = "E:/Research/Tensorflow Models/cnn-text-classification/data/wuil-data/"
		
	# Print Data
	for class_ in files:
		with open(output + "wuil-train." + class_, "w") as tensor_f:
			for val in trainData[class_]:
				line = ' '.join(val)
				line = line.replace("\\", " ")
				print(line, file=tensor_f)
		with open(output + "wuil-test." + class_, "w") as tensor_f:
			for val in testData[class_]:
				line = ' '.join(val)
				line = line.replace("\\", " ")
				print(line, file=tensor_f)
				
def MultipleUsers(input_path, output_path, files, maxSizeTrain = 20000, maxSizeTest = 2500, randomSize = 5000,
	instanceSize = 50, linesToKeep = 1000):
	
	np.random.seed(10)
	freqsLength = 100000
	freq = [0] * freqsLength
	freqs = dict()
	
	vocab = {
		"train" : dict(),
		"test" : dict()
	}
	
	maxSize = {
		"train" : maxSizeTrain,
		"test" : maxSizeTest
	}
	trainDirs = 0
	randomSplit = -1
	
	for class_key, class_val in files.items():
		for set in data:
			data[set][class_key] = []
			
			
		for dir_key, dir_val in class_val.items():
			uRange = dir_key.replace("User", "").split("-")
			uRange = [int(i) for i in uRange]
			for uNum in range(uRange[0], uRange[1] + 1):
				for name_key, name_val in dir_val.items():
					fileData = []
					with open(input_path + "/User" + str(uNum) + "/" + name_key, "r") as f:
						fileData = [SplitLine(line)[5] for line in f.readlines()]
					
					instances = [item for item in SegmentArray(fileData, instanceSize)]		
					np.random.shuffle(instances)
					instances = instances[:linesToKeep]
					split = int(len(instances) * name_val)
									
					train, test = instances[:split], instances[split:]
					if len(train) > 0 and uNum not in freqs:
						freqs[uNum] = [0] * freqsLength
						
					for instance in train:
						for path in instance:
							parts = path.split("\\")
							for file in parts:
								freqs[uNum][int(file)] = 1
										
					data["train"][class_key].extend(train)
					data["test"][class_key].extend(test)
					
	
	minFreq = 0.8 * len(freqs)
	for dir in freqs:
		for i in range(freqsLength):
			freq[i] += freqs[dir][i]
			
	cnn_path = output_path
	rnn_path = "E:/Research/Tensorflow Models/jiegshan/data/"
	
	for type in data:
		with open(rnn_path + "wuil-" + type + ".csv", "w") as rnn_file:
			print("Category,Descript", file=rnn_file)
			for class_ in data[type]:
				np.random.shuffle(data[type][class_])
				data[type][class_] = data[type][class_][:maxSize[type]]
				print(class_ + " " + type + " : " + str(len(data[type][class_])))
				
				with open(output_path + "wuil-" + type + "." + class_, "w") as cnn_file:
					for vPaths in data[type][class_]:
						line = ""
						for vPath in vPaths:
							vPath = vPath.split("\\")
							for vFile in vPath:
								vFile = vFile if freq[int(vFile)] > minFreq else "U"
								line = line + " " + vFile
							#line = str(len(line) 
						print(line, file=cnn_file)
						print(class_ + "," + line, file=rnn_file)

def MultipleUser(input_path, output_path, files, maxSizeTrain = 20000, maxSizeTest = 2500, randomSize = 5000,
	instanceSize = 50, linesToKeep = 1000):
	
	np.random.seed(10)
	freqsLength = 100000
	freq = [0] * freqsLength
	freqs = dict()
	
	vocab = dict()
	data = {
		"train" : dict(),
		"test" : dict()
	}
	maxSize = {
		"train" : maxSizeTrain,
		"test" : maxSizeTest
	}
	trainDirs = 0
	randomSplit = -1
	
	rawData = dict()
	
	# Read Data
	for class_key, class_val in files.items():
		rawData[class_key] = dict()
		for dir_key, dir_val in class_val.items():
			uRange = dir_key.replace("User", "").split("-")
			uRange = [int(i) for i in uRange]
			for uNum in range(uRange[0], uRange[1] + 1):
				rawData[class_key]["User" + str(uNum)] = dict()
				vocab["User" + str(uNum)] = dict()
				for name_key, name_val in dir_val.items():
					rawData[class_key]["User" + str(uNum)][name_key] = {
						"split" : name_val,
						"data" : []
					}
					with open(input_path + "/User" + str(uNum) + "/" + name_key, "r") as f:
						rawData[class_key]["User" + str(uNum)][name_key]["data"].extend([SplitLine(line)[5] for line in f.readlines()])

	# Split Data into instances
	seperateTest = dict()
	seperateTrain = dict()
	for class_key, class_val in rawData.items():
		data["train"][class_key] = []
		data["test"][class_key] = []
		for user_key, user_val in class_val.items():
			train, test = [], []
			print(user_key)
			for file_key, file_val in user_val.items():
				print(file_key)
				instances = [item for item in SegmentArray(file_val["data"], instanceSize)]		
				np.random.shuffle(instances)
				instances = instances[:linesToKeep]
				split = int(len(instances) * file_val["split"])
													
				train.extend(instances[:split])
				test.extend(instances[split:])
				
				if len(train) > 0 and uNum not in freqs:
					freqs[uNum] = [0] * freqsLength
						
				
				for instance in train:
					for path in instance:
						if path not in vocab[user_key]:
							vocab[user_key][path] = 0
						vocab[user_key][path] += 1
						total += 1
						
			seperateTest[user_key]
			newTrain = []
			for instance in train:
				newInstance = []
				for path in instance:
					if path not in vocab[user_key]:
						vocab[user_key][path] = 0
					newPath = vocab[user_key][path] / total
					newPath *= 1000
					newInstance.append(newPath)
				newTrain.append(newInstance)
					
			newTest = []
			for instance in test:
				newInstance = []
				for path in instance:
					if path not in vocab[user_key]:
						vocab[user_key][path] = 0
					newPath = vocab[user_key][path] / total
					newPath *= 1000
					newInstance.append(newPath)
				newTest.append(newInstance)

			data["train"][class_key].extend(train)
			data["test"][class_key].extend(test)
					
									
			
						
def main():
	input_path = "E:/Research/WUIL Logs/data/"
	output_path = "E:/Research/Tensorflow Models/cnn-text-classification/data/wuil-data/"
	files = {
		"positive" : {
			"User2-10" : {
				"/Attack 1 Log/log.txt" : 1,
				"/Attack 2 Log/log.txt" : 1,
				"/Attack 3 Log/log.txt" : 1	
			},			
			"User1-1" : {
				"/User Log/log.txt" : 0.95
			}
		},
		"negative" : {
			"User2-10" : {
				"/Attack 1 Log/log.txt" : 1,
				"/Attack 2 Log/log.txt" : 1,
				"/Attack 3 Log/log.txt" : 1	
			},
			"User1-1" : {
				"/User Log/log.txt" : 0
			}
		}
	}
	
	#UniqueUser(input_path + "/User1", files)
	MultipleUser(input_path, output_path, files)
	#OldMultipleUsers(input_path, output_path, file, dirs)
	
if __name__ == "__main__":
	main()

