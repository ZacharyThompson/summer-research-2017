from __future__ import print_function
import re
import json
import sys
import os.path
import csv
import datetime
import numpy as np
import contextlib
from os import walk

# TO DO:
	# Improve Test/Train distribution
	# Add in method to create random NEGATIVE instances and use to benchmark performance. It is highly unlikely that a random instance will resemble this.


	
	
def levDistance(source, target):
    if len(source) < len(target):
        return levDistance(target, source)

    if len(target) == 0:
        return len(source)
		
    source = np.array(tuple(source))
    target = np.array(tuple(target))
	
    previous_row = np.arange(target.size + 1)
    for s in source:
        current_row = previous_row + 1
		
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]
	
def cleanWord(word):
	word = ''.join(s for s in word if ord(s)>31 and ord(s)<126)
	return word
	
def cleanString(string):
	string = string.replace("\"", "")
	string = string.replace("\\", "")
	string = string.replace("/", "")
	string = string.replace("<", " <")
	string = string.replace(">", "> ")
	string = string.replace("  ", " ")
	#string = string.replace(".", " ")
	string = string.replace("*", "")
	return string
	
def println(string):
	sys.stdout.write(str(string) + "\n")
	sys.stdout.flush()
	
def findClosest(vocab, string):
	dClosest = 100000
	wClosest = ""
	fClosest = 0
	for word in vocab:
		distance = levDistance(string, word)
		if (distance < dClosest) or (distance == dClosest and vocab[word] > fClosest):
			dClosest = distance
			wClosest = word
			fClosest = vocab[word]
	return wClosest

def RandomInstance(vocab):
	return ""

def GenerateDataset(trainFiles, type, input, output, maxSizeTrain = 400, maxSizeTest = 200):
	vocab = dict()
	rawData = dict()
	
	numRandom = 0
	if ("RANDOM" in trainFiles["negative"]):
		numRandom = trainFiles["negative"]["RANDOM"]
		trainFiles["negative"].pop("RANDOM", None)
	minTermFreq = 20
	minInstanceSize = 5
	distrSize = []
	
	# Read in data
	println("Reading Data : ")
	for class_ in trainFiles:
		rawData[class_] = dict()
		for filename in trainFiles[class_]:
			rawData[class_][filename] = []
			line = ""
			with open(input + "/" + filename, 'r') as f:
				for word in f.readlines():
					if ("**EOF**" in word):
						line = cleanString(line)
						words = line.split()
						if (len(words) < minInstanceSize):
							continue
						rawData[class_][filename].append(line)
						line = ""
						continue
					elif ("**SOF**" in word):
						continue
					else:
						line = line + " " + cleanWord(word)

	np.random.seed(10)

	trainData = dict()
	testData = dict()

	# Split data into test and training sets
	println("Splitting Data : ")
	for class_ in rawData:
		trainData[class_] = []
		testData[class_] = []
		for filename in rawData[class_]:
			array = rawData[class_][filename]
			np.random.shuffle(array)
			split = int(len(array) * trainFiles[class_][filename])
			train, test = array[:split], array[split:]
			trainData[class_].extend(train)
			testData[class_].extend(test)

	# Build vocab from training set
	println("Building Vocab : ")
	for class_ in trainData:
		for line in trainData[class_]:
			words = line.split()
			distrSize.append(len(words))
			for word in words:
				if word in vocab:
					vocab[word] += 1
				else:
					vocab[word] = 1

	# Clean Vocab
	println("Cleaning Vocab : ")
	cleanVocab = dict()
	for word, freq in vocab.items():
		if (freq >= minTermFreq):
			cleanVocab[word] = vocab[word]

	# Generate Random Data
	println("Generating Random : ")
	p = []
	for val in range(0, max(distrSize) + 1):
		p.append(distrSize.count(val) / len(distrSize))

	sizes = random = np.random.choice(max(distrSize) + 1, numRandom, p=p)

	vocabKeys = list(vocab.keys())
	for size in sizes:
		line = ""
		for val in range(0, size):
			line += vocabKeys[np.random.random_integers(0, len(vocabKeys)-1)] + " "
		testData["negative"].append(line)
		
	# Split data to specified sizes
	for class_ in trainData:
		np.random.shuffle(trainData[class_])
		trainData[class_] = trainData[class_][:maxSizeTrain]
	
	for class_ in testData:
		np.random.shuffle(testData[class_])
		testData[class_] = testData[class_][:maxSizeTest]

	# Replace Words
	println("Replacing Words (Train) : ")
	cTrainData = dict()
	for class_ in trainData:
		cTrainData[class_] = []
		for line in trainData[class_]:
			words = line.split()
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cTrainData[class_].append(line)

		
	# Replace Words
	println("Replacing Words (Test) : ")
	cTestData = dict()
	for class_ in testData:
		cTestData[class_] = []
		for line in testData[class_]:
			words = line.split()
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cTestData[class_].append(line)

	# Save datasets
	print("Dataset Sizes")
	with open("weka_data/" + type + "-train.arff", "w") as weka_f:
		print("@relation " + type + "-data", file=weka_f)
		print("@attribute history string", file=weka_f)
		print("@attribute class {positive, negative}", file=weka_f)
		print("@data", file=weka_f)

		for class_ in cTrainData:
			print("	" + type + "-train." + class_ + " : " + str(len(cTrainData[class_])))
			with open(output + type + "-train." + class_, "w") as tensor_f:
				for line in cTrainData[class_]:
					print(line, file=tensor_f)
					
			for line in cTrainData[class_]:
				print("\"" + line + "\"," + class_, file=weka_f)

	with open("weka_data/" + type + "-test.arff", "w") as weka_f:
		print("@relation " + type + "-data", file=weka_f)
		print("@attribute history string", file=weka_f)
		print("@attribute class {positive, negative}", file=weka_f)
		print("@data", file=weka_f)
		
		for class_ in cTestData:
			print(" " + type + "-test." + class_ + " : " + str(len(cTestData[class_])))
			with open(output + type + "-test." + class_, "w") as tensor_f:
				for line in cTestData[class_]:
					print(line, file=tensor_f)
					
			for line in cTestData[class_]:
				print("\"" + line + "\"," + class_, file=weka_f)
	

def main():
	if (len(sys.argv) < 3):
		print("Expected Input : python CNN_dataset <type> <input path> <output path>")
		sys.exit()
		
	type = sys.argv[1]
	input = sys.argv[2] + "/"
	output = sys.argv[3] + "/"
	print(type + " " + input + " " + output)

	uci_data = {
		"positive" : {
			"USER0" : 0.8,
			"USER1" : 0.8
		},
		"negative" : {
			"USER2" : 1,
			"USER3" : 1,
			"USER4" : 1,
			"USER5" : 1,
			"USER6" : 0,
			"USER7" : 0,
			"USER8" : 0,
			"RANDOM" : 200
		}
	}
	cert_data = {
		"positive" : {
			"SDH2394" : 0.8
		},
		"negative" : {
			"NFP2441" : 1,
			"ADR1517" : 1,
			"MWB4000" : 1,
			"MLS2856" : 1,
			"BTR2026" : 1,
			"ZCT1009" : 1,
			"SEM1983" : 1,
			"BDW1459" : 1,
			"FAJ3101" : 1,
			"CLR1239" : 1,
			"LOP1708" : 1,
			"GKK2422" : 1,
			"MYB3817" : 1,
			"JBO2839" : 1,
			"JTC0941" : 0,
			"IKA2925" : 0,
			"JSN1905" : 0,
			"JRL2365" : 0,
			"GJB0850" : 0,
			"LMD2804" : 0,
			"KMB3003" : 0,
			"LWV1525" : 0,
			"RBB3867" : 0,
			"ARF0575" : 0,
			"WSC2981" : 0,
			"HKT1778" : 0,
			"MAS3218" : 0,
			"MJG2304" : 0,
			"CCH1508" : 0,
			"HDN2609" : 0,
			"FCM3926" : 0,
			"DMR0336" : 0,
			"XAB0936" : 0,
			"LCB2041" : 0,
			"ZJB0552" : 0,
			"AKP3573" : 0,
			"JRA0522" : 0,
			"RLS1738" : 0,
			"CWK2870" : 0,
			"BRH1107" : 0,
			"TEF3555" : 0,
			"GEH0420" : 0,
			"VCV2521" : 0,
			"LJR0253" : 0,
			"RPH2326" : 0,
			"FJO1828" : 0,
			"JIG1936" : 0,
			"OAS0991" : 0,
			"CRT2273" : 0,
			"KGR2815" : 0,
			"ACH1910" : 0,
			"WDS2197" : 0,
			"TJM0328" : 0,
			"JJS2973" : 0,
			"UDW0487" : 0,
			"SHB2106" : 0,
			"PCT1234" : 0,
			"KAV2926" : 0,
			"ADS0763" : 0,
			"SKG1731" : 0,
			"ISJ1479" : 0,
			"CAG2236" : 0,
			"YKW3832" : 0,
			"AVC2328" : 0,
			"CSP2186" : 0,
			"DBG0477" : 0,
			"HPR2023" : 0,
			"HAR3571" : 0,
			"NJM0440" : 0,
			"IBW1779" : 0,
			"BVB1673" : 0,
			"DDF2937" : 0,
			"NDH3827" : 0,
			"NLS1866" : 0,
			"MKM0647" : 0,
			"DCR1531" : 0,
			"BSB3083" : 0,
			"SLM1119" : 0,
			"KCO0269" : 0,
			"HVS1143" : 0,
			"JRR0829" : 0,
			"BMF2337" : 0,
			"JRK2707" : 0,
			"CEC1352" : 0,
			"KDB0266" : 0,
			"MKR2138" : 0,
			"ALS3440" : 0,
			"AZG2343" : 0,
			"RJS0846" : 0,
			"BRG3267" : 0,
			"QNW0977" : 0,
			"CNT0946" : 0,
			"DMR2518" : 0,
			"DSB0245" : 0,
			"LBR1387" : 0,
			"CKB3645" : 0,
			"DHG0962" : 0,
			"HMR0098" : 0,
			"TLW2668" : 0,
			"LLH3157" : 0,
			"VJB1928" : 0,
			"IMC3296" : 0,
			"LJC0191" : 0
		}
	}	
	GenerateDataset(cert_data, type, input, output, maxSizeTrain = 1000, maxSizeTest = 1000)
		
	

if __name__ == "__main__":
	main()

