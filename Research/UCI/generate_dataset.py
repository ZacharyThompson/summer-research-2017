from __future__ import print_function
import re
import json
import sys
import os.path
import csv
import datetime
import numpy as np
import contextlib
from os import walk

# TO DO:
	# Improve Test/Train distribution
	# Add in method to create random NEGATIVE instances and use to benchmark performance. It is highly unlikely that a random instance will resemble this.



def levDistance(source, target):
    if len(source) < len(target):
        return levDistance(target, source)

    if len(target) == 0:
        return len(source)
		
    source = np.array(tuple(source))
    target = np.array(tuple(target))
	
    previous_row = np.arange(target.size + 1)
    for s in source:
        current_row = previous_row + 1
		
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]
	
def cleanWord(word):
	word = ''.join(s for s in word if ord(s)>31 and ord(s)<126)
	return word
	
def cleanString(string):
	string = string.replace("\"", "")
	string = string.replace("\\", "")
	string = string.replace("/", "")
	string = string.replace("<", " <")
	string = string.replace(">", "> ")
	string = string.replace("  ", " ")
	string = string.replace(".", " ")
	string = string.replace("*", "")
	return string
	
def println(string):
	sys.stdout.write(str(string) + "\n")
	sys.stdout.flush()
	
def findClosest(vocab, string):
	dClosest = 100000
	wClosest = ""
	fClosest = 0
	for word in vocab:
		distance = levDistance(string, word)
		if (distance < dClosest) or (distance == dClosest and vocab[word] > fClosest):
			dClosest = distance
			wClosest = word
			fClosest = vocab[word]
	return wClosest

def RandomInstance(vocab):
	return ""

def Tensorflow(trainFiles, output):
	path = "data/shell_data"
	vocab = dict()
	rawData = dict()
	numRandom = trainFiles["negative"]["RANDOM"]
	trainFiles["negative"].pop("RANDOM", None)
	minTermFreq = 20
	minInstanceSize = 5
	maxSizeTrain = 200
	maxSizeTest = 200
	distrSize = []
	
	# Read in data
	println("Reading Data : ")
	for class_ in trainFiles:
		rawData[class_] = dict()
		for filename in trainFiles[class_]:
			rawData[class_][filename] = []
			line = ""
			with open(path + "/" + filename, 'r') as f:
				for word in f.readlines():
					if ("**EOF**" in word):
						line = cleanString(line)
						words = line.split()
						if (len(words) < minInstanceSize):
							continue
						rawData[class_][filename].append(line)
						line = ""
						continue
					elif ("**SOF**" in word):
						continue
					else:
						line = line + " " + cleanWord(word)

	np.random.seed(10)

	trainData = dict()
	testData = dict()

	# Split data into test and training sets
	println("Splitting Data : ")
	for class_ in rawData:
		trainData[class_] = []
		testData[class_] = []
		for filename in rawData[class_]:
			array = rawData[class_][filename]
			np.random.shuffle(array)
			split = int(len(array) * trainFiles[class_][filename])
			train, test = array[:split], array[split:]
			trainData[class_].extend(train)
			testData[class_].extend(test)

	# Build vocab from training set
	println("Building Vocab : ")
	for class_ in trainData:
		for line in trainData[class_]:
			words = line.split()
			distrSize.append(len(words))
			for word in words:
				if word in vocab:
					vocab[word] += 1
				else:
					vocab[word] = 1

	# Clean Vocab
	println("Cleaning Vocab : ")
	cleanVocab = dict()
	for word, freq in vocab.items():
		if (freq >= minTermFreq):
			cleanVocab[word] = vocab[word]

	# Generate Random Data
	println("Generating Random : ")
	p = []
	for val in range(0, max(distrSize) + 1):
		p.append(distrSize.count(val) / len(distrSize))

	sizes = random = np.random.choice(max(distrSize) + 1, numRandom, p=p)

	vocabKeys = list(vocab.keys())
	for size in sizes:
		line = ""
		for val in range(0, size):
			line += vocabKeys[np.random.random_integers(0, len(vocabKeys)-1)] + " "
		testData["negative"].append(line)
		
	# Split data to specified sizes
	for class_ in trainData:
		np.random.shuffle(trainData[class_])
		trainData[class_] = trainData[class_][:maxSizeTrain]
	
	for class_ in testData:
		np.random.shuffle(testData[class_])
		testData[class_] = testData[class_][:maxSizeTest]

	# Replace Words
	println("Replacing Words (Train) : ")
	cTrainData = dict()
	for class_ in trainData:
		cTrainData[class_] = []
		for line in trainData[class_]:
			words = line.split()
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cTrainData[class_].append(line)

		
	# Replace Words
	println("Replacing Words (Test) : ")
	cTestData = dict()
	for class_ in testData:
		cTestData[class_] = []
		for line in testData[class_]:
			words = line.split()
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cTestData[class_].append(line)

	# Save datasets
	print("Dataset Sizes")
	for class_ in cTrainData:
		print("	uci-train." + class_ + " : " + str(len(cTrainData[class_])))
		with open(output + "uci-train." + class_, "w+") as f:
			for line in cTrainData[class_]:
				print(line, file=f)
	for class_ in cTestData:
		print("	uci-test." + class_ + " : " + str(len(cTestData[class_])))
		with open(output + "uci-test." + class_, "w+") as f:
			for line in cTestData[class_]:
				print(line, file=f)

	

def main():
	if (len(sys.argv) < 1):
		print("Expected Input : python3 generate_dataset <output path>")
		sys.exit()

	output = sys.argv[1] + "/"

	data = {
		"positive" : {
			"USER0" : 0.8,
			"USER1" : 0.8
		},
		"negative" : {
			"USER2" : 1,
			"USER3" : 1,
			"USER4" : 1,
			"USER5" : 1,
			"USER6" : 0,
			"USER7" : 0,
			"USER8" : 0,
			"RANDOM" : 200
		}
	}
	Tensorflow(data, output)
		
	

if __name__ == "__main__":
	main()

