from __future__ import print_function
import numpy as np
import re
import sys
import numbers

# Source : https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
# Distance metric used to calculate how similar two words are
def levDistance(source, target):
    if len(source) < len(target):
        return levDistance(target, source)

    if len(target) == 0:
        return len(source)
		
    source = np.array(tuple(source))
    target = np.array(tuple(target))
	
    previous_row = np.arange(target.size + 1)
    for s in source:
        current_row = previous_row + 1
		
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]
	
def cleanWord(word):
	word = ''.join(s for s in word if ord(s)>31 and ord(s)<126)
	#word = re.sub("\d", "#", word)
	word = word.replace("\\", "")
	word = word.replace("/", "")
	#word = word.replace("<", " <")
	#word = word.replace(">", "> ")
	word = word.replace(" ", "")
	return word
	
def cleanString(string):
	string = string.replace("\"", "")
	string = string.replace("\\", "")
	string = string.replace("/", "")
	string = string.replace("<", " <")
	string = string.replace(">", "> ")
	string = string.replace("  ", " ")
	string = string.replace(".", " ")
	string = string.replace("*", "")
	return string
	
def println(string):
	sys.stdout.write(str(string) + "\n")
	sys.stdout.flush()
	
def findClosest(vocab, string):
	dClosest = 100000
	wClosest = ""
	fClosest = 0
	for word in vocab:
		distance = levDistance(string, word)
		if (distance < dClosest) or (distance == dClosest and vocab[word] > fClosest):
			dClosest = distance
			wClosest = word
			fClosest = vocab[word]
	return wClosest


def main():
	files = {
		"USER0" : "USER0",
		"USER1" : "USER0",
		"USER2" : "USER2",
		"USER3" : "USER3",
		"USER4" : "USER4",
		"USER5" : "USER5",
		"USER6" : "USER6",
		"USER7" : "USER7",
		"USER8" : "USER8",
	}

	output = "tensorflow_data/"
	path = "data/shell_data"
	vocab = dict()
	data = dict()
	minTermFreq = 20
	minInstanceSize = 5
	
	for filename in files:
		data[files[filename]] = []
		
	for filename in files:
		println("Reading : " + filename)
		line = ""
		with open(path + "/" + filename, 'r') as f:
			for word in f.readlines():
				if ("**EOF**" in word):
					line = cleanString(line)
					words = line.split()
					for word in words:
						if word in vocab:
							vocab[word] += 1
						else:
							vocab[word] = 1
				
					data[files[filename]].append(line)
					line = ""
					continue
				elif ("**SOF**" in word):
					continue
				elif ("<" in word):
					line = line + "" + cleanWord(word)
				else:
					line = line + " " + cleanWord(word)
					
	println("Cleaning Vocab : ")
	
	cleanVocab = dict()
	for word, freq in vocab.items():
		if (freq >= minTermFreq):
			cleanVocab[word] = vocab[word]
			
	cData = dict()
	for _class in data:
		cData[_class] = []
		println("Replacing Words : " + _class)
		for line in data[_class]:
			words = line.split()
			if (len(words) < minInstanceSize):
				continue
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cData[_class].append(line)

			
	for _class in cData:
		with open(output + "training." + _class, "w") as f:
			for line in cData[_class]:
				print(line, file=f)

if __name__ == "__main__":
	main()

