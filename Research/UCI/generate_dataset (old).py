from __future__ import print_function
import re
import json
import sys
import os.path
import csv
import datetime
import numpy as np
import contextlib
from os import walk

def StringToAscii(string):
	string = string.replace("\"", "'")
	string = ''.join(s for s in string if ord(s)>31 and ord(s)<126)
	string = string.replace("\\", " ")
	string = string.replace("/", " ")
	string = string.replace("<", " <")
	string = string.replace(">", "> ")
	string = string.replace("  ", " ")
	return string

def levDistance(source, target):
    if len(source) < len(target):
        return levDistance(target, source)

    if len(target) == 0:
        return len(source)
		
    source = np.array(tuple(source))
    target = np.array(tuple(target))
	
    previous_row = np.arange(target.size + 1)
    for s in source:
        current_row = previous_row + 1
		
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]
	
def cleanWord(word):
	word = ''.join(s for s in word if ord(s)>31 and ord(s)<126)
	#word = word.replace(" ", "")
	return word
	
def cleanString(string):
	string = string.replace("\"", "")
	string = string.replace("\\", "")
	string = string.replace("/", "")
	string = string.replace("<", " <")
	string = string.replace(">", "> ")
	string = string.replace("  ", " ")
	string = string.replace(".", " ")
	string = string.replace("*", "")
	return string
	
def println(string):
	sys.stdout.write(str(string) + "\n")
	sys.stdout.flush()
	
def findClosest(vocab, string):
	dClosest = 100000
	wClosest = ""
	fClosest = 0
	for word in vocab:
		distance = levDistance(string, word)
		if (distance < dClosest) or (distance == dClosest and vocab[word] > fClosest):
			dClosest = distance
			wClosest = word
			fClosest = vocab[word]
	return wClosest


def weka():
	files = {
		"USER0" : "USER0",
		"USER1" : "USER0",
		"USER2" : "USER2",
		"USER3" : "USER3",
		"USER4" : "USER4",
		"USER5" : "USER5",
		"USER6" : "USER6",
		"USER7" : "USER7",
		"USER8" : "USER8",
	}

	class_args = sys.argv[2:]	
	path = "data/shell_data"
	data = {}
	
	if len(class_args) > 1:
		class_args.append("OTHER")
		for filename in files:
			if files[filename] not in class_args:
				files[filename] = "OTHER"
	else:
		for filename in files:
			if files[filename] not in class_args:
				class_args.append(files[filename])			

	for class_ in class_args:
		data[class_] = [] 

	for filename in files:		

		line = ""
		with open(path + "/" + filename, 'r') as f:
			for word in f.readlines():
				if ("**EOF**" in word):
					data[files[filename]].append(line)
					line = ""
					continue
				elif ("**SOF**" in word):
					continue
				elif ("<" in word):
					line = line + "" + StringToAscii(word)
				else:
					line = line + " " + StringToAscii(word)


	with open("weka_data/shell_data.arff", 'w') as f:
		print("@relation shell", file=f)
		print("@attribute history string", file=f)
		classes = ''.join(class_ + " " for class_ in class_args)
		print (classes)

		print("@attribute class {" + classes + "}", file=f)
		print("@data", file=f)
		for class_ in data:
			for line in data[class_]:
				print("\"" + line + "\"," + class_, file=f)
				
def Tensorflow(files, train_files, test_files, output):
	# TO DO:
		# Improve Test/Train distribution
		# Add in method to create random NEGATIVE instances and use to benchmark performance. It is highly unlikely that a random instance will resemble this.


	print(output)	
	path = "data/shell_data"
	vocab = dict()
	data = dict()
	minTermFreq = 5
	minInstanceSize = 5
	maxSizeTrain = 200
	maxSizeTest = 200
		
	# Read in each line from the provided files
	for filename in files:
		println("Reading : " + filename)
		data[filename] = []
		line = ""
		with open(path + "/" + filename, 'r') as f:
			for word in f.readlines():
				# If EOF token, finish session
				if ("**EOF**" in word):
					# Clean string and split into word array
					line = cleanString(line)
					words = line.split()
					# Append line to data
					data[filename].append(line)
					line = ""
					# If file not in training set, don't put in vocab
					if (filename not in train_files):
						continue
					# Increment the frequency of each word within the vocab
					for word in words:
						if word in vocab:
							vocab[word] += 1
						else:
							vocab[word] = 1
					continue
				elif ("**SOF**" in word):
					continue
				else:
					# Append clean word to end of session line
					line = line + " " + cleanWord(word)
	
	# Remove words that do meet the minimum term frequency			
	println("Cleaning Vocab : ")
	cleanVocab = dict()
	for word, freq in vocab.items():
		if (freq >= minTermFreq):
			cleanVocab[word] = vocab[word]

	# Replace words not within the vocabulary with the closest matching word that is
	cleanData = dict()
	for filename in data:
		cleanData[filename] = []
		println("Replacing Words : " + filename)
		for line in data[filename]:
			words = line.split()
			if (len(words) < minInstanceSize):
				continue
			line = ""
			for word in words:
				if word not in cleanVocab:
					word = findClosest(cleanVocab, word)
				line += word + " "
			cleanData[filename].append(line)

	# Split into Training and Testing sets
	
	# Remove existing files
	for filename in files:
		with contextlib.suppress(FileNotFoundError):
			os.remove(output + "uci-train." + files[filename])
			os.remove(output + "uci-test." + files[filename])
	
	# Create training sets
	println("Writing Train Data : ")	
	train_length = dict()
	for filename in train_files:
		if files[filename] not in train_length:
			train_length[files[filename]] = 0
		with open(output + "uci-train." + files[filename], "a") as f:
			for line in cleanData[filename]:
				if (train_length[files[filename]] >= maxSizeTrain):
					break
				print(line, file=f)
				train_length[files[filename]] += 1

	# Create test sets
	println("Writing Test Data : ")
	test_length = dict()
	for filename in test_files:
		if files[filename] not in test_length:
			test_length[files[filename]] = 0
		with open(output + "uci-test." + files[filename], "a") as f:
			for line in cleanData[filename]:
				if (test_length[files[filename]] >= maxSizeTrain):
					break
				print(line, file=f)
				test_length[files[filename]] += 1

	println ("Train Length:")
	for name in train_length:
		println ("	" + name + " : " + str(train_length[name])) 
	println ("Test Length:")
	for name in test_length:
		println ("	" + name + " : " + str(test_length[name])) 

def main():
	if (len(sys.argv) < 3):
		print("Expected Input : python3 generate_dataset {weka|tensorflow} <output path> [class...]")
		sys.exit()

	output = sys.argv[2] + "/"
	files = {
		"USER0" : "USER0",
		"USER1" : "USER0",
		"USER2" : "USER2",
		"USER3" : "USER3",
		"USER4" : "USER4",
		"USER5" : "USER5",
		"USER6" : "USER6",
		"USER7" : "USER7",
		"USER8" : "USER8",
	}
	trainFiles = ["USER1", "USER2", "USER3", "USER4"]
	testFiles = ["USER0", "USER5", "USER6", "USER7", "USER8"]

	class_args = sys.argv[3:]
	if len(class_args) >= 1:
		class_args.append("OTHER")
		for filename in files:
			if files[filename] not in class_args:
				files[filename] = "OTHER"
	else:
		for filename in files:
			if files[filename] not in class_args:
				class_args.append(files[filename])


	if len(sys.argv[:]) == 0:
		print ("Invalid Arguments")
		return

	if sys.argv[1] == 'weka':
		weka()
	elif sys.argv[1] == 'tensorflow':
		Tensorflow(files, trainFiles, testFiles, output)
	

if __name__ == "__main__":
	main()

