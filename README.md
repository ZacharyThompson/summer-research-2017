# README #


### What is this repository for? ###

* This repository consists of various information collected during my Summer Research at the University of Waikato. The primary project is an application that monitors file accesses on a device and attempts to isolate deviations in behaviour. 
* The repository also contains information about several other predictor variables researched, though these files purely exist in a record keeping capacity; they have not been organised in any meaningful manner.


### Author ###

Zachary Thompson (zaachary3@hotmail.com)